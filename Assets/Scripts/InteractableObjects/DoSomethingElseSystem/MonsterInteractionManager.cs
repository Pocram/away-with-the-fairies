﻿using UnityEngine;
using System.Collections;

namespace AI.MonsterInteraction {

	public sealed class MonsterInteractionManager : MonoBehaviourExtended {

		#region Singleton

		private static MonsterInteractionManager _instance;

		public static MonsterInteractionManager Instance
		{
			get
			{
				if ( _instance == null ) {
					_instance = FindObjectOfType<MonsterInteractionManager>();

					// If no instance is found, create one
					if ( _instance == null ) {
						GameObject newGo = new GameObject();
						newGo.name = "MonsterInteractionManager";
						newGo.AddComponent<BadOMetreManager>();
					}

					if ( _instance != null )
						DontDestroyOnLoad( _instance.gameObject );
				}

				return _instance;
			}
		}

		#endregion

		#region Exposed variables

		[SerializeField]
		private int _currentSelectionIndex;

		public int CurrentSelectionIndex {
			get { return _currentSelectionIndex; }
			set {
				value = MathfExtension.ClampInt( value, 0, MaxAmountOfOptions - 1 );
				_currentSelectionIndex = value;
			}
		}

		public static bool InteractionIsInProgress { get; private set; }

		#endregion

		#region Input

		[Header( "Input" )]
		[SerializeField]
		private KeyCode[] _scrollUpKeys = new KeyCode[] { KeyCode.UpArrow, KeyCode.W, KeyCode.LeftArrow, KeyCode.A };

		[SerializeField]
		private KeyCode[] _scrollDownKeys = new KeyCode[] { KeyCode.DownArrow, KeyCode.S, KeyCode.RightArrow, KeyCode.D };

		[SerializeField]
		private KeyCode[] _chooseOptionKeys = new KeyCode[] { KeyCode.Return };

		[SerializeField]
		private KeyCode[] _exitInteractionKeys = new KeyCode[] { KeyCode.Escape };

		#endregion

		#region Private variables

		private const int MaxAmountOfOptions = 3;

		private Coroutine _handleActionInputCoroutine;

		#endregion

		#region MonoBehaviour

		private void Awake() {
			EnsureSingletonPattern();
		}

		#endregion
		
		#region Private Methods

		/// <summary>
		/// Ensures only one instance can be active at a time.
		/// </summary>
		private void EnsureSingletonPattern() {
			if ( _instance != null && _instance != this ) {
				Destroy( gameObject );
			} else {
				_instance = this;
				DontDestroyOnLoad( gameObject );
			}
		}

		private IEnumerator HandleActionInput( MonsterInteraction interactionScript ) {
			BlockPlayerInput();
			CursorController.Instance.FreeCursor();

			bool hasChosenOption = false;
			bool exitInteraction = false;

			float lastScrollDirection = 0f;

			float loopStartTime = Time.time;

			// Choose option
			while (!hasChosenOption && !exitInteraction) {
				float scrollDir = Input.GetAxisRaw( "Mouse ScrollWheel" );

				// Check input
				if (_scrollUpKeys.HasKeyDown() || ( scrollDir > 0f && lastScrollDirection == 0f ) ) {
					// Scroll up
					CurrentSelectionIndex--;
				} else if (_scrollDownKeys.HasKeyDown() || ( scrollDir < 0f && lastScrollDirection == 0f ) ) {
					// Scroll down
					CurrentSelectionIndex++;
				} else if (_chooseOptionKeys.HasKeyDown() && Time.time - loopStartTime >= 0.01f) {
					// Choose option
					hasChosenOption = true;
				} else if (_exitInteractionKeys.HasKeyDown()) {
					// Exit interaction
					exitInteraction = true;
				}

				lastScrollDirection = Input.GetAxisRaw( "Mouse ScrollWheel" );
				yield return new GamePausing.WaitIfPaused();
			}

			if (hasChosenOption) {
				// TODO: interact with option
			}

			// End
			Debug.Log( "End" );
			FreePlayerInput();
			CursorController.Instance.LockCursor();

			// Unblock pausing
			GamePausing.PauseManager.EnablePausing();

			InteractionIsInProgress = false;
		}

		#endregion

		#region Public methods

		public static void StartInteraction( MonsterInteraction interactionScript ) {
			if (Instance == null)
				return;

			if (InteractionIsInProgress)
				return;
			InteractionIsInProgress = true;

			// Block pausing
			GamePausing.PauseManager.BlockPausing();

			// Start coroutine
			if(Instance._handleActionInputCoroutine != null)
				Instance.StopCoroutine( Instance._handleActionInputCoroutine );
			Instance.StartCoroutine( Instance.HandleActionInput( interactionScript ) );
		}

		#endregion

	}

}