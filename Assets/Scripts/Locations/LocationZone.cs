﻿using UnityEngine;

namespace Locations {

	[RequireComponent( typeof( Collider ) )]
	public class LocationZone : MonoBehaviourExtended {

		#region Events

		public delegate void LocationEventDelegate(LocationZone zone, LocationZoneEventData data);

		public event LocationEventDelegate OnThisZoneEnter;

		public static event LocationEventDelegate OnAnyZoneEnter;

		public event LocationEventDelegate OnThisZoneExit;

		public static event LocationEventDelegate OnAnyZoneExit;

		#endregion

		[SerializeField]
		private string _locationName;

		public string LocationName {
			get { return _locationName; }
		}

		[SerializeField]
		private LayerMask _layerMask;

		#region MonoBehavior

		private void OnTriggerEnter(Collider other) {
			if (!_layerMask.ContainsLayer( other.gameObject.layer ))
				return;

			LocationZoneEventData data = new LocationZoneEventData( other );

			if (OnThisZoneEnter != null) OnThisZoneEnter( this, data );
			if (OnAnyZoneEnter != null) OnAnyZoneEnter( this, data );
		}

		private void OnTriggerExit(Collider other) {
			if (!_layerMask.ContainsLayer( other.gameObject.layer ))
				return;

			LocationZoneEventData data = new LocationZoneEventData( other );

			if (OnThisZoneExit != null)
				OnThisZoneExit( this, data );
			if (OnAnyZoneExit != null)
				OnAnyZoneExit( this, data );
		}

		#endregion

		#region Public Types

		public class LocationZoneEventData {

			public LocationZoneEventData(Collider collider) {
				Collider = collider;
			}

			public Collider Collider;

		}

		#endregion

	}

}