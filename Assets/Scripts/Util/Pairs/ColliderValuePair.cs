﻿using UnityEngine;

public class ColliderValuePair<T> {

	public Collider Collider;

	public T Value;

}