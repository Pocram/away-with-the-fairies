﻿using Dialogue;
using ObjectInteraction;
using UnityEngine;

public class TestObjectInteraction : MonoBehaviourExtended, IInteractableObject {

	public string ObjectName {
		get { return _objectName; }

		set { _objectName = value; }
	}

	[SerializeField]
	private string _objectName;

	[SerializeField]
	private DialogueNode _node;


	#region IInteractableObject

	public string GetInteractionPromptText() {
		return "<b>[E]</b> Talk to <b><color=#ffa500ff>" + ObjectName + "</color></b>";
	}

	public GameObject GetGameObject() {
		return gameObject;
	}

	public void OnInteractionEnter() {
		GetComponent<Renderer>().material.color = Color.yellow;

		DialogueManager.ShowDialogueNode( _node );
	}

	public void OnInteractionStay() {
		
	}

	public void OnInteractionExit() {
		GetComponent<Renderer>().material.color = Color.white;
	}

	#endregion

}