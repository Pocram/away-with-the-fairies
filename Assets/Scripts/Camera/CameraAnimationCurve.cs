﻿using UnityEngine;

[System.Serializable]
public class CameraAnimationCurve {

	[Header("Position")]
	[CurveStyle( CurveStyleAttribute.CurveColor.Red, 0f, -1f, 1f, 2f )]
	public AnimationCurve XPositionCurve = new AnimationCurve( new Keyframe( 0f, 0f ), new Keyframe( 1f, 0f ) );

	[CurveStyle( CurveStyleAttribute.CurveColor.Green, 0f, -1f, 1f, 2f )]
	public AnimationCurve YPositionCurve = new AnimationCurve( new Keyframe( 0f, 0f ), new Keyframe( 1f, 0f ) );

	[CurveStyle( CurveStyleAttribute.CurveColor.Blue, 0f, -1f, 1f, 2f )]
	public AnimationCurve ZPositionCurve = new AnimationCurve( new Keyframe( 0f, 0f ), new Keyframe( 1f, 0f ) );


	[Header( "Rotation" )]
	[CurveStyle( CurveStyleAttribute.CurveColor.Red, 0f, -360f, 1f, 720f )]
	public AnimationCurve XRotationCurve = new AnimationCurve( new Keyframe( 0f, 0f ), new Keyframe( 1f, 0f ) );

	[CurveStyle( CurveStyleAttribute.CurveColor.Green, 0f, -360f, 1f, 720f )]
	public AnimationCurve YRotationCurve = new AnimationCurve( new Keyframe( 0f, 0f ), new Keyframe( 1f, 0f ) );

	[CurveStyle( CurveStyleAttribute.CurveColor.Blue, 0f, -360f, 1f, 720f )]
	public AnimationCurve ZRotationCurve = new AnimationCurve( new Keyframe( 0f, 0f ), new Keyframe( 1f, 0f ) );

}