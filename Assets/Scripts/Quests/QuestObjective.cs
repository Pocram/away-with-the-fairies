﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Quests {

	[System.Serializable]
	public sealed class QuestObjective : ScriptableObject {

		#region Exposed variables

		public string Description;

		public bool IsFinished { get; private set; }

		public List<QuestObjectiveSolution> Solutions = new List<QuestObjectiveSolution>( new QuestObjectiveSolution[1] );

		#endregion

		#region Events

		public delegate void FinishObjectiveEventDelegate( QuestObjective objective, FinishObjectiveEventData data );

		public delegate void FailObjectiveEventDelegate( QuestObjective objective, FailObjectiveEventData data );

		public delegate void QuestObjectiveUpdateEventDelegate( QuestObjective objective, QuestObjectiveUpdateEventData data );

		public event FinishObjectiveEventDelegate OnFinishObjective;

		public event FailObjectiveEventDelegate OnFailObjective;

		public event QuestObjectiveUpdateEventDelegate OnObjectiveUpdated;

		#endregion

		#region Public Methods

		public void Init() {
			IsFinished = Solutions.Count == 0;

			// Init solutions and subscribe to them
			for ( int i = 0; i < Solutions.Count; i++ ) {
				if ( Solutions[i] == null )
					continue;

				Solutions[i].Init();
				Solutions[i].OnFinishSolution -= OnFinishSolution;
				Solutions[i].OnFinishSolution += OnFinishSolution;
			}
		}

		public void Destruct() {
			// Destruct and unsubscribe
			for ( int i = 0; i < Solutions.Count; i++ ) {
				if ( Solutions[i] == null )
					continue;

				Solutions[i].Destruct();
				Solutions[i].OnFinishSolution -= OnFinishSolution;
			}
		}

		#endregion

		#region Private methods

		private void OnFinishSolution( QuestObjectiveSolution solution, QuestObjectiveSolution.FinishQuestObjectiveSolutionEventData data ) {
			if ( OnObjectiveUpdated != null ) {
				List<QuestObjectiveSolution> updatedSolutions = new List<QuestObjectiveSolution>() { solution };
				OnObjectiveUpdated( this, new QuestObjectiveUpdateEventData( updatedSolutions ) );
			}

			CheckAllSolutionStates();
		}

		private void CheckAllSolutionStates() {
			bool allSolutionsAreFinished = true;

			for ( int i = 0; i < Solutions.Count; i++ ) {
				if ( !Solutions[i].IsFinished ) {
					// Skip non mandatory objectives
					if (!Solutions[i].IsMandatoryForObjective)
						continue;

					allSolutionsAreFinished = false;
					break;
				}
			}

			if ( allSolutionsAreFinished ) {
				IsFinished = true;
				if ( OnFinishObjective != null )
					OnFinishObjective( this, new FinishObjectiveEventData() );
			}
		}

		#endregion

		#region Object creation

		// Menu item to create new object
		[MenuItem( "AWTF/Assets/Quests/New Objective" )]
		public static QuestObjective CreateNewNode() {
			return CreateNewNode( "NewQuestObjective" );
		}

		public static QuestObjective CreateNewNode( string name ) {
			string folder = "ScriptableObjects/Quests/Objectives";
			string fileName = name;

			// Create directory if needed
			if ( !Directory.Exists( Application.dataPath + "/" + folder ) ) {
				Directory.CreateDirectory( Application.dataPath + "/" + folder );
			}

			folder = "Assets/" + folder;

			QuestObjective newAsset = ScriptableObject.CreateInstance<QuestObjective>();

			int totalObjectsByThatName = AssetDatabase.FindAssets( fileName, new[] { folder } ).Length;
			// No other objects by that name found
			if ( totalObjectsByThatName == 0 ) {
				AssetDatabase.CreateAsset( newAsset, folder + "/" + fileName + ".asset" );
				AssetDatabase.SaveAssets();

				EditorUtility.FocusProjectWindow();

				Selection.activeObject = newAsset;
			} else {
				// Other objects by that name exist!
				// Count up; just read the code it's easy
				int id = 1;
				while ( AssetDatabase.FindAssets( fileName + " (" + id + ")", new[] { folder } ).Length != 0 )
					id++;

				AssetDatabase.CreateAsset( newAsset, folder + "/" + fileName + " (" + id + ").asset" );
				AssetDatabase.SaveAssets();

				EditorUtility.FocusProjectWindow();

				Selection.activeObject = newAsset;
			}
			return newAsset;
		}

		#endregion

		#region Public Types

		public class FinishObjectiveEventData {

		}

		public class FailObjectiveEventData {

		}

		public class QuestObjectiveUpdateEventData {

			public QuestObjectiveUpdateEventData( List<QuestObjectiveSolution> updatedSolutions ) {
				UpdatedSolutions = updatedSolutions;
			}

			public List<QuestObjectiveSolution> UpdatedSolutions;

		}

		#endregion

	}

}