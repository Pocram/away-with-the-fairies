﻿using System.Collections;
using UnityEngine;

namespace GamePausing {

	public class WaitForSecondsAndPause : IEnumerator {

		private float _remainingTime;


		public WaitForSecondsAndPause(float seconds) {
			_remainingTime = seconds;
		}


		public bool MoveNext() {
			_remainingTime -= Time.deltaTime;

			return !PauseManager.IsPaused && _remainingTime <= 0f;
		}

		public void Reset() { }

		public object Current {
			get { return null; }
		}
	}

}