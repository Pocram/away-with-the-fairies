﻿using UnityEngine;

public interface IRegionallyDamageable {
	/// <summary>
	/// Applies damage to an actor depending on the collider hit.
	/// </summary>
	/// <param name="damagePoints">The amount of base damage the actor takes. Value should be positive.</param>
	/// <param name="hitCollider">The collider that registered the damage.</param>
	/// <param name="canHeal">Whether the damage may increase the actor's health points.</param>
	/// <param name="sourceObject">The gameObject that caused the damage.</param>
	void Damage( float damagePoints, Collider hitCollider, GameObject sourceObject, bool canHeal = false );

}