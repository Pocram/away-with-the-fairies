﻿using UnityEngine;

using ObjectInteraction;

namespace AI.MonsterInteraction {

	public class MonsterInteraction : MonoBehaviourExtended, IInteractableObject {

		#region Exposed variables

		[SerializeField]
		private string _monsterName = string.Empty;

		#endregion

		#region IInteractableObject Interface

		public string ObjectName
		{
			get { return _monsterName; }
			set { _monsterName = value; }
		}

		public string GetInteractionPromptText() {
			return "<b>[E]</b> Interact with <b><color=#E8A95A>" + ObjectName + "</color></b>";
		}

		public GameObject GetGameObject() {
			return gameObject;
		}

		public void OnInteractionEnter() {
			MonsterInteractionManager.StartInteraction( this );
		}

		public void OnInteractionStay() {
			// Nothing
		}

		public void OnInteractionExit() {
			// Nothing
		}

		#endregion

	}

}