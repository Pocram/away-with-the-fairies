﻿using UnityEngine;
using UnityEditor;

public static class GUIStyleExtension {

	public static GUIStyle PreviewLabel = new GUIStyle( GUI.skin.label ) {
		normal = new GUIStyleState {
			textColor = new Color( 0.675f, 0.675f, 0.675f )
		},
		richText = true
	};

	public static GUIStyle PreviewLabelBold = new GUIStyle(PreviewLabel) {
		fontStyle = FontStyle.Bold
	};


	public static GUIStyle FoldoutBold = new GUIStyle( EditorStyles.foldout ) {
		fontStyle = FontStyle.Bold
	};

}