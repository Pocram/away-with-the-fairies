// GROUND_ACTOR_DEBUG is used for debugging the GroundActorPhysics script.
// Defining it displays movement lines for each actor and shows a GUI with debugging values.
//#define GROUND_ACTOR_DEBUG

using System;
using System.Collections.Generic;

using UnityEngine;

namespace ActorPhysics {

	[DisallowMultipleComponent]
	[RequireComponent( typeof (CharacterController) )]
	public class GroundActorPhysics : MonoBehaviourExtended, IActorPhysics {

		#region Public and exposed fields

		public float Height {
			get { return CharacterController.height; }
		}

		[Header("Basic movement")]
		[Tooltip( "The amount of degrees the actor can turn per second." )]
		public float MaxTurnSpeed = 150f;

		[Tooltip( "If checked, the actor can be controlled in the air." )]
		public bool CanMoveInAir = false;

		public Vector3 Velocity {
			get { return _velocity; }
		}

		public Vector3 FixedVelocity {
			get {
				Vector3 outPut = _velocity;
				if (_velocity.y == GroundingVelocity)
					outPut.y = 0f;

				return outPut;
			}
		}

		/// <summary>
		/// 'True' if the character is walking. Does not include sliding.
		/// </summary>
		public bool IsMoving {
			get {
				Vector3 fixedVelocity = _velocity;
				if (fixedVelocity.y == GroundingVelocity)
					fixedVelocity.y = 0f;

				return fixedVelocity.magnitude > 0f;
			}
		}

		public float CurrentMovementEffectivity {
			get {
				float slidingVelocityPercentage = Mathf.Clamp01( _slidingVelocity.magnitude / MaximumSlidingVelocity );
				float movementSpeedImpediment = MovementSpeedImpedimentByslidingSpeed.Evaluate( slidingVelocityPercentage );
				return movementSpeedImpediment;
			}
		}


		[Header("Jumping")]
		public bool CanJumpWhileSliding = true;

		public float MaximumSlopeAngleToJump = 45f;

		public bool CanJumpInAir = false;

		public int MaxJumpsInAir = 1;

		public AnimationCurve JumpingStrengthBySlidingSpeed = new AnimationCurve(
			new Keyframe( 0f, 1f ),
			new Keyframe( 1f, 0.5f )
			);

		/// <summary>
		/// 'True' if the actor is currently performing a jump.
		/// </summary>
		public bool IsJumping {
			get { return _verticalMovementInput != 0f; }
		}

		[Header( "Slopes and sliding" )]
		public float SlidingSpeedAccelerationMagnitude = 5f;

		public float SlopeLimit = 45f;

		public float MaximumSlidingVelocity = 5f;

		public float SlidingDecelerationPerSecond = -5f;

		public AnimationCurve SlidingDecelerationStrengthPerAngleCurve = new AnimationCurve(
			new Keyframe( 0f, 1f ),
			new Keyframe( 1f, 0f )
			);

		public AnimationCurve MovementSpeedImpedimentByAngleAscending = new AnimationCurve(
			new Keyframe( 0f, 1f ),
			new Keyframe( 1f, 0f )
			);

		public AnimationCurve MovementSpeedBonusByAngleDescending = new AnimationCurve(
			new Keyframe( 0f, 0f ),
			new Keyframe( 1f, 0.5f )
            );

		public bool IsSlowerWhenAscending = true;

		public bool IsFasterWhenDescending = false;

		public AnimationCurve MovementSpeedImpedimentByslidingSpeed = new AnimationCurve(
			new Keyframe( 0f, 1f ),
			new Keyframe( 1f, 0f )
			);


		/// <summary>
		/// Returns the actor's current sliding velocity
		/// </summary>
		public Vector3 SlidingVelocity {
			get { return _slidingVelocity; }
		}

		public bool IsSliding {
			get { return _slidingVelocity.magnitude > 0f; }
		}


		[Header("Gravity")]
		public float Gravity = -9.81f * 2f;

		public float GroundingVelocity = -50f;

		public bool IsOnGround {
			get { return _hasGround; }
		}

		public bool IsInAir {
			get { return !IsOnGround; }
		}

		#endregion

		#region Events

		public delegate void LandingEventDelegate( GroundActorPhysics actorPhysics, GroundActorPhysicsLandingEvent eventData );

		public delegate void SlidingEventDelegate( GroundActorPhysics actorPhysics, GroundActorPhysicsSlidingEvent eventData );

		public delegate void GroundStateEventDelegate( GroundActorPhysics actorPhysics );

		public delegate void HeightChangeEventDelegate( GroundActorPhysics actorPhysics, GroundActorHeightChangeEventData eventData );


		public event LandingEventDelegate OnActorLand;

		public event SlidingEventDelegate OnStartSlide;

		public event SlidingEventDelegate OnStopSlide;

		public event GroundStateEventDelegate OnEnterSteepSurface;

		public event GroundStateEventDelegate OnExitValidSurface;

		public event HeightChangeEventDelegate OnChangeHeight;

		#endregion

		#region Dependencies

		private CharacterController _characterController;

		private CharacterController CharacterController {
			get {
				if ( _characterController == null )
					_characterController = GetComponent<CharacterController>();
				return _characterController;
			}
		}

		#endregion

		#region Private fields

		private Vector3 _velocity = Vector3.zero;

		private Vector3 _slidingVelocity = Vector3.zero;

		private Vector3 _movementInput = Vector3.zero;


		// Vertical forces
		private float _verticalMovementInput = 0f;

		private float _currentVerticalVelocity = 0f;

		private float _currentGravityEffect = 0f;


		// Ground info
		private List<ControllerColliderHit> _controllerColliderHits = new List<ControllerColliderHit>();

		private ControllerColliderHit _lowestControllerColliderHit = null;

		private SurfaceType _surfaceType = SurfaceType.Valid;

		private float _angleOfSurface = 0f;

		private bool _hasGround = false;


		// Jumping info
		private int _currentJumpInAirCount = 0;

		
		// Falling info
		private float _lastApex = 0f;

		#endregion

		#region MonoBehaviour

		private void Start() {
			_lastApex = transform.position.y;
		}

		private void Update() {
			GetGroundInfo();

			if (_surfaceType == SurfaceType.Valid)
				CalculateMovementValidSurface();
			else if (_surfaceType == SurfaceType.Steep)
				CalculateMovementSteepSurface();

			GetSlidingVelocity();

			ApplyVerticalForces();

			// Draw debug rays.
			// They are only drawn when 'GROUND_ACTOR_DEBUG' is defined.
			__DrawVelocityRays();

			CharacterController.Move( ( _velocity + _slidingVelocity ) * Time.deltaTime );
			_movementInput = Vector3.zero;
		}

		private void OnControllerColliderHit( ControllerColliderHit hit ) {
			// Add the hit to the list of hits.
			// The list is then used to find the lowest point, which should be the ground.
			_controllerColliderHits.Add( hit );
		}

		#endregion

		#region Physics calculations

		void GetGroundInfo() {
			SurfaceType previouSurfaceType = _surfaceType;

			// Get lowest collider hit in list.
			// This should usually be the ground.
			ControllerColliderHit lowestHit = null;
			for (int i = 0; i < _controllerColliderHits.Count; i++) {
				if (lowestHit == null || _controllerColliderHits[i].point.y < lowestHit.point.y) {
					lowestHit = _controllerColliderHits[i];
				}
			}
			_lowestControllerColliderHit = lowestHit;

			// Get angle of surface.
			// If no hit was found, go for 0.
			_angleOfSurface = lowestHit != null ? Vector3.Angle( lowestHit.normal, Vector3.up ) : 0f;

			// If the angle is <= to the slope limit, it's valid.
			// If it's over, it's steep.
			_surfaceType = _angleOfSurface <= SlopeLimit ? SurfaceType.Valid : SurfaceType.Steep;

			// Handle surface events
			if (_surfaceType == SurfaceType.Steep && previouSurfaceType != SurfaceType.Steep) {
				if (OnEnterSteepSurface != null) {
					OnEnterSteepSurface( this );
				}
			} else if (_surfaceType != SurfaceType.Steep && previouSurfaceType == SurfaceType.Steep) {
				if (OnExitValidSurface != null) {
					OnExitValidSurface( this );
				}
			}

			_hasGround = CharacterController.isGrounded;

			// Reset list of collisions.
			_controllerColliderHits.Clear();
		}

		// Movement
		private void CalculateMovementValidSurface() {
			if (_hasGround || CanMoveInAir) {
				float finalMovementSpeed = 1f;

				// Detect whether the player is ascending or descending
				if (_hasGround && (IsSlowerWhenAscending || IsFasterWhenDescending)) {
					float surfaceanglePercentage = Mathf.Clamp01( _angleOfSurface / 90f );

					// Compare the direction of the slope with the direction of the actor's movement.
					// dot > 0 = descending
					// dot < 0 = ascending
					Vector3 slopedirection = _lowestControllerColliderHit.normal.ToFlat().normalized;
					float dot = Vector3.Dot( slopedirection, _movementInput.normalized );

					if (dot > 0 && IsFasterWhenDescending) {
						// Increase speed
						finalMovementSpeed *= ( 1f + MovementSpeedBonusByAngleDescending.Evaluate( surfaceanglePercentage ) );
					} else if (dot < 0 && IsSlowerWhenAscending) {
						// Decrease speed
						finalMovementSpeed *= MovementSpeedImpedimentByAngleAscending.Evaluate( surfaceanglePercentage );
					}
				}

				// Alter movement speed based on sliding velocity
				float slidingVelocityPercentage = Mathf.Clamp01( _slidingVelocity.magnitude / MaximumSlidingVelocity );
				float movementSpeedImpediment = MovementSpeedImpedimentByslidingSpeed.Evaluate( slidingVelocityPercentage );
				finalMovementSpeed *= movementSpeedImpediment;


				_velocity = _movementInput * finalMovementSpeed;
			}
		}

		private void CalculateMovementSteepSurface() {
			// Decelerate velocity when _ascending_
			Vector3 surfaceDirection = _lowestControllerColliderHit.normal.ToFlat().normalized;
			float dot = Vector3.Dot( surfaceDirection, _velocity.normalized );

			// dot is less than zero when moving into slope, i.e. ascending
			if (dot < 0) {
				// Rapidly decelerate velocity
				_velocity += 15f * surfaceDirection * Time.deltaTime;
			}
		}


		// Sliding
		private void GetSlidingVelocity() {
			if (_lowestControllerColliderHit != null) {
				// Slide if the surface is steep
				if(_surfaceType == SurfaceType.Steep ) {
					if (!IsSliding) {
						if (OnStartSlide != null) {
							OnStartSlide( this, new GroundActorPhysicsSlidingEvent() );
						}
					}

					Vector3 surfaceDirection = _lowestControllerColliderHit.normal.ToFlat().normalized;
					_slidingVelocity += surfaceDirection * Time.deltaTime * SlidingSpeedAccelerationMagnitude;
				}

				// Cap sliding velocity
				if ( _slidingVelocity.magnitude > 5f )
					_slidingVelocity.SetMagnitude( 5f );
			}


			DecelerateSlidingVelocity();
		}

		private void DecelerateSlidingVelocity() {
			// Only decelerate when grounded
			if (!_hasGround)
				return;

			float anglePercentage = Mathf.Clamp01( _angleOfSurface / 90f );
			float decelerationWeight = SlidingDecelerationStrengthPerAngleCurve.Evaluate( anglePercentage );
			float weighedDeceleration = decelerationWeight * SlidingDecelerationPerSecond;

			_slidingVelocity = _slidingVelocity.AddMagnitude( weighedDeceleration * Time.deltaTime );

			// Fix jittering of sliding vector
			if (_slidingVelocity.magnitude <= 0.01f) {
				if (IsSliding) {
					if (OnStopSlide != null) {
						OnStopSlide( this, new GroundActorPhysicsSlidingEvent() );
					}
				}

				_slidingVelocity = Vector3.zero;
			}
		}


		// Vertical forces
		private void ApplyVerticalForces()  {
			if (_hasGround) {
				// If the player just entered the ground, reset the jumping velocity
				if (_currentVerticalVelocity < 0f && _currentVerticalVelocity != GroundingVelocity) {
					// Player landed!
					if (OnActorLand != null) {
						OnActorLand( this, new GroundActorPhysicsLandingEvent( _lastApex - transform.position.y, _currentVerticalVelocity ) );
					}

					_currentJumpInAirCount = 0;
					_verticalMovementInput = 0f;
				}

				_lastApex = transform.position.y;

				// Movement on ground
				_currentGravityEffect = 0f;
				_currentVerticalVelocity = GroundingVelocity;

				if (_verticalMovementInput > 0f) {
					_currentVerticalVelocity = _verticalMovementInput * Time.deltaTime;
				}

			} else {
				// Movement in air
				// ----------------
				float previousVerticalVelocity = _currentVerticalVelocity;

				// Check if the actor hit a ceiling last frame
				if ((CharacterController.collisionFlags & CollisionFlags.Above) != 0) {
					// Ouch! Better fall down again.
					_verticalMovementInput = 0f;
				}

				_currentGravityEffect += Gravity * Time.deltaTime;
				_currentVerticalVelocity = _verticalMovementInput + _currentGravityEffect;

				// Check if the actor just now started falling
				if (previousVerticalVelocity >= 0f && _currentVerticalVelocity < 0f) {
					_lastApex = transform.position.y;
				}
			}

			_velocity.y = _currentVerticalVelocity;
		}

		/// <summary>
		/// Calculates the force that is required to reach a certain jump height.
		/// </summary>
		/// <param name="jumpHeight">The desired height to reach with the jump.</param>
		public float GetJumpForceRequiredToReachJumpHeight( float jumpHeight ) {
			// Sqrt(2) * Sqrt(g) * Sqrt(height)
			return 1.41421356237f * Mathf.Sqrt( Mathf.Abs( Gravity ) ) * Mathf.Sqrt( jumpHeight );
		}

		#endregion

		#region IActorPhysics

		public void MoveInDirection( Vector3 direction ) {
			_movementInput = direction;
		}

		public void MoveTowardsPosition( Vector3 targetPosition ) {
			MoveInDirection( targetPosition - transform.position );
		}

		public void RotateAroundY( float angle ) {
			if (angle > MaxTurnSpeed * Time.deltaTime)
				angle = MaxTurnSpeed * Time.deltaTime;
			if (angle < -MaxTurnSpeed * Time.deltaTime)
				angle = -MaxTurnSpeed * Time.deltaTime;

			transform.Rotate( Vector3.up, angle );
		}

		public void LookInDirection( Vector3 direction ) {
			direction = direction.ToFlat();

			Vector3 newDirection = Vector3.RotateTowards( transform.forward, direction.normalized, MaxTurnSpeed * Time.deltaTime, 0f );
			transform.rotation = Quaternion.LookRotation( newDirection );
		}

		public bool Jump(float force) {
			if (IsSliding && !CanJumpWhileSliding)
				return false;

			if (_angleOfSurface > MaximumSlopeAngleToJump)
				return false;

			if (!_hasGround && !CanJumpInAir)
				return false;

			if (_currentJumpInAirCount >= MaxJumpsInAir)
				return false;

			if(!_hasGround)
				_currentJumpInAirCount++;

			float jumpingStrength = JumpingStrengthBySlidingSpeed.Evaluate( Mathf.Clamp01( _slidingVelocity.magnitude / MaximumSlidingVelocity ) );
			_currentGravityEffect = 0f;
			_verticalMovementInput = force * jumpingStrength;

			return true;
		}

		#endregion

		#region Misc. Public Methods

		public void SetActorHeight( float newHeight ) {
			if ( newHeight < CharacterController.radius * 2f ) {
				newHeight = CharacterController.radius * 2f;
			}

			if (OnChangeHeight != null) {
				OnChangeHeight( this, new GroundActorHeightChangeEventData( CharacterController.height, newHeight ) );
			}

			CharacterController.height = newHeight;
			CharacterController.center = new Vector3( CharacterController.center.x, newHeight / 2f, CharacterController.center.z );
		}

		public bool CheckCapsule( float height, LayerMask layerMask ) {
			Vector3 capsuleStart = new Vector3( transform.position.x, transform.position.y + CharacterController.radius, transform.position.z );
			Vector3 capsuleEnd = capsuleStart + new Vector3( 0f, height - 2f * CharacterController.radius, 0f );

			return Physics.CheckCapsule( capsuleStart, capsuleEnd, CharacterController.radius, layerMask );
		}

		#endregion

		#region Internal types

		/// <summary>
		/// The SurfaceType of the surface the actor is currently standing on. <para/>
		/// A valid surface is within the slope limit; a steep surface is not.
		/// </summary>
		private enum SurfaceType {

			Valid,
			Steep

		}

		#endregion

		#region __DEBUG

#if GROUND_ACTOR_DEBUG
		private void OnGUI() {
			Vector2 boxDimensions = new Vector2(300f, 310f);
			Vector2 boxPosition = new Vector2(10f, 10f);
			float boxPadding = 5f;

			// Velocity without strong gravity
			Vector3 fixedVelocity = _velocity;
			if ( _hasGround && fixedVelocity.y <= GroundingVelocity) {
				fixedVelocity.y -= GroundingVelocity;
			}

			Vector3 fixedFinalVelocity = _velocity + _slidingVelocity;
			if ( _hasGround && fixedFinalVelocity.y <= GroundingVelocity ) {
				fixedFinalVelocity.y -= GroundingVelocity;
			}

			float anglePercentage = Mathf.Clamp01( _angleOfSurface / 90f );
			float decelerationWeight = SlidingDecelerationStrengthPerAngleCurve.Evaluate( anglePercentage );
			float weighedDeceleration = -( decelerationWeight * SlidingDecelerationPerSecond );

			string displayText = "<b>Input</b>: " + _movementInput;
			displayText += "\n";
			displayText += "\n<b>Velocity</b>: " + _velocity + " (" + Math.Round( _velocity.magnitude, 2 ) + ")";
			displayText += "\n<b>FixedVelocity</b>: " + fixedVelocity + " (" + Math.Round( fixedVelocity.magnitude, 2 ) + ")";
			displayText += "\n<b>SlidingVelocity</b>: " + _slidingVelocity + " (" + Math.Round( _slidingVelocity.magnitude, 2 ) + ")";
			displayText += "\n<b>SlidingDeceleration</b>: " + Math.Round(weighedDeceleration, 2);
			displayText += "\n<b>TotalVelocity</b>: " + ( _velocity + _slidingVelocity ) + " (" + Math.Round( ( _velocity + _slidingVelocity ).magnitude, 2 ) + ")";
			displayText += "\n<b>FixedTotalVelocity</b>: " + ( fixedFinalVelocity ) + " (" + Math.Round( ( fixedFinalVelocity ).magnitude, 2 ) + ")";
			displayText += "\n";
			displayText += "\n<b>HasGround</b>: " + _hasGround;
			displayText += "\n<b>SurfaceType</b>: " + _surfaceType;
			displayText += "\n<b>Angle</b>: " + Math.Round(_angleOfSurface, 2);
			displayText += "\n";
			displayText += "\n<b>CurrentGravityEffect</b>: " + _currentGravityEffect;
			displayText += "\n<b>VerticalMovementInput</b>: " + _verticalMovementInput;
			displayText += "\n<b>VerticalVelocity</b>: " + Math.Round(_currentVerticalVelocity, 2);
			displayText += "\n";
			displayText += "\n<b>CurrentAirJumpCount</b>: " + _currentJumpInAirCount;

			GUI.Box( new Rect( boxPosition.x, boxPosition.y, boxDimensions.x, boxDimensions.y ), "<b>PlayerController Debug Console</b>" );
			GUI.Label( new Rect( boxPosition.x + boxPadding, boxPosition.y + boxPadding + 20f, boxDimensions.x - boxPadding, boxDimensions.y - boxPadding - 20f), displayText );
		}
#endif

		[System.Diagnostics.Conditional( "GROUND_ACTOR_DEBUG" )]
		private void __DrawRay(Vector3 start, Vector3 direction, Color color, float duration = 0f, bool depthTest = true) {
			Debug.DrawRay( start, direction, color, duration, depthTest );
		}

		[System.Diagnostics.Conditional( "GROUND_ACTOR_DEBUG" )]
		private void __DrawLine( Vector3 start, Vector3 end, Color color, float duration = 0f, bool depthTest = true ) {
			Debug.DrawLine( start, end, color, duration, depthTest );
		}

		[System.Diagnostics.Conditional( "GROUND_ACTOR_DEBUG" )]
		private void __DrawVelocityRays() {
			Vector3 fixedVelocity = _velocity;
			if (_hasGround && fixedVelocity.y <= GroundingVelocity ) {
				fixedVelocity.y -= GroundingVelocity;
			}

			__DrawRay( transform.position, fixedVelocity, Color.blue );
			__DrawRay( transform.position, _slidingVelocity, Color.red );
			__DrawRay( transform.position, fixedVelocity + _slidingVelocity, Color.yellow );
		}

		#endregion

	}


	public class GroundActorPhysicsLandingEvent {

		public GroundActorPhysicsLandingEvent(float fallHeight, float fallVelocity) {
			FallHeight = fallHeight;
			FallVelocity = fallVelocity;
		}

		public float FallHeight;
		public float FallVelocity;

	}

	public class GroundActorPhysicsSlidingEvent {

		

	}

	public class GroundActorHeightChangeEventData {

		public GroundActorHeightChangeEventData( float oldHeight, float newHeight ) {
			OldHeight = oldHeight;
			NewHeight = newHeight;

			HeightDelta = NewHeight - OldHeight;
		}

		public float OldHeight;

		public float NewHeight;

		public float HeightDelta;

	}

}