﻿using System.Collections;
using UnityEngine;

namespace GamePausing {

	public class WaitForUnscaledSecondsAndPause : IEnumerator {

		private float _remainingTime;


		public WaitForUnscaledSecondsAndPause( float seconds ) {
			_remainingTime = seconds;
		}


		public bool MoveNext() {
			_remainingTime -= Time.unscaledDeltaTime;

			return !PauseManager.IsPaused && _remainingTime <= 0f;
		}

		public void Reset() { }

		public object Current {
			get { return null; }
		}
	}

}