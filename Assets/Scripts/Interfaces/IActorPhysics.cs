﻿using UnityEngine;


public interface IActorPhysics {

	/// <summary>
	/// Moves the actor into the direction of the given vector.
	/// </summary>
	void MoveInDirection( Vector3 direction );

	/// <summary>
	/// Moves the actor towards the position of the givenvector.
	/// </summary>
	void MoveTowardsPosition( Vector3 targetPosition );

	/// <summary>
	/// Rotates the actor around the Y-axis by n degrees.
	/// </summary>
	/// <param name="angle">The angle in degrees.</param>
	void RotateAroundY( float angle );

	/// <summary>
	/// Rotates the actor around to look into the given direction.
	/// </summary>
	/// <param name="direction">The direction to look into.</param>
	void LookInDirection( Vector3 direction );

	/// <summary>
	/// Applies a positive amount of vertical force to the actor if grounded, causing them to jump.
	/// </summary>
	/// <returns>Returns true if the actor jumped.</returns>
	bool Jump(float force);

}