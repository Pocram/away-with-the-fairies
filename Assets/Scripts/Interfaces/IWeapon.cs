﻿namespace Weapons {

	public interface IWeapon {

		void Fire();

		void Reload();

	}

}