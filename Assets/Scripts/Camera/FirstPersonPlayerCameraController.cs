﻿using UnityEngine;

namespace FirstPersonPlayerController {

	public class FirstPersonPlayerCameraController : MonoBehaviourExtended {

		#region Exposed Variables

		[SerializeField]
		private FirstPersonGroundActorController _controller;

		/// <summary>
		/// The camera's local position without any bobbing motions added to it.
		/// </summary>
		public Vector3 StabilisedLocalPosition {
			get {
				return _basePosition;
			}
		}

		/// <summary>
		/// The camera's world position without any bobbing motions added to it.
		/// </summary>
		public Vector3 StabilisedWorldPositon {
			get {
				return transform.parent.position + StabilisedLocalPosition;
			}
		}

		#region Looking

		[Header( "Looking" )]
		[SerializeField]
		private float _eyeDistanceFromTop = 0.125f;

		[SerializeField]
		private float _minLookingAngle = -90f;

		[SerializeField]
		private float _maxLookingAngle = 90f;

		#endregion

		#region Movement

		[SerializeField]
		private float _heightTransitionDuration = 5f;

		[SerializeField]
		private AnimationCurve _heightTransitionCurve = new AnimationCurve(
			new Keyframe(0f, 0f),
			new Keyframe(1f, 1f)
			);
		
		#endregion

		#endregion

		#region Private Variables

		private Vector3 _basePosition = new Vector3(0f, 1.83f, 0f);

		private float _currentLookingAngle;


		// Head height movement
		// --------------------------------
		private float _heightTransitionRemainder = 0f;

		private float _targetHeight = 0f;

		private float _currentHeight = 0f;

		private float _previousTargetHeight = 0f;

		private float _transitionStartingHeight = 0f;

		#endregion

		#region MonoBehaviour

		private void Start() {
			_targetHeight = _controller.ActorHeight;
			_currentHeight = _targetHeight;
			_previousTargetHeight = _currentHeight;
		}

		private void LateUpdate() {
			if (_controller == null || GamePausing.PauseManager.IsPaused)
				return;

			MoveHeadHeight();

			float finalHeight = _currentHeight - _eyeDistanceFromTop;
			_basePosition = new Vector3( 0f, finalHeight, 0f );



			transform.localPosition = _basePosition;
		}

		#endregion

		#region Private methods

		/// <summary>
		/// Handles head height and tweening between head heights.
		/// </summary>
		private void MoveHeadHeight() {
			_targetHeight = _controller.ActorHeight;

			// Detect whether the target height changed
			if (_previousTargetHeight != _targetHeight) {
				// Restart the animation
				_heightTransitionRemainder = _heightTransitionDuration;
				_transitionStartingHeight = _currentHeight;
			}

			// If the animation is finished and no change is in order, save on performance
			if (_heightTransitionRemainder == 0f) {
				return;
			}

			// Count down the remainer and clamp it between 0 and the max duration
			_heightTransitionRemainder -= Time.deltaTime;
			_heightTransitionRemainder = Mathf.Clamp( _heightTransitionRemainder, 0f, _heightTransitionDuration );

			// Get the respective value of the animation curve
			float transitionPercentage = 1f - ( _heightTransitionRemainder / _heightTransitionDuration );
			transitionPercentage = Mathf.Clamp01( transitionPercentage );
			float transitionCurveValue = _heightTransitionCurve.Evaluate( transitionPercentage );

			// Get the new position
			// Yes it does
			_currentHeight = Mathf.LerpUnclamped( _transitionStartingHeight, _targetHeight, transitionCurveValue );

			_previousTargetHeight = _targetHeight;
		}

		#endregion

		public void RotateUpDownByAngle( float angle ) {
			_currentLookingAngle = Mathf.Clamp( _currentLookingAngle + angle, _minLookingAngle, _maxLookingAngle );
			transform.localEulerAngles = new Vector3( _currentLookingAngle, transform.localEulerAngles.y, transform.localEulerAngles.z );
		}

	}

}