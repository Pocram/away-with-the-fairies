﻿using System.Collections.Generic;
using UnityEngine;

namespace Actors.Health {

	public class GenericActorHealth : MonoBehaviourExtended, IHealth, IRegionallyDamageable {

		#region Exposed variables

		[SerializeField]
		private float _maxHealthPoints = 100f;

		public float MaximumHealthPoints {
			get { return _maxHealthPoints; }
		}

		[SerializeField]
		private float _currentHealth = 100f;

		public float CurrentHealth {
			get { return _currentHealth; }
			private set {
				// Events
				var eventData = new GenericActorHealthHealthChangeEventData( _currentHealth, value );
				if (OnThisHealthChange != null) OnThisHealthChange( this, eventData );
				if (OnAnyHealthChange != null) OnAnyHealthChange( this, eventData );
				OnInternalHealthChange( eventData );

				_currentHealth = value;
			}
		}

		public float CurrentHealthPercentage {
			get { return Mathf.Clamp01( _currentHealth / _maxHealthPoints ); }
			set {
				value = Mathf.Clamp01( value );
				CurrentHealth = _maxHealthPoints * value;
			}
		}

		public bool IsAlive {
			get { return _currentHealth <= 0f; }
		}

		[SerializeField]
		private List<ColliderFloatPair> _damageRegions = new List<ColliderFloatPair>();

		#endregion

		#region Events

		public delegate void HealthChangeEventDelegate(GenericActorHealth o, GenericActorHealthHealthChangeEventData e);

		public delegate void DamageEventDelegate(GenericActorHealth o, GenericActorHealthDamageEventData e);

		public event HealthChangeEventDelegate OnThisHealthChange;

		public static event HealthChangeEventDelegate OnAnyHealthChange;

		public event DamageEventDelegate OnThisTakeDamage;

		public static event DamageEventDelegate OnAnyTakeDamage;

		#endregion

		#region Monobehaviour

		#endregion

		#region Private methods

		private void OnInternalHealthChange(GenericActorHealthHealthChangeEventData e) {
			if (e.NewHealth <= 0f) {
				gameObject.SetActive( false );
			}
		}

		private float GetDamageByRegion(float damage, Collider region) {
			// Compare each collider and find the fitting foo- Uh, collider.
			for (int i = 0; i < _damageRegions.Count; i++) {
				if (_damageRegions[i].Collider == region) {
					// Calculate new damage
					damage *= _damageRegions[i].Value;
					return damage;
				}
			}

			return damage;
		}

		#endregion

		#region Interfaces
		public void Heal(float healthPoints, GameObject sourceObject, bool canDamage = false) {
			if (canDamage && healthPoints < 0f)
				healthPoints = 0f;

			CurrentHealth += healthPoints;
		}

		public void Damage(float damagePoints, GameObject sourceObject, bool canHeal = false ) {
			if ( canHeal && damagePoints < 0f )
				damagePoints = 0f;

			GenericActorHealthDamageEventData eventData = new GenericActorHealthDamageEventData( CurrentHealth, CurrentHealth - damagePoints, sourceObject );

			CurrentHealth -= damagePoints;

			if (OnAnyTakeDamage != null) OnAnyTakeDamage( this, eventData );
			if (OnThisTakeDamage != null) OnThisTakeDamage( this, eventData );
		}

		public void Damage( float damagePoints, Collider hitCollider, GameObject sourceObject, bool canHeal = false ) {
			float regionalDamage = GetDamageByRegion( damagePoints, hitCollider );
			Damage( regionalDamage, sourceObject, canHeal );
		}
		#endregion

		#region Public types

		public class GenericActorHealthHealthChangeEventData {

			public GenericActorHealthHealthChangeEventData(float prevHealth, float newHealth) {
				PreviousHealth = prevHealth;
				NewHealth = newHealth;

				HealthDelta = NewHealth - PreviousHealth;
			}

			public float HealthDelta;
			public float PreviousHealth;
			public float NewHealth;

		}

		public class GenericActorHealthDamageEventData {

			public GenericActorHealthDamageEventData(float previousHealth, float newHealth, GameObject sourceObject) {
				HealthChange = new GenericActorHealthHealthChangeEventData( previousHealth, newHealth );
				SourceObject = sourceObject;
			}

			public GenericActorHealthHealthChangeEventData HealthChange;

			public GameObject SourceObject;

		}

		#endregion
	}

}