﻿using System;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace Dialogue.Inspector {

	[CustomEditor(typeof (DialogueNode))]
	public sealed class DialogueNodeInspector : Editor {

		#region Private properties

		private DialogueNode _node;

		// Message
		// ============================================================
		private bool _showMessage = true;


		// Dialogue options
		// ============================================================
		private bool _showOptions = true;

		private ReorderableList _dialogueOptionsList;

		#endregion


		#region MonoBehaviour

		private void OnEnable() {
			InitDialogueOptions();
		}

		#endregion


		public override void OnInspectorGUI() {
			_node = (DialogueNode) target;
			if (target == null) {
				EditorGUILayout.HelpBox( "Component not found!", MessageType.Error );
				return;
			}

			DrawNodeInspector( _node );
			DisplayMessageUI( _node );

		}

		private void DrawNodeInspector(DialogueNode node) {
			DrawMessageInspector( node );
			EditorGUILayout.Space();
			DrawOptionsList( node );
			EditorGUILayout.Space();
			DrawNextNodeField( node );
		}


		private void DrawMessageInspector(DialogueNode node) {
			GUIContent messageTitleContent = new GUIContent("Message");
			_showMessage = EditorGUILayout.Foldout( _showMessage, messageTitleContent, GUIStyleExtension.FoldoutBold );
				
			if (!_showMessage)
				return;

			int msgLines = 1;

			if (node.Message != null) {
				msgLines = node.Message.Split( '\n' ).Length;
			}


			// SpeakerName
			GUIContent titleContent = new GUIContent("Speaker", "Shown in the message's title bar.");
			node.SpeakerName = EditorGUILayout.TextField( titleContent, node.SpeakerName );

			// Box
			GUIContent msgBoxContent = new GUIContent("Message", "The message displayed in the dialogue message box.");
			EditorGUILayout.LabelField( msgBoxContent );
			node.Message = EditorGUILayout.TextArea( node.Message, GUILayout.Height( msgLines * 11f + msgLines * 1.5f + 8f ) );

			// Char info
			EditorGUILayout.Space();
			EditorGUILayout.LabelField( "Character Animation Per Line", EditorStyles.boldLabel );

			Array.Resize( ref node.CharacterAnimationInfo, msgLines );
			for (int i = 0; i < msgLines; i++) {
				DrawCharacterInformation( node, i );
			}
		}

		private void DrawCharacterInformation(DialogueNode node, int line) {
			if (node.Message == null)
				return;

			if(node.CharacterAnimationInfo[line] == null)
				return;

			EditorGUILayout.BeginVertical( "Box" );

			string[] msgLines = node.Message.Split( '\n' );

			if(line < msgLines.Length && msgLines[line] != null)
				EditorGUILayout.LabelField( msgLines[line] );
			else
				EditorGUILayout.LabelField( " " );

			EditorGUILayout.Space();

			GUIContent lineDelayContent = new GUIContent( "Line Delay", "The delay of the whole line in seconds." );
			node.CharacterAnimationInfo[line].DelayOfLine = EditorGUILayout.FloatField( lineDelayContent, node.CharacterAnimationInfo[line].DelayOfLine );

			GUIContent delayContent = new GUIContent( "Character Delay", "The delay after each character in seconds." );
			node.CharacterAnimationInfo[line].CharacterDelay = EditorGUILayout.FloatField( delayContent, node.CharacterAnimationInfo[line].CharacterDelay );

			GUIContent fadeTimeContent = new GUIContent( "Fade Time", "The duration of each character's fade animation in seconds." );
			node.CharacterAnimationInfo[line].CharacterFadeTime = EditorGUILayout.FloatField( fadeTimeContent, node.CharacterAnimationInfo[line].CharacterFadeTime );

			GUIContent fadeAnimationContent = new GUIContent( "Fade In Animation", "The alpha value of each character over time." );
			node.CharacterAnimationInfo[line].CharacterFadeAnimationCurve = EditorGUILayout.CurveField( fadeAnimationContent,
				node.CharacterAnimationInfo[line].CharacterFadeAnimationCurve, Color.white, new Rect( 0f, 0f, 1f, 1f ) );

			GUIContent colorContent = new GUIContent("Color");
			node.CharacterAnimationInfo[line].LineColor = EditorGUILayout.ColorField( colorContent, node.CharacterAnimationInfo[line].LineColor );

			EditorGUILayout.EndVertical();
		}


		#region DialogueOptionsList

		private void InitDialogueOptions() {
			_dialogueOptionsList = new ReorderableList( serializedObject, serializedObject.FindProperty( "DialogueOptions" ), true, true, true, true );

			_dialogueOptionsList.drawHeaderCallback = ( Rect rect ) => {
				EditorGUI.LabelField( rect, "Dialogue Options" );
			};

			_dialogueOptionsList.drawElementCallback = DrawDialogueOptionsListElement;

			_dialogueOptionsList.elementHeight = ( 2f * EditorGUIUtility.singleLineHeight ) + ( 1f * EditorGUIUtility.standardVerticalSpacing ) + 2f + EditorGUIUtility.standardVerticalSpacing;
		}

		private void DrawOptionsList( DialogueNode node ) {
			GUIContent messageTitleContent = new GUIContent( "Dialogue Options (" + node.DialogueOptions.Count + ")" );
			_showOptions = EditorGUILayout.Foldout( _showOptions, messageTitleContent, GUIStyleExtension.FoldoutBold );

			if ( !_showOptions )
				return;

			serializedObject.Update();
			_dialogueOptionsList.DoLayoutList();
			serializedObject.ApplyModifiedProperties();

			if (node.DialogueOptions.Count > 3) {
				EditorGUILayout.HelpBox( "There are more than 3 dialogue options!\nExcess options will not be used in the game.", MessageType.Warning );
			}
		}

		private void DrawDialogueOptionsListElement( Rect rect, int index, bool isActive, bool isFocused ) {
			rect.y += 2;

			float lineWidth = EditorGUIUtility.currentViewWidth - 50f;

			// Text box
			Rect textBoxRect = new Rect( rect.x, rect.y, lineWidth, EditorGUIUtility.singleLineHeight );
			_node.DialogueOptions[index].Text = EditorGUI.TextField( textBoxRect, _node.DialogueOptions[index].Text );

			// Next node
			Rect nodeRect = new Rect( rect.x, rect.y + textBoxRect.height + EditorGUIUtility.standardVerticalSpacing, lineWidth - 43f, EditorGUIUtility.singleLineHeight );
			GUIContent nextNodeContent = new GUIContent( "Next Node" );
			_node.DialogueOptions[index].NextNode = (DialogueNode) EditorGUI.ObjectField( nodeRect, nextNodeContent, _node.DialogueOptions[index].NextNode, typeof( DialogueNode ), false );

			// New node
			GUIContent newNodeContent = new GUIContent( "New", "Creates a new node \"" + _node.DialogueOptions[index].Text + "_Node" + "\" and marks it as the next node." );
			Rect buttonRect = new Rect( rect.x + nodeRect.width + 4f, nodeRect.y, lineWidth - nodeRect.width - 4f, EditorGUIUtility.singleLineHeight );
			if ( GUI.Button( buttonRect, newNodeContent ) ) {
				DialogueNode newNode = DialogueNode.CreateNewNode( _node.DialogueOptions[index].Text + "_Node" );
				_node.DialogueOptions[index].NextNode = newNode;
			}
		}

		#endregion


		private void DrawNextNodeField(DialogueNode node) {
			if (node.DialogueOptions.Count > 0)
				return;

			EditorGUILayout.LabelField( "On Finish", EditorStyles.boldLabel );

			GUIContent nextNodeContent = new GUIContent("Next Node");
			node.NextNode = (DialogueNode) EditorGUILayout.ObjectField( nextNodeContent, node.NextNode, typeof (DialogueNode), false );

			DrawCreateNewNodeButton( ref node.NextNode );
		}

		private void DrawCreateNewNodeButton(ref DialogueNode node) {
			if (GUILayout.Button( "Create New" )) {
				node = DialogueNode.CreateNewNode( _node.name + "_next_Node" );
			}
		}


		private void DisplayMessageUI(DialogueNode node) {
			if (DialogueSystemUiManager.Instance == null)
				return;

			if ( DialogueSystemUiManager.Instance.TitleTextComponent != null )
				DialogueSystemUiManager.Instance.TitleTextComponent.text = node.SpeakerName;

			if (DialogueSystemUiManager.Instance.DialogeMessageTextComponent != null)
				DialogueSystemUiManager.Instance.DialogeMessageTextComponent.text = node.Message;

			if (DialogueSystemUiManager.Instance.DialogueOptionsPanel != null) {
				DialogueSystemUiManager.Instance.DialogueOptionsPanel.SetAmountOfOptions( node.DialogueOptions.Count );

				for (int i = 0; i < node.DialogueOptions.Count; i++) {
					if (i >= DialogueSystemUiManager.Instance.DialogueOptionsPanel.DialogueOptionUiObjects.Length)
						break;

					DialogueSystemUiManager.Instance.DialogueOptionsPanel.DialogueOptionUiObjects[i].UiTextElement.text = node.DialogueOptions[i].Text;
				}
			}
		}

	}

}