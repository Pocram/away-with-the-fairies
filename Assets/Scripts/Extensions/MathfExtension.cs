﻿using UnityEngine;

public static class MathfExtension {

	public static int ClampInt(int value, int min, int max) {
		if (value < min)
			return min;

		if (value > max)
			return max;

		return value;
	}

	public static int Sum(int upToNumber) {
		int num = 0;

		for (int i = 0; i <= upToNumber; i++) {
			num += i;
		}

		return num;
	}

} 