﻿using System.Collections;

namespace GamePausing {

	public class WaitIfPaused : IEnumerator {

		public bool MoveNext() {
			return !PauseManager.IsPaused;
		}

		public void Reset() {}

		public object Current {
			get { return null; }
		}
	}

}