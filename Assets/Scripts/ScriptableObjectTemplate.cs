﻿using System.IO;

using UnityEditor;
using UnityEngine;

public sealed class ScriptableObjectTemplate : ScriptableObject {

	#region Object creation

	// Menu item to create new node
	[MenuItem( "Assets/Create/New Scriptable Object" )]
	public static ScriptableObjectTemplate CreateNewNode() {
		return CreateNewNode( "NewQuest" );
	}

	public static ScriptableObjectTemplate CreateNewNode( string name ) {
		string folder = "ScriptableObjects/Quests";
		string fileName = name;

		// Create directory if needed
        if (!Directory.Exists( Application.dataPath + "/" + folder )) {
			Directory.CreateDirectory( Application.dataPath + "/" + folder );
		}

		folder = "Assets/" + folder;

		ScriptableObjectTemplate newAsset = ScriptableObject.CreateInstance<ScriptableObjectTemplate>();

		int totalObjectsByThatName = AssetDatabase.FindAssets( fileName, new[] { folder } ).Length;
		// No other objects by that name found
		if ( totalObjectsByThatName == 0 ) {
			AssetDatabase.CreateAsset( newAsset, folder + "/" + fileName + ".asset" );
			AssetDatabase.SaveAssets();

			EditorUtility.FocusProjectWindow();

			Selection.activeObject = newAsset;
		} else {
			// Other objects by that name exist!
			// Count up; just read the code it's easy
			int id = 1;
			while ( AssetDatabase.FindAssets( fileName + " (" + id + ")", new[] { folder } ).Length != 0 )
				id++;

			AssetDatabase.CreateAsset( newAsset, folder + "/" + fileName + " (" + id + ").asset" );
			AssetDatabase.SaveAssets();

			EditorUtility.FocusProjectWindow();

			Selection.activeObject = newAsset;
		}
		return newAsset;
	}

	#endregion

}