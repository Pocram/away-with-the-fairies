﻿using UnityEngine;


public static class Vector3Extension {

	/// <summary>
	/// Flattens the vector, setting its y-component to 0.
	/// </summary>
	public static Vector3 ToFlat( this Vector3 v ) {
		return new Vector3(v.x, 0f, v.z);
	}


	/// <summary>
	/// Sets the vector's magnitude to the given value.
	/// </summary>
	public static Vector3 SetMagnitude( this Vector3 v, float magnitude ) {
		return v.normalized * magnitude;
	}

	/// <summary>
	/// Increases the vector's magnitude by the given amount.
	/// </summary>
	public static Vector3 AddMagnitude( this Vector3 v, float magnitude ) {
		return v.SetMagnitude( v.magnitude + magnitude );
	}

	/// <summary>
	/// Multiplies the vector's magnitude by the given amount.
	/// </summary>
	public static Vector3 MultiplyMagnitude( this Vector3 v, float magnitude ) {
		return v.SetMagnitude( v.magnitude * magnitude );
	}

}