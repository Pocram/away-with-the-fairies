﻿using UnityEngine;

namespace FirstPersonPlayerController {

	[AddComponentMenu("Pigbin/Controllers/Player Input")]
	public sealed class PlayerInput : MonoBehaviourExtended {

		#region Exposed fields

		public static int InputBlockers = 0;

		#endregion

		#region Input keys

		[Header( "Input keys" )]
		[SerializeField]
		private KeyCode[] _walkForwardKeys = new KeyCode[] {KeyCode.W, KeyCode.UpArrow};

		[SerializeField]
		private KeyCode[] _walkBackwardsKeys = new KeyCode[] {KeyCode.S, KeyCode.DownArrow};

		[SerializeField]
		private KeyCode[] _strafeLeftKeys = new KeyCode[] {KeyCode.A, KeyCode.LeftArrow};

		[SerializeField]
		private KeyCode[] _strafeRights = new KeyCode[] {KeyCode.D, KeyCode.RightArrow};

		[SerializeField]
		private KeyCode _sprintKey = KeyCode.LeftShift;

		[SerializeField]
		private KeyCode _jumpKey = KeyCode.Space;


		[Space(10f)]
		[SerializeField]
		private KeyCode _crouchKey = KeyCode.Q;

		[SerializeField]
		private KeyCode _proneKey = KeyCode.R;

		#endregion

		#region Targets

		[Header( "The controller" )]
		[SerializeField]
		private FirstPersonGroundActorController _actorController;

		[SerializeField]
		private LocationSpace _locationSpace = LocationSpace.Local;


		#endregion

		#region Private fields

		// Input fields
		private Vector3 _playerMovementInput = Vector3.zero;

		private Vector2 _cameraMovementInput = Vector3.zero;

		private bool _doJump = false;

		private bool _doSprint = false;

		private bool _doCrouch = false;

		private bool _doProne = false;

		#endregion

		#region MonoBehaviour

		private void Update() {
			if (GamePausing.PauseManager.IsPaused)
				return;

			ResetInput();

			if(InputBlockers == 0)
				GetKeyboardMouseInput();
			else if (InputBlockers < 0) {
				Debug.LogError( "InputBlockers value went below 0!" );
				InputBlockers = 0;
			}

			ApplyInput();
		}

		#endregion

		#region Input

		/// <summary>
		/// Resets the input fields to their zero-values to prevent the input from being saved indefinitely.
		/// Call this every frame.
		/// </summary>
		private void ResetInput() {
			_playerMovementInput = Vector3.zero;
			_cameraMovementInput = Vector2.zero;

			_doJump = false;
			_doSprint = false;

			_doCrouch = false;
			_doProne = false;

		}

		/// <summary>
		/// Fetches the input of the mouse and keyboard.
		/// Call this every frame.
		/// </summary>
		private void GetKeyboardMouseInput() {
			// Movement
			if ( _walkForwardKeys.HasKey() )
				_playerMovementInput += new Vector3(0f, 0f, 1f);
			else if ( _walkBackwardsKeys.HasKey() )
				_playerMovementInput += new Vector3( 0f, 0f, -1f );

			if ( _strafeLeftKeys.HasKey() )
				_playerMovementInput += new Vector3( -1f, 0f, 0f );
			else if ( _strafeRights.HasKey() )
				_playerMovementInput += new Vector3( 1f, 0f, 0f );


			// Sprint
			if (Input.GetKey( _sprintKey ))
				_doSprint = true;

			// Jump
			if ( Input.GetKeyDown( _jumpKey ) )
				_doJump = true;

			// Cower
			if (Input.GetKeyDown( _crouchKey ))
				_doCrouch = true;

			// Lie down
			if ( Input.GetKeyDown( _proneKey ) )
				_doProne = true;


			// Camera
			float inputX = Input.GetAxis( "Mouse X" );
			float inputY = Input.GetAxis( "Mouse Y" );
			Vector2 mouseInput = new Vector2( inputX, inputY );
			_cameraMovementInput = mouseInput;
		}

		/// <summary>
		/// Sends the input to the respective classes.
		/// </summary>
		private void ApplyInput() {
			// Actor
			_actorController.MoveInDirection( _playerMovementInput.normalized, _locationSpace, _doSprint );
			_actorController.RotateAndLook( _cameraMovementInput );

			if (_doJump) {
				if (_actorController.Jump())
					return;
			}

			if (_doCrouch) {
				_actorController.Crouch();
			}

			if (_doProne) {
				_actorController.GoProne();
			}

		}

		#endregion

	}

}