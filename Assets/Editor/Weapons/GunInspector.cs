﻿using System;
using UnityEngine;
using UnityEditor;

using Weapons.Guns;

[CustomEditor(typeof(Gun))]
[CanEditMultipleObjects]
public class GunInspector : Editor {

	private float _sprayDeviationSlider = 1f;

	public override bool HasPreviewGUI() {
		return true;
	}

	public override void DrawPreview( Rect previewArea ) {
		Gun gun = (Gun) target;

		previewArea.position = new Vector2( previewArea.x + 5f, previewArea.y + 5f );
		previewArea.width -= 10f;
		previewArea.height -= 10f;


		// General info
		// ==========================================================================================
		Rect infoRect = previewArea;
		infoRect.height = 35f;

		string infoText = "<b>Bullet objects</b>:\t" + gun.GetMaximumAmountOfSimultaneousBullets();
		infoText += "\n<b>Max bullet lifetime</b>:\t" + Math.Round( gun.MaxBulletDistance / gun.BulletSpeed, 2 );
		GUI.Label( infoRect, infoText, GUIStyleExtension.PreviewLabel );


		// Spray deviation
		// ==========================================================================================
		Rect sliderRectTitle = infoRect;
		sliderRectTitle.Set( 5f, infoRect.y + infoRect.height + 10f, sliderRectTitle.width, 15f );

		float deviationSliderDistance = (float) Math.Round( _sprayDeviationSlider * gun.MaxBulletDistance, 2 );

		float finalDeviation = deviationSliderDistance * Mathf.Tan( gun.BulletSprayAngleRange * Mathf.Deg2Rad );
		// Add gravity
		float lifetime = gun.MaxBulletDistance / gun.BulletSpeed;
		float gravityEffect =  Gun.BulletGravity / 2f * Mathf.Pow( lifetime * _sprayDeviationSlider, 2f );

		finalDeviation -= gravityEffect;
		finalDeviation = (float) Math.Round( finalDeviation, 2 );
		gravityEffect = (float) Math.Round( gravityEffect, 2 );
		gravityEffect = Mathf.Abs( gravityEffect );

		// Slider
		// ------------------
		string deviationSliderTitle = "<b>Max spray deviation</b> at " + deviationSliderDistance + " meters: ";
		GUI.Label( sliderRectTitle, deviationSliderTitle, GUIStyleExtension.PreviewLabel );

		Rect sliderRect = sliderRectTitle;
		sliderRect.Set( sliderRect.x, sliderRectTitle.y + sliderRectTitle.height, sliderRect.width, 10f );
		_sprayDeviationSlider = GUI.HorizontalSlider( sliderRect, _sprayDeviationSlider, 0f, 1f );

		// Result
		// ------------------
		Rect resultRect = sliderRect;
		resultRect.Set( resultRect.x, resultRect.y + resultRect.height + 5f, resultRect.width, 40f );

		string deviationResult = finalDeviation.ToString();
		deviationResult += finalDeviation != 1f ? " meters" : " meter";
		deviationResult += " at " + gun.BulletSprayAngleRange + "° of spray";
		deviationResult += "\nIncluded effect of gravity: " + gravityEffect;
		deviationResult += gravityEffect != 1f ? " meters" : " meter";

		GUI.Label( resultRect, deviationResult, GUIStyleExtension.PreviewLabel );

	}

}