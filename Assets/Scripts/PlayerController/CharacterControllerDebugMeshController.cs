﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CharacterControllerDebugMeshController : MonoBehaviour {

	[SerializeField]
	private CharacterController _characterController;

	private void Update() {
		if (_characterController == null)
			return;

		transform.localPosition = new Vector3( transform.localPosition.x, _characterController.center.y, transform.localPosition.z );
		transform.localScale = new Vector3( _characterController.radius * 2f, _characterController.height / 2f, _characterController.radius * 2f );
	}

}