﻿using ObjectInteraction;
using UnityEngine;
using UnityEngine.UI;

public class ObjectInteractionUi : MonoBehaviour {

	private PlayerInteractionManager _interactionManager;

	[SerializeField]
	private Text _interactionPrompTextComponent;


	private void Start() {
		_interactionManager = FindObjectOfType<PlayerInteractionManager>();
	}

	private void Update() {
		if (_interactionManager == null || _interactionPrompTextComponent == null)
			return;

		_interactionPrompTextComponent.text = _interactionManager.PromptOfTarget;
	}

}