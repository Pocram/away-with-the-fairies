﻿using UnityEngine;
using System.Collections.Generic;
using UnityEditor;

[System.Serializable]
public class ObjectPool<T> where T : MonoBehaviour {

	public List<T> Pool { get; set; }

	private Transform _folderObject;

	private GameObject _prefab;


	private int _currentObjectIndex = 0;


	public int Count {
		get { return Pool.Count; }
	}

	public ObjectPool(Transform folderObject, int count) {
		Pool = new List<T>();
		_folderObject = folderObject;

		Resize( count );
	}

	public ObjectPool( Transform folderObject, GameObject prefab, int count ) {
		Pool = new List<T>();
		_folderObject = folderObject;
		_prefab = prefab;

		Resize( count );
	}


	public void Resize(int size) {
		for ( int i = Pool.Count; i < size; i++ ) {
			GameObject newObject;

			if (_prefab == null) {
				newObject = new GameObject();
				newObject.AddComponent<T>();

				_prefab = newObject;
			}
			else {
				newObject = Object.Instantiate( _prefab );
			}
			
			newObject.name = typeof( T ).Name + " " + i;
			newObject.transform.SetParent( _folderObject );

			T component = newObject.GetComponent<T>();
			Pool.Add( component );

			newObject.SetActive( false );
		}
	}

	public T GetNext() {
		if (Pool.Count == 0)
			return null;

		if (Pool.Count == 1)
			return Pool[0];

		_currentObjectIndex++;
		if (_currentObjectIndex >= Pool.Count)
			_currentObjectIndex = 0;

		return Pool[_currentObjectIndex];
	}

	public T GetPrevious() {
		if ( Pool.Count == 0 )
			return null;

		if ( Pool.Count == 1 )
			return Pool[0];

		int index = _currentObjectIndex - 1;

		if (index < 0)
			_currentObjectIndex = Pool.Count - 1;

		return Pool[index];
	}


	public void SetActiveAll(bool isActive) {
		for (int i = 0; i < Pool.Count; i++) {
			Pool[i].gameObject.SetActive( isActive );
		}
	}
}