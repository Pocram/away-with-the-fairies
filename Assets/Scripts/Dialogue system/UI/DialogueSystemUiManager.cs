﻿using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.UI;

using GamePausing;

namespace Dialogue {

	public class DialogueSystemUiManager : MonoBehaviourExtended {

		#region Singleton

		private static DialogueSystemUiManager _instance;

		public static DialogueSystemUiManager Instance {
			get {
				if ( _instance == null ) {
					_instance = FindObjectOfType<DialogueSystemUiManager>();
				}

				return _instance;
			}
		}

		#endregion

		#region Exposed variables

		[Header("UI Elements")]
		[SerializeField]
		private GameObject _canvas;

		[SerializeField]
		private Text _titleTextComponent;

		public Text TitleTextComponent {
			get { return _titleTextComponent; }
		}

		[SerializeField]
		private Text _dialogueMessageTextComponent;

		public Text DialogeMessageTextComponent {
			get { return _dialogueMessageTextComponent; }
		}


		[Header("Dialogue Options UI Elements")]
		[SerializeField]
		private DialogueOptionsPanel _dialogueOptionsPanel;

		public DialogueOptionsPanel DialogueOptionsPanel {
			get { return _dialogueOptionsPanel; }
		}

		#endregion

		#region Events

		public delegate void MessageShownDelegate(DialogueNode node);

		public static event MessageShownDelegate OnMessageFullyShown;

		#endregion

		#region Private variables

		private List<float> _characterStartingTimes;

		private float _currentMessageDuration = 0f;

		private Coroutine _messageDisplayCoroutine;

		private bool _skipToEndOfMessage = false;

		#endregion

		#region MonoBehaviour

		private void Start() {
			_canvas.SetActive( false );

			// Subscribe to events
			DialogueManager.OnNodeStart += OnNewNode;
			DialogueManager.OnDialogueEnd += OnDialogueClose;
		}

		private void OnDisable() {
			// Unsubscribe
			DialogueManager.OnNodeStart -= OnNewNode;
			DialogueManager.OnDialogueEnd -= OnDialogueClose;
		}

		#endregion

		#region Private methods

		private void OnNewNode(DialogueNode node) {
			_canvas.SetActive( true );

			DialogueOptionsPanel.Hide();

			// Calculate the fade times just once
			_characterStartingTimes = CalculateCharacterFadeInTimes( node, out _currentMessageDuration );

			// Calculate null state and display it before the fading animation starts
			List<float> alphas = GetCurrentMessageAlphaValues( 0f, node, _characterStartingTimes );
			_dialogueMessageTextComponent.text = GetFadedMessage( node, alphas );

			// Set title
			_titleTextComponent.text = node.SpeakerName;

			// Set option texts
			for (int i = 0; i < node.DialogueOptions.Count; i++) {
				if (i >= DialogueOptionsPanel.DialogueOptionUiObjects.Length)
					break;

				DialogueOptionsPanel.DialogueOptionUiObjects[i].UiTextElement.text = node.DialogueOptions[i].Text;
			}

			// (Re)Start the animation
			if(_messageDisplayCoroutine != null)
				StopCoroutine( _messageDisplayCoroutine );
			_messageDisplayCoroutine = StartCoroutine( FadeMessageIn( node, _characterStartingTimes ) );
		}

		private void OnDialogueClose(DialogueNode node, DialogueManager.DialogueEndEventData data) {
			_canvas.SetActive( false );
			DialogueOptionsPanel.Hide();
		}

		/// <summary>
		/// Calculates the time in seconds it takes until a character starts fading in.
		/// </summary>
		private List<float> CalculateCharacterFadeInTimes(DialogueNode node, out float duration) {
			List<float> times = new List<float>( node.Message.Length );

			if (times.Capacity == 0) {
				duration = 0f;
				return times;
			}

			int currentLine = 0;
			float currentLineDelay = node.CharacterAnimationInfo[0].DelayOfLine;
			float currentCharacterDelay = 0f;

			for (int i = 0; i < times.Capacity; i++) {
				// Get current line
				if (node.Message[i] == '\n') {
					currentLine++;
					// Add the new line delay
					currentLineDelay += node.CharacterAnimationInfo[currentLine].DelayOfLine;
				}

				// Add them togezah
				float finalTime = currentLineDelay + currentCharacterDelay;
				times.Add( finalTime );

				currentCharacterDelay += node.CharacterAnimationInfo[currentLine].CharacterDelay;
			}

			duration = times[times.Count - 1] + node.CharacterAnimationInfo[currentLine].CharacterFadeTime;

			return times;
		}

		/// <summary>
		/// Calculates the messages' current fading state for each character for a given time.
		/// </summary>
		private List<float> GetCurrentMessageAlphaValues(float currentTime, DialogueNode node, List<float> characterFadeTimes) {
			if (characterFadeTimes == null) {
				characterFadeTimes = CalculateCharacterFadeInTimes( node, out _currentMessageDuration );
			}

			List<float> characterAlphaValues = new List<float>( characterFadeTimes.Count );

			int currentLine = 0;
			for (int i = 0; i < node.Message.Length; i++) {
				// Count the lines
				if (node.Message[i] == '\n') {
					currentLine++;
				}

				float currentLineAnimationTime = node.CharacterAnimationInfo[currentLine].CharacterFadeTime;
				float animationPercentage = Mathf.InverseLerp( characterFadeTimes[i], characterFadeTimes[i] + currentLineAnimationTime, currentTime );

				float alphaValue = node.CharacterAnimationInfo[currentLine].CharacterFadeAnimationCurve.Evaluate( animationPercentage );

				characterAlphaValues.Add( alphaValue );
			}

			return characterAlphaValues;
		}


		private string GetFadedMessage(DialogueNode node, List<float> currentAlphaValues) {
			string fadedMessage = string.Empty;

			int currentLine = 0;
			for (int i = 0; i < node.Message.Length; i++) {
				// Count the lines
				if ( node.Message[i] == '\n' ) {
					currentLine++;
				}

				// Fade color up to maximum specified in object and convert it to hex
				Color fadedColor = node.CharacterAnimationInfo[currentLine].LineColor;
				fadedColor.a = Mathf.Lerp( 0f, fadedColor.a, currentAlphaValues[i] );
				string hexColor = ColorUtility.ToHtmlStringRGBA( fadedColor );

				string richTextCharacter = "<color=#" + hexColor + ">" + node.Message[i] + "</color>" ;
				fadedMessage += richTextCharacter;
			}

			return fadedMessage;
		}

		private string GetFadedMessage( DialogueNode node, float currentTime, List<float> characterFadeTimes  ) {
			string fadedMessage = string.Empty;
			List<float> currentAlphaValues = GetCurrentMessageAlphaValues( currentTime, node, characterFadeTimes );

			int currentLine = 0;
			for ( int i = 0; i < node.Message.Length; i++ ) {
				// Count the lines
				if ( node.Message[i] == '\n' ) {
					currentLine++;
				}

				// Fade color up to maximum specified in object and convert it to hex
				Color fadedColor = node.CharacterAnimationInfo[currentLine].LineColor;
				fadedColor.a = Mathf.Lerp( 0f, fadedColor.a, currentAlphaValues[i] );
				string hexColor = ColorUtility.ToHtmlStringRGBA( fadedColor );

				string richTextCharacter = "<color=#" + hexColor + ">" + node.Message[i] + "</color>";
				fadedMessage += richTextCharacter;
			}

			return fadedMessage;
		}


		private IEnumerator FadeMessageIn(DialogueNode node, List<float> characterFadeTimes) {
			_skipToEndOfMessage = false;
			float currentTime = 0f;

			// Fade in the characters until all are visible
			while (currentTime <= _currentMessageDuration) {
				// Skip if the player wants to
				// The time condition prevents the text from skipping instantly
				if ( currentTime > 0.1f && _skipToEndOfMessage ) {
					currentTime = 2f * _currentMessageDuration;
				}

				// Get the text with the appropriate rich text format and display it
				string fadedMessage = GetFadedMessage( node, currentTime, characterFadeTimes );
				_dialogueMessageTextComponent.text = fadedMessage;

				_skipToEndOfMessage = false;
				currentTime += Time.smoothDeltaTime;
				yield return new WaitIfPaused();
			}

			// When done, show the options
			InitOptions( node );
			if (OnMessageFullyShown != null)
				OnMessageFullyShown( node );
		}


		private void InitOptions(DialogueNode node) {
			// If there are no options, show nothing
			if (node.DialogueOptions.Count == 0) {
				DialogueOptionsPanel.Hide();
				return;
			}

			DialogueOptionsPanel.Show();
			DialogueOptionsPanel.SetAmountOfOptions( node.DialogueOptions.Count );
			DialogueOptionsPanel.SelectOption( 0, true );
		}

		#endregion

		#region Public methods

		public void SkipAnimation() {
			_skipToEndOfMessage = true;
		}

		#endregion

	}

}