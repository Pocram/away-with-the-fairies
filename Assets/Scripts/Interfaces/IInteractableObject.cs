﻿using UnityEngine;

namespace ObjectInteraction {

	public interface IInteractableObject {

		// UI
		// ======================================
		string ObjectName { get; set; }

		string GetInteractionPromptText();

		GameObject GetGameObject();



		// Interaction
		// ======================================
		void OnInteractionEnter();

		void OnInteractionStay();

		void OnInteractionExit();

	}

}