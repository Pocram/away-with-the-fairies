﻿using UnityEngine;
using UnityEditor;

using UnityEditorInternal;

namespace Quests {

	[CustomEditor(typeof (Quest))]
	public class QuestInspector : Editor {

		#region Private fields

		private Quest _quest;

		// Foldout groups
		private bool _showQuestInfo = true;
		private bool _showMainObjective = true;
		private bool _showSubObjectives = true;


		// Lists
		private SerializedObject _serializedMainObjective;
		private SerializedProperty _serializedSolutionsProperty;

		private ReorderableList _mainObjectiveSolutiosList;

		private SerializedProperty _serializedSubObjectives;

		private ReorderableList _subObjectiveList;

		#endregion

		#region MonoBehaviour

		private void OnEnable() {
			_quest = (Quest) target;

			if (_quest.MainObjective != null) {
				_serializedMainObjective = new SerializedObject( _quest.MainObjective );
				_serializedSolutionsProperty = _serializedMainObjective.FindProperty( "Solutions" );
				_mainObjectiveSolutiosList = new ReorderableList( _serializedMainObjective, _serializedSolutionsProperty, true, true, true, true );

				// Config list
				ResizeSolutionsListElements( ref _mainObjectiveSolutiosList, _quest.MainObjective );
				_mainObjectiveSolutiosList.drawHeaderCallback = QuestObjectiveInspector.DrawObjectiveSolutionsListHeader;
				_mainObjectiveSolutiosList.drawElementCallback += OnDrawMainObjectiveSolutionListElement;
			}


			// Sub objectives
			_serializedSubObjectives = serializedObject.FindProperty( "SubObjectives" );
			_subObjectiveList = new ReorderableList( serializedObject, _serializedSubObjectives, true, true, true, true );

			// Config list
			int lines = 3;
			_subObjectiveList.elementHeight = EditorGUIUtility.singleLineHeight * lines + EditorGUIUtility.standardVerticalSpacing * lines;
			_subObjectiveList.drawHeaderCallback = rect => EditorGUI.LabelField( rect, "Sub Objectives" );
			_subObjectiveList.drawElementCallback = OnDrawSubObjectiveListElement;
		}

		#endregion

		private void ResizeSolutionsListElements(ref ReorderableList list, QuestObjective objective) {
			if (list == null)
				return;

			int elementLines = 0;
			for ( int i = 0; i < objective.Solutions.Count; i++ ) {
				if ( objective.Solutions[i].GetAmountOfUiElements() > elementLines )
					elementLines = objective.Solutions[i].GetAmountOfUiElements();
			}
			elementLines += 3;

			list.elementHeight = EditorGUIUtility.singleLineHeight * elementLines + EditorGUIUtility.standardVerticalSpacing * (elementLines + 4);
		}

		#region Drawers

		public override void OnInspectorGUI() {
			if (_quest == null) {
				EditorGUILayout.HelpBox( "Something went wrong! The quest could not be found!", MessageType.Error );
				return;
			}

			DrawQuestInfo();
			EditorGUILayout.Space();
			DrawMainObjective();
			EditorGUILayout.Space();
			DrawSubObjectiveList();
		}

		private void DrawQuestInfo() {
			GUIContent showQuestInfoContent = new GUIContent( "Quest Info" );
			_showQuestInfo = EditorGUILayout.Foldout( _showQuestInfo, showQuestInfoContent, GUIStyleExtension.FoldoutBold );

			if (!_showQuestInfo)
				return;

			GUIContent questNameContent = new GUIContent( "Name", "The quest's name as can be seen in the UI." );
			_quest.Name = EditorGUILayout.TextField( questNameContent, _quest.Name );

			GUIContent questDescLongContent = new GUIContent( "Description", "The description as can be seen in the pause menu." );
			EditorGUILayout.LabelField( questDescLongContent );

			int questDescLongLines = 1;
			if(_quest.Description != null)
				questDescLongLines = _quest.Description.Split( '\n' ).Length;

			if (questDescLongLines < 3)
				questDescLongLines = 3;
			_quest.Description = EditorGUILayout.TextArea( _quest.Description, GUILayout.Height( 12f * questDescLongLines + 2f * questDescLongLines + 2f ) );
		}

		private void DrawMainObjective() {
			GUIContent showMainObjectiveContent = new GUIContent( "Main Objective" );
			_showMainObjective = EditorGUILayout.Foldout( _showMainObjective, showMainObjectiveContent, GUIStyleExtension.FoldoutBold );

			if ( !_showMainObjective )
				return;

			GUIContent mainObjectiveContent = new GUIContent( "Main Objective" );
			_quest.MainObjective = (QuestObjective) EditorGUILayout.ObjectField( mainObjectiveContent, _quest.MainObjective, typeof (QuestObjective), false );

			if (_quest.MainObjective == null)
				return;

			QuestObjectiveInspector.DrawObjectiveInfoLayout( ref _quest.MainObjective );

			EditorGUILayout.Space();

			DrawMainObjectiveSolutions();
		}

		private void DrawSubObjectiveList() {
			GUIContent showSubObjectivesContent = new GUIContent( "Sub Objectives" );
			_showSubObjectives = EditorGUILayout.Foldout( _showSubObjectives, showSubObjectivesContent, GUIStyleExtension.FoldoutBold );

			if ( !_showSubObjectives )
				return;

			serializedObject.Update();
			_subObjectiveList.DoLayoutList();
			serializedObject.ApplyModifiedProperties();
		}

		private void OnDrawSubObjectiveListElement(Rect rect, int index, bool isActive, bool isFocused) {
			rect.y += 2f;

			SerializedProperty element = _serializedSubObjectives.GetArrayElementAtIndex( index );

			// Object
			GUIContent objectContent = new GUIContent( "Objective" );
			rect = new Rect( rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight );
			EditorGUI.ObjectField( rect, element, objectContent );

			if (_quest.SubObjectives[index] == null)
				return; // miriam&marco4eva

			// Description
			GUIContent descrContent = new GUIContent( "Description" );
			rect = new Rect( rect.x, rect.yMax + EditorGUIUtility.standardVerticalSpacing, rect.width, EditorGUIUtility.singleLineHeight );
			_quest.SubObjectives[index].Description = EditorGUI.TextField( rect, descrContent, _quest.SubObjectives[index].Description );

			// Solutions
			GUIContent solutionsContent = new GUIContent("Soultions");
			rect = new Rect( rect.x, rect.yMax + EditorGUIUtility.standardVerticalSpacing, rect.width, EditorGUIUtility.singleLineHeight );
			EditorGUI.LabelField( rect, solutionsContent, new GUIContent( _quest.SubObjectives[index].Solutions.Count.ToString() ) );
		}

		#region DrawObjective

		private void DrawMainObjectiveSolutions() {
			if (_quest.MainObjective == null)
				return;

			ResizeSolutionsListElements( ref _mainObjectiveSolutiosList, _quest.MainObjective );

			_serializedMainObjective.Update();
			_mainObjectiveSolutiosList.DoLayoutList();
			_serializedMainObjective.ApplyModifiedProperties();
		}

		private void OnDrawMainObjectiveSolutionListElement(Rect rect, int index, bool isActive, bool isFocused) {
			rect.y += 2f;

			SerializedProperty element = _serializedSolutionsProperty.GetArrayElementAtIndex( index );
			QuestObjectiveInspector.DrawSolutionElementList( rect, index, isActive, isFocused, _mainObjectiveSolutiosList, element );
		}

		#endregion

		#endregion

	}

}