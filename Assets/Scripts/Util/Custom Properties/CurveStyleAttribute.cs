﻿using UnityEngine;

public class CurveStyleAttribute : PropertyAttribute {

	public CurveStyleAttribute( CurveColor color ) {
		SetColor( color );
		HasRect = false;
	}

	public CurveStyleAttribute( float x, float y, float width, float height ) {
		Color = Color.green;

		CurveRect = new Rect( x, y, width, height );
		HasRect = true;
	}

	public CurveStyleAttribute( CurveColor color, float x, float y, float width, float height) {
		SetColor( color );

		CurveRect = new Rect( x, y, width, height );
		HasRect = true;
	}


	public Color Color { get; private set; }
	public Rect CurveRect { get; private set; }

	public bool HasRect { get; private set; }


	private void SetColor( CurveColor color ) {
		switch ( color ) {
			case CurveColor.Red:
				Color = Color.red;
				break;
			case CurveColor.Green:
				Color = Color.green;
				break;
			case CurveColor.Blue:
				Color = Color.blue;
				break;
			case CurveColor.Black:
				Color = Color.black;
				break;
			case CurveColor.White:
				Color = Color.white;
				break;
		}
	}


	public enum CurveColor {

		Red,
		Green,
		Blue,
		White,
		Black

	}

}