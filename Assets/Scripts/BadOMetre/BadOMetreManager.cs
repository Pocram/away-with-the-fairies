﻿using UnityEngine;

public class BadOMetreManager : MonoBehaviourExtended {

	#region Singleton

	private static BadOMetreManager _instance;

	public static BadOMetreManager Instance
	{
		get
		{
			if ( _instance == null ) {
				_instance = FindObjectOfType<BadOMetreManager>();

				// If no instance is found, create one
				if ( _instance == null ) {
					GameObject newGo = new GameObject();
					newGo.name = "BadOMetreManager";
					newGo.AddComponent<BadOMetreManager>();
				}

				if (_instance != null)
					DontDestroyOnLoad( _instance.gameObject );
			}

			return _instance;
		}
	}

	#endregion

	#region ExposedVariables

	[Header( "State thresholds" )]
	[SerializeField]
	private int _stageOneThreshold = 25;

	[SerializeField]
	private int _stageTwoThreshold = 50;
	[SerializeField]
	private int _stageThreeThreshold = 75;
	[SerializeField]
	private int _stageFourThreshold = 100;


	[Header( "Current state" )]
	[SerializeField]
	private int _badnessPoints = 0;

	public int BadnessPoints {
		get { return _badnessPoints; }
	}

	public int BadnessStage {
		get {
			if (_badnessPoints >= _stageOneThreshold)
				return 1;

			if (_badnessPoints >= _stageTwoThreshold)
				return 2;

			if (_badnessPoints >= _stageThreeThreshold)
				return 3;

			if (_badnessPoints >= _stageFourThreshold)
				return 4;

			return 0;
		}
	}


	[Header( "Event values" )]
	[SerializeField]
	private int _killCreatureLv1Value;

	[SerializeField]
	private int _killCreatureLv2Value;

	[SerializeField]
	private int _killCreatureLv3Value;

	[SerializeField]
	private int _killCreatureLv4Value;

	[Space( 10f )]
	[SerializeField]
	private int _talkEventValue = -25;

	[SerializeField]
	private int _irritateEventValue = -10;

	[SerializeField]
	private int _hurtEventValue = -5;

	#endregion

	#region MonoBehaviour

	private void Awake() {
		EnsureSingletonPattern();
	}

	#endregion

	#region Private Methods

	/// <summary>
	/// Ensures only one instance can be active at a time.
	/// </summary>
	private void EnsureSingletonPattern() {
		if ( _instance != null && _instance != this ) {
			Destroy( gameObject );
		} else {
			_instance = this;
			DontDestroyOnLoad( gameObject );
		}
	}

	#endregion

}