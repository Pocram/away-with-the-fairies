﻿using UnityEngine;

namespace Weapons.Guns {
	public class Bullet : MonoBehaviourExtended {
		#region Components

		[SerializeField]
		private ParticleSystem _miscCollisionParticleSystem;

		[SerializeField]
		private ParticleSystem _bloodCollisionParticleSystem;

		#endregion

		#region Exposed fields

		[SerializeField]
		private float _bulletRadius = 0.05f;

		public float BulletRadius {
			get { return _bulletRadius; }
		}

		[SerializeField]
		private LayerMask _entityLayerMask;

		[SerializeField]
		private LayerMask _miscCollisionLayerMask;

		[SerializeField]
		private float _gravity = -9.81f;

		public GameObject SourceObject { get; private set; }

		#endregion

		#region Private fields

		// Layer mask
		private LayerMask _combinedLayerMask;


		// Firing
		private Vector3 _firingPosition = Vector3.zero;

		private Gun _sourceGun;


		// Control
		private bool _isEnabled = false;


		// Movement
		private Vector3 _velocity = Vector3.zero;

		private Vector3 _lastPosition = Vector3.zero;

		#endregion

		#region MonoBehaviour

		private void Start() {
			_lastPosition = transform.position;

			GetAllComponents();
			RemoveEntityLayersFromMiscLayers();
			_combinedLayerMask = _entityLayerMask | _miscCollisionLayerMask;
		}

		private void Update() {
			if (GamePausing.PauseManager.IsPaused)
				return;

            if (_sourceGun == null || !_isEnabled)
				return;

			float currentDistance = ( transform.position - _firingPosition ).magnitude;

			if (currentDistance >= _sourceGun.MaxBulletDistance) {
				DisableBullet();
			}
		}

		private void FixedUpdate() {
			if ( GamePausing.PauseManager.IsPaused )
				return;

			if (!_isEnabled)
				return;

			// Move the bullet in the direction of its velocity
			_velocity.y -= _gravity * Time.fixedDeltaTime;
			transform.Translate( _velocity, Space.World );

			// Check for any collisions and act accordingly
			Vector3 castDirection = transform.position - _lastPosition;
			RaycastHit hit;
			if (Physics.SphereCast( _lastPosition, _bulletRadius, castDirection, out hit, castDirection.magnitude, _combinedLayerMask, QueryTriggerInteraction.Collide )) {
				transform.position = hit.point;

				int layerHit = hit.transform.gameObject.layer;

				if (_entityLayerMask.ContainsLayer( layerHit ) ) {
					HandleEntityCollisions( hit );
				} else if (_miscCollisionLayerMask.ContainsLayer( layerHit )) {
					HandleMiscCollisions( hit );
				}
			}
		}

		private void OnDrawGizmos() {
			Color gizmoColour = Color.white;
			gizmoColour.a = 0.75f;
			Gizmos.color = gizmoColour;

			Gizmos.DrawSphere( transform.position, _bulletRadius );
		}

		#endregion

		#region Private parts

		private void DisableBullet(bool alsoDisableObject = false) {
			_isEnabled = false;

			if(alsoDisableObject)
				gameObject.SetActive( false );
		}

		private void EnableBullet(bool alsoEnableObject = false) {
			_isEnabled = true;

			if ( alsoEnableObject )
				gameObject.SetActive( true );
		}

		private void RemoveEntityLayersFromMiscLayers() {
			LayerMask temp = _miscCollisionLayerMask;

			_miscCollisionLayerMask &= _entityLayerMask;
			_miscCollisionLayerMask = ~_miscCollisionLayerMask;
			_miscCollisionLayerMask &= temp;
		}

		private void GetAllComponents() {
		}

		// COLLISIONS
		// ==============================
		private void HandleEntityCollisions(RaycastHit hit) {
			_bloodCollisionParticleSystem.Play();

			var receiver = hit.transform.GetComponent<IRegionallyDamageable>();

			// If the receiver could not be found, look in its children.
			if (receiver == null) {
				receiver = hit.transform.GetComponentInChildren<IRegionallyDamageable>();

				// If the receiver could still not be found, look in its parents.
				if (receiver == null) {
					receiver = hit.transform.GetComponentInParent<IRegionallyDamageable>();
				}
			}

			if (receiver == null) {
				Debug.LogError( hit.transform.name + " has been hit by a bullet but does not implement " + typeof( IRegionallyDamageable ).Name + "!" );
				DisableBullet();
				return;
			}

			receiver.Damage( _sourceGun.DamagePerBullet, hit.collider, gameObject );

			DisableBullet();
		}

		private void HandleMiscCollisions(RaycastHit hit) {
			_miscCollisionParticleSystem.Play();
			// TODO: Show bullet hole
			DisableBullet();
		}

		#endregion

		#region Pubic methods

		public void Shoot(Vector3 direction, Gun sourceGun, GameObject sourceObject) {
			EnableBullet( true );

			_sourceGun = sourceGun;
			_firingPosition = transform.position;

			direction.Normalize();
			direction *= sourceGun.BulletSpeed;

			_velocity = direction;
			_lastPosition = transform.position;

			SourceObject = sourceObject;
		}

		#endregion
	}
}