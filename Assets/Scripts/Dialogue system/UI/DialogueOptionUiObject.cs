﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;

namespace Dialogue {

	public class DialogueOptionUiObject : MonoBehaviourExtended, IPointerEnterHandler, IPointerDownHandler {

		public int OptionId = 0;

		public Text UiTextElement;


		public void OnPointerEnter(PointerEventData eventData) {
			DialogueManager.Instance.CurrentSelectionIndex = OptionId;
		}

		public void OnPointerDown(PointerEventData eventData) {
			DialogueManager.ChooseCurrentOption();
		}
	}

}