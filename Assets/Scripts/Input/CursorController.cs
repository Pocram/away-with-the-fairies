﻿using UnityEngine;

public class CursorController : MonoBehaviour {

	#region Singleton Accessor

	private static CursorController _instance;

	public static CursorController Instance {
		get {
			if (_instance == null) {
				_instance = FindObjectOfType<CursorController>();

				// If it's still not there, create it
				if (_instance == null) {
					GameObject newGo = new GameObject( "CursorController", typeof (CursorController) );
					Instantiate( newGo );
				}
			}

			return _instance;
		}
	}

	#endregion

	private int CursorShowCallers = 0;


	private void Start() {
		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}


	/// <summary>
	/// Enables cursor and allows it to move freely.
	/// </summary>
	public void FreeCursor() {
		CursorShowCallers++;

		Cursor.lockState = CursorLockMode.None;
		Cursor.visible = true;
	}

	/// <summary>
	/// Hides the cursor and locks it at the center position.
	/// </summary>
	public void LockCursor() {
		CursorShowCallers--;

		if (CursorShowCallers != 0)
			return;

		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}

}