﻿#define PLAYER_INTERACTION_DEBUG

using System;
using System.Linq;
using FirstPersonPlayerController;
using UnityEngine;

namespace ObjectInteraction {

	public sealed class PlayerInteractionManager : MonoBehaviourExtended {

		public static int InputBlockers = 0;


		[Header("Camera")]
		[SerializeField]
		private FirstPersonPlayerCameraController _playerCameraController;

		[Header("Input")]
		[SerializeField]
		private KeyCode _interactionKey = KeyCode.E;


		#region RayCastControl properties

		[Header("Raycast control")]
		[Tooltip("Only objects that are marked with one of these tags are accepted.")]
		[SerializeField]
		private string[] _tagMask;

		[Tooltip("Only objects on the chosen layers are accepted.")]
		[SerializeField]
		private LayerMask _layerMask;

		[Tooltip("The maximum distance to check for.")]
		[SerializeField]
		private float _interactionDistance = 1.5f;

		#endregion

		#region Public properties

		public bool IsTargetingValidObject {
			get { return _interactableObjectComponent != null; }
		}

		public string NameOfTarget {
			get {
				if(IsTargetingValidObject)
					return _interactableObjectComponent.ObjectName;

				return string.Empty;
			}
		}

		public string PromptOfTarget {
			get
			{
				if ( IsTargetingValidObject )
					return _interactableObjectComponent.GetInteractionPromptText();

				return string.Empty;
			}
		}

		#endregion

		#region Events

		public delegate void PlayerInteractionEventDelegate(PlayerInteractionEventData data);

		public static event PlayerInteractionEventDelegate OnPlayerInteractWithObject;

		#endregion

		#region Private properties

		/// <summary>
		/// The hit information of the raycast.
		/// </summary>
		private RaycastHit _lookHit;

		private bool _isInteractingWithObject = false;

		private bool _previousIsInteractingWithObject = false;

		private IInteractableObject _interactableObjectComponent;

		private IInteractableObject _lastInteractableObjectComponent;

		/// <summary>
		/// The RayCastHit of the previous update iteration
		/// </summary>
		private RaycastHit _previousHit;

		#endregion

		#region MonoBehaviour

		private void Update() {
			if (GamePausing.PauseManager.IsPaused)
				return;

			if ( InputBlockers > 0 ) {
				if ( IsTargetingValidObject ) {
					InteractUp();
				}

				_interactableObjectComponent = null;
				return;
			}

			CheckForObject();

			// Handle input
			if ( Input.GetKeyDown( _interactionKey ) ) {
				InteractDown();
			} else if ( Input.GetKey( _interactionKey ) ) {
				Interact();
			} else if ( Input.GetKeyUp( _interactionKey ) ) {
				InteractUp();
			}
		}

		#endregion


		private void CheckForObject() {
			if ( _playerCameraController == null )
				return;

			// Raycast data
			Vector3 rayStart = _playerCameraController.StabilisedWorldPositon;
			Vector3 rayDirection = _playerCameraController.transform.forward;

			// Check
			if ( Physics.Raycast( rayStart, rayDirection, out _lookHit, _interactionDistance, _layerMask ) ) {
				if ( _tagMask.Contains( _lookHit.transform.tag ) ) {
					// Object with fitting tag found.
					__DrawRay( rayStart, rayDirection, Color.green );

					// Only perform a new query if the object changed.
					// This is good for the game's performance and makes it like you! :)
					if ( _lookHit.transform != _previousHit.transform ) {
						_interactableObjectComponent = _lookHit.transform.GetComponent<IInteractableObject>();
					}

				} else {
					// Object found, but tag does not fit.
					__DrawRay( rayStart, rayDirection, Color.blue );
					_interactableObjectComponent = null;

				}
			} else {
				// No object found.
				__DrawRay( rayStart, rayDirection, Color.red );
				_interactableObjectComponent = null;
			}

			// DELTAAAAAAAAS.
			_previousHit = _lookHit;
			_previousIsInteractingWithObject = _isInteractingWithObject;
		}


		public void InteractDown() {
			if (_interactableObjectComponent != null) {
				_isInteractingWithObject = true;
				_interactableObjectComponent.OnInteractionEnter();

				_lastInteractableObjectComponent = _interactableObjectComponent;

				if (OnPlayerInteractWithObject != null)
					OnPlayerInteractWithObject( new PlayerInteractionEventData( InteractionDeltaType.Start, _interactableObjectComponent ) );
			}
		}

		public void Interact() {
			if (_interactableObjectComponent != null) {
				if (_previousIsInteractingWithObject) {
					_isInteractingWithObject = true;
					_interactableObjectComponent.OnInteractionStay();

					if (OnPlayerInteractWithObject != null)
						OnPlayerInteractWithObject( new PlayerInteractionEventData( InteractionDeltaType.Hold, _interactableObjectComponent ) );
				}

			} else {
				// Call the exit method on the object if the ray leaves it while holding down the button.
				_isInteractingWithObject = false;
				if (_previousIsInteractingWithObject && _lastInteractableObjectComponent != null) {
					_lastInteractableObjectComponent.OnInteractionExit();

					if (OnPlayerInteractWithObject != null)
						OnPlayerInteractWithObject( new PlayerInteractionEventData( InteractionDeltaType.Exit, _lastInteractableObjectComponent ) );

					_lastInteractableObjectComponent = _interactableObjectComponent;
				}
			}
		}

		public void InteractUp() {
			if (_interactableObjectComponent != null && _isInteractingWithObject) {
				_interactableObjectComponent.OnInteractionExit();
				if ( OnPlayerInteractWithObject != null )
					OnPlayerInteractWithObject( new PlayerInteractionEventData( InteractionDeltaType.Exit, _interactableObjectComponent ) );

				_lastInteractableObjectComponent = _interactableObjectComponent;
			}

			_isInteractingWithObject = false;
		}


		#region Internal types

		public enum InteractionDeltaType {

			Start,
			Hold,
			Exit

		}

		public class PlayerInteractionEventData {

			public PlayerInteractionEventData(InteractionDeltaType type, IInteractableObject interactableObject) {
				Type = type;
				InteractableObject = interactableObject;
			}

			public InteractionDeltaType Type;
			public IInteractableObject InteractableObject;

		}

		#endregion

		#region __DEBUG

#if PLAYER_INTERACTION_DEBUG
		private void OnGUI() {
			if (_lookHit.transform == null)
				return;

			Vector2 boxDimensions = new Vector2( 300f, 180f );
			Vector2 boxPosition = new Vector2( 10f, 10f );
			float boxPadding = 5f;

			string displayText = "<b>LookingAt</b>: " + _lookHit.transform.name;
			displayText += "\n<b>HasInterface</b>: " + ( _interactableObjectComponent != null );

			displayText += "\n";
			displayText += "\n<b>IsTargetingValidObject</b>: " + IsTargetingValidObject;

			if (_interactableObjectComponent != null) {
				displayText += "\n";
				displayText += "\n<b>Name</b>: " + NameOfTarget;
				displayText += "\n<b>Prompt</b>: " + PromptOfTarget;
				displayText += "\n<b>IsInteracting</b>: " + _isInteractingWithObject;
			}

			GUI.Box( new Rect( boxPosition.x, boxPosition.y, boxDimensions.x, boxDimensions.y ), "<b>PlayerInteraction Debug Console</b>" );
			GUI.Label( new Rect( boxPosition.x + boxPadding, boxPosition.y + boxPadding + 20f, boxDimensions.x - boxPadding, boxDimensions.y - boxPadding - 20f ), displayText );
		}
#endif

		[System.Diagnostics.Conditional( "PLAYER_INTERACTION_DEBUG" )]
		private void __DrawRay( Vector3 origin, Vector3 dir, Color color ) {
			Debug.DrawRay( origin, dir * _interactionDistance, color );
		}

		#endregion
	}

}