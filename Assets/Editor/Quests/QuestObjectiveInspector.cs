﻿using System;
using UnityEngine;
using UnityEditor;
using UnityEditorInternal;

namespace Quests {

	[CustomEditor( typeof( QuestObjective ) )]
	public sealed class QuestObjectiveInspector : Editor {

		private QuestObjective _objective;

		private ReorderableList _solutionsList;

		private SerializedProperty _serializedSolutionsProperty;


		private void OnEnable() {
			_objective = (QuestObjective) target;

			if (_objective == null) {
				EditorGUILayout.HelpBox( "Something went horribly wrong!!1", MessageType.Error );
				return;
			}

			_serializedSolutionsProperty = serializedObject.FindProperty( "Solutions" );
			_solutionsList = new ReorderableList( serializedObject, _serializedSolutionsProperty, true, true, true, true );

			// Config list
			_solutionsList.drawHeaderCallback = DrawObjectiveSolutionsListHeader;
			_solutionsList.drawElementCallback = OnDrawObjectiveSolutionListElement;
		}

		public override void OnInspectorGUI() {
			serializedObject.Update();
			ResizeSolutionsListElements( ref _solutionsList, _objective );

			DrawObjectiveInfoLayout( ref _objective );
			
			_solutionsList.DoLayoutList();
			serializedObject.ApplyModifiedProperties();
		}

		private void OnDrawObjectiveSolutionListElement( Rect rect, int index, bool isActive, bool isFocused ) {
			rect.y += 2f;

			SerializedProperty element = _serializedSolutionsProperty.GetArrayElementAtIndex( index );
			QuestObjectiveInspector.DrawSolutionElementList( rect, index, isActive, isFocused, _solutionsList, element );
		}

		private void ResizeSolutionsListElements( ref ReorderableList list, QuestObjective objective ) {
			int elementLines = 0;
			for ( int i = 0; i < objective.Solutions.Count; i++ ) {
				if ( objective.Solutions[i].GetAmountOfUiElements() > elementLines )
					elementLines = objective.Solutions[i].GetAmountOfUiElements();
			}
			elementLines += 3;

			list.elementHeight = EditorGUIUtility.singleLineHeight * elementLines + EditorGUIUtility.standardVerticalSpacing * ( elementLines + 4 );
		}

		#region Public static methods

		public static Rect DrawObjective( QuestObjective objective, Rect rect, ref ReorderableList rList ) {
			throw new System.NotImplementedException();
		}


		public static void DrawObjectiveInfo(Rect rect, SerializedProperty serializedObjectiveProperty) {
			GUIContent descrContent = new GUIContent( "Description" );
			rect = new Rect( rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight );
			serializedObjectiveProperty.FindPropertyRelative( "Description" ).stringValue = EditorGUI.TextField( rect, descrContent, serializedObjectiveProperty.FindPropertyRelative( "Description" ).stringValue );
		}

		public static void DrawObjectiveInfoLayout( ref QuestObjective objective ) {
			GUIContent descrContent = new GUIContent( "Description" );
			objective.Description = EditorGUILayout.TextField( descrContent, objective.Description );
		}


		public static void DrawObjectiveSolutionsListHeader( Rect rect ) {
			EditorGUI.LabelField( rect, "Solutions" );
		}

		public static void DrawSolutionElementList(Rect rect, int index, bool isActive, bool isFocused, ReorderableList rList, SerializedProperty elementProperty ) {
			// Draw description
			GUIContent descrContent = new GUIContent( "Description" );
			Rect descrRect = new Rect( rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight );
			elementProperty.FindPropertyRelative( "Description" ).stringValue = EditorGUI.TextField( descrRect, descrContent, elementProperty.FindPropertyRelative( "Description" ).stringValue );

			// Is mandatory?
			GUIContent mandatoryContent = new GUIContent( "Is Mandatory", "Whether this objective is mandatory in order to complete the solution." );
			Rect mandatoryRect = new Rect( descrRect.x, descrRect.yMax + EditorGUIUtility.standardVerticalSpacing, descrRect.width, EditorGUIUtility.singleLineHeight );
			elementProperty.FindPropertyRelative( "IsMandatoryForObjective" ).boolValue = EditorGUI.Toggle( mandatoryRect, mandatoryContent, elementProperty.FindPropertyRelative( "IsMandatoryForObjective" ).boolValue );

			// Type
			GUIContent typeContent = new GUIContent( "Solution Type" );
			Rect typeRect = new Rect( mandatoryRect.x, mandatoryRect.yMax + EditorGUIUtility.standardVerticalSpacing, mandatoryRect.width, EditorGUIUtility.singleLineHeight );
			elementProperty.FindPropertyRelative( "Type" ).enumValueIndex = (int) (QuestObjectiveSolution.SolutionType) EditorGUI.EnumPopup( typeRect, typeContent, (QuestObjectiveSolution.SolutionType) Enum.GetValues( typeof (QuestObjectiveSolution.SolutionType) ).GetValue( elementProperty.FindPropertyRelative( "Type" ).enumValueIndex ) );

			Rect paramRect = typeRect;
			paramRect.y = paramRect.yMax + EditorGUIUtility.standardVerticalSpacing;

			int oldIndentLevel = EditorGUI.indentLevel;
			EditorGUI.indentLevel++;
			// Draw parameters depending on type
			switch ( (QuestObjectiveSolution.SolutionType) elementProperty.FindPropertyRelative( "Type" ).enumValueIndex ) {
				case QuestObjectiveSolution.SolutionType.ReachLocation:
					DrawReachLocationParameters( paramRect, index, isActive, isFocused, rList, elementProperty.FindPropertyRelative( "ReachLocationParam" ) );
					break;
				case QuestObjectiveSolution.SolutionType.InteractWithObject:
					DrawInteractWithObjectParameters( paramRect, index, isActive, isFocused, rList, elementProperty.FindPropertyRelative( "InteractWithObjectParam" ) );
					break;
				case QuestObjectiveSolution.SolutionType.KillActor:
					DrawKillActorParameters( paramRect, index, isActive, isFocused, rList, elementProperty.FindPropertyRelative( "KillActorParam" ) );
					break;
				case QuestObjectiveSolution.SolutionType.ShootAtActor:
					DrawShootAtActorParameters( paramRect, index, isActive, isFocused, rList, elementProperty.FindPropertyRelative( "ShootAtActorParam" ) );
					break;
				case QuestObjectiveSolution.SolutionType.StartDialogue:
					DrawStartDialogueParameters( paramRect, index, isActive, isFocused, rList, elementProperty.FindPropertyRelative( "StartDialogueParam" ) );
					break;
				case QuestObjectiveSolution.SolutionType.EndDialogue:
					DrawEndDialogueParameters( paramRect, index, isActive, isFocused, rList, elementProperty.FindPropertyRelative( "EndDialogueParam" ) );
					break;
			}
			EditorGUI.indentLevel = oldIndentLevel;
		}


		public static void DrawReachLocationParameters(Rect rect, int index, bool isActive, bool isFocused, ReorderableList rList, SerializedProperty elementProperty) {
			// Location name
			GUIContent namecontent = new GUIContent( "Location Name" );
			rect = new Rect( rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight );
			elementProperty.FindPropertyRelative( "LocationName" ).stringValue = EditorGUI.TextField( rect, namecontent, elementProperty.FindPropertyRelative( "LocationName" ).stringValue );

			// Layer Mask
			GUIContent maskContent = new GUIContent( "Object Layer Mask" );
			rect = new Rect( rect.x, rect.yMax + EditorGUIUtility.standardVerticalSpacing, rect.width, EditorGUIUtility.singleLineHeight );
			EditorGUI.PropertyField( rect, elementProperty.FindPropertyRelative( "LayerMask" ), maskContent, false );

		}

		public static void DrawInteractWithObjectParameters( Rect rect, int index, bool isActive, bool isFocused, ReorderableList rList, SerializedProperty elementProperty ) {
			// Required amount field
			GUIContent reqAmountContent = new GUIContent( "Required Amount" );
			rect = new Rect(rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight);
			elementProperty.FindPropertyRelative( "RequiredAmount" ).intValue = EditorGUI.IntField( rect, reqAmountContent, elementProperty.FindPropertyRelative( "RequiredAmount" ).intValue );

			
			// Valid object name array count field
			GUIContent validNameCountContent = new GUIContent( "Valid Names" );
			Rect validNameCountRect = new Rect( rect.x, rect.yMax + EditorGUIUtility.standardVerticalSpacing, rect.width, EditorGUIUtility.singleLineHeight );
			elementProperty.FindPropertyRelative( "ValidObjectNames" ).arraySize = EditorGUI.IntField( validNameCountRect, validNameCountContent, elementProperty.FindPropertyRelative( "ValidObjectNames" ).arraySize );

			EditorGUI.indentLevel++;
			// Valid object name array
			Rect elementRect = new Rect( validNameCountRect.x, validNameCountRect.yMin, validNameCountRect.width, EditorGUIUtility.singleLineHeight );
			for (int i = 0; i < elementProperty.FindPropertyRelative( "ValidObjectNames" ).arraySize; i++) {
				SerializedProperty element = elementProperty.FindPropertyRelative( "ValidObjectNames" ).GetArrayElementAtIndex( i );

				GUIContent elementContent = new GUIContent( "Name" );
				elementRect = new Rect( elementRect.x, elementRect.yMin + EditorGUIUtility.standardVerticalSpacing + EditorGUIUtility.singleLineHeight, elementRect.width, EditorGUIUtility.singleLineHeight );
				element.stringValue = EditorGUI.TextField( elementRect, elementContent, element.stringValue );
			}
			EditorGUI.indentLevel--;


			// Compare mode
			GUIContent compareModeContent = new GUIContent( "Compare Mode" );
			Rect compareModeRect = new Rect( elementRect.x, elementRect.yMax + EditorGUIUtility.standardVerticalSpacing, elementRect.width, EditorGUIUtility.singleLineHeight );
			elementProperty.FindPropertyRelative( "CompareMode" ).enumValueIndex = (int) (CompareNameMode) EditorGUI.EnumPopup( compareModeRect, compareModeContent, (CompareNameMode) Enum.GetValues( typeof( CompareNameMode ) ).GetValue( elementProperty.FindPropertyRelative( "CompareMode" ).enumValueIndex ) );


			// Valid delta types count
			GUIContent validDataTypesContent = new GUIContent( "Valid Delta Types" );
			Rect validDeltaTypesRect = new Rect( compareModeRect.x, compareModeRect.yMax + EditorGUIUtility.standardVerticalSpacing, compareModeRect.width, EditorGUIUtility.singleLineHeight );
			elementProperty.FindPropertyRelative( "ValidDeltaTypes" ).arraySize = EditorGUI.IntField( validDeltaTypesRect, validDataTypesContent, elementProperty.FindPropertyRelative( "ValidDeltaTypes" ).arraySize );

			EditorGUI.indentLevel++;
			// Valid delta types array
			elementRect = new Rect( validDeltaTypesRect.x, validDeltaTypesRect.yMin, validDeltaTypesRect.width, EditorGUIUtility.singleLineHeight );
			for (int i = 0; i < elementProperty.FindPropertyRelative( "ValidDeltaTypes" ).arraySize; i++) {
				SerializedProperty element = elementProperty.FindPropertyRelative( "ValidDeltaTypes" ).GetArrayElementAtIndex( i );

				GUIContent elementContent = new GUIContent( "Delta Type" );
				elementRect = new Rect( elementRect.x, elementRect.yMin + EditorGUIUtility.standardVerticalSpacing + EditorGUIUtility.singleLineHeight, elementRect.width, EditorGUIUtility.singleLineHeight );
				element.enumValueIndex = (int) (ObjectInteraction.PlayerInteractionManager.InteractionDeltaType) EditorGUI.EnumPopup( elementRect, elementContent, (ObjectInteraction.PlayerInteractionManager.InteractionDeltaType) Enum.GetValues( typeof( ObjectInteraction.PlayerInteractionManager.InteractionDeltaType ) ).GetValue( element.enumValueIndex ) );
			}
			EditorGUI.indentLevel--;
		}

		public static void DrawKillActorParameters(Rect rect, int index, bool isActive, bool isFocused, ReorderableList rList, SerializedProperty elementProperty) {
			// Required amount field
			GUIContent reqAmountContent = new GUIContent( "Required Amount" );
			rect = new Rect( rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight );
			elementProperty.FindPropertyRelative( "RequiredAmount" ).intValue = EditorGUI.IntField( rect, reqAmountContent, elementProperty.FindPropertyRelative( "RequiredAmount" ).intValue );


			// Valid object name array count field
			GUIContent validNameCountContent = new GUIContent( "Valid Names" );
			rect = new Rect( rect.x, rect.yMax + EditorGUIUtility.standardVerticalSpacing, rect.width, EditorGUIUtility.singleLineHeight );
			elementProperty.FindPropertyRelative( "ValidActorNames" ).arraySize = EditorGUI.IntField( rect, validNameCountContent, elementProperty.FindPropertyRelative( "ValidActorNames" ).arraySize );

			EditorGUI.indentLevel++;
			// Valid object name array
			rect = new Rect( rect.x, rect.yMin, rect.width, EditorGUIUtility.singleLineHeight );
			for ( int i = 0; i < elementProperty.FindPropertyRelative( "ValidActorNames" ).arraySize; i++ ) {
				SerializedProperty element = elementProperty.FindPropertyRelative( "ValidActorNames" ).GetArrayElementAtIndex( i );

				GUIContent elementContent = new GUIContent( "Name" );
				rect = new Rect( rect.x, rect.yMin + EditorGUIUtility.standardVerticalSpacing + EditorGUIUtility.singleLineHeight, rect.width, EditorGUIUtility.singleLineHeight );
				element.stringValue = EditorGUI.TextField( rect, elementContent, element.stringValue );
			}
			EditorGUI.indentLevel--;


			// Valid object name array count field
			GUIContent validTagCountContent = new GUIContent( "Valid Source Tags", "The source tag is the tag of the weapon manager that fired the bullet." );
			rect = new Rect( rect.x, rect.yMax + EditorGUIUtility.standardVerticalSpacing, rect.width, EditorGUIUtility.singleLineHeight );
			elementProperty.FindPropertyRelative( "ValidSourceTags" ).arraySize = EditorGUI.IntField( rect, validTagCountContent, elementProperty.FindPropertyRelative( "ValidSourceTags" ).arraySize );

			EditorGUI.indentLevel++;
			// Valid object name array
			rect = new Rect( rect.x, rect.yMin, rect.width, EditorGUIUtility.singleLineHeight );
			for ( int i = 0; i < elementProperty.FindPropertyRelative( "ValidSourceTags" ).arraySize; i++ ) {
				SerializedProperty element = elementProperty.FindPropertyRelative( "ValidSourceTags" ).GetArrayElementAtIndex( i );

				GUIContent elementContent = new GUIContent( "Tag" );
				rect = new Rect( rect.x, rect.yMin + EditorGUIUtility.standardVerticalSpacing + EditorGUIUtility.singleLineHeight, rect.width, EditorGUIUtility.singleLineHeight );
				element.stringValue = EditorGUI.TextField( rect, elementContent, element.stringValue );
			}
			EditorGUI.indentLevel--;
		}

		public static void DrawShootAtActorParameters( Rect rect, int index, bool isActive, bool isFocused, ReorderableList rList, SerializedProperty elementProperty ) {
			// Required amount field
			GUIContent reqAmountContent = new GUIContent( "Required Amount" );
			rect = new Rect( rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight );
			elementProperty.FindPropertyRelative( "RequiredAmount" ).intValue = EditorGUI.IntField( rect, reqAmountContent, elementProperty.FindPropertyRelative( "RequiredAmount" ).intValue );


			// Valid object name array count field
			GUIContent validNameCountContent = new GUIContent( "Valid Names" );
			rect = new Rect( rect.x, rect.yMax + EditorGUIUtility.standardVerticalSpacing, rect.width, EditorGUIUtility.singleLineHeight );
			elementProperty.FindPropertyRelative( "ValidActorNames" ).arraySize = EditorGUI.IntField( rect, validNameCountContent, elementProperty.FindPropertyRelative( "ValidActorNames" ).arraySize );

			EditorGUI.indentLevel++;
			// Valid object name array
			rect = new Rect( rect.x, rect.yMin, rect.width, EditorGUIUtility.singleLineHeight );
			for ( int i = 0; i < elementProperty.FindPropertyRelative( "ValidActorNames" ).arraySize; i++ ) {
				SerializedProperty element = elementProperty.FindPropertyRelative( "ValidActorNames" ).GetArrayElementAtIndex( i );

				GUIContent elementContent = new GUIContent( "Name" );
				rect = new Rect( rect.x, rect.yMin + EditorGUIUtility.standardVerticalSpacing + EditorGUIUtility.singleLineHeight, rect.width, EditorGUIUtility.singleLineHeight );
				element.stringValue = EditorGUI.TextField( rect, elementContent, element.stringValue );
			}
			EditorGUI.indentLevel--;


			// Valid object name array count field
			GUIContent validTagCountContent = new GUIContent( "Valid Source Tags", "The source tag is the tag of the weapon manager that fired the bullet." );
			rect = new Rect( rect.x, rect.yMax + EditorGUIUtility.standardVerticalSpacing, rect.width, EditorGUIUtility.singleLineHeight );
			elementProperty.FindPropertyRelative( "ValidSourceTags" ).arraySize = EditorGUI.IntField( rect, validTagCountContent, elementProperty.FindPropertyRelative( "ValidSourceTags" ).arraySize );

			EditorGUI.indentLevel++;
			// Valid object name array
			rect = new Rect( rect.x, rect.yMin, rect.width, EditorGUIUtility.singleLineHeight );
			for ( int i = 0; i < elementProperty.FindPropertyRelative( "ValidSourceTags" ).arraySize; i++ ) {
				SerializedProperty element = elementProperty.FindPropertyRelative( "ValidSourceTags" ).GetArrayElementAtIndex( i );

				GUIContent elementContent = new GUIContent( "Tag" );
				rect = new Rect( rect.x, rect.yMin + EditorGUIUtility.standardVerticalSpacing + EditorGUIUtility.singleLineHeight, rect.width, EditorGUIUtility.singleLineHeight );
				element.stringValue = EditorGUI.TextField( rect, elementContent, element.stringValue );
			}
			EditorGUI.indentLevel--;


			// Single damage
			GUIContent reqDmgPerShotContent = new GUIContent( "Min Damage" );
			rect = new Rect( rect.x, rect.yMax + EditorGUIUtility.standardVerticalSpacing, rect.width, EditorGUIUtility.singleLineHeight );
			elementProperty.FindPropertyRelative( "RequiredDamagePerShot" ).floatValue = EditorGUI.FloatField( rect, reqDmgPerShotContent, elementProperty.FindPropertyRelative( "RequiredDamagePerShot" ).floatValue );

			// Single damage
			GUIContent reqTotalDamageContent = new GUIContent( "Total Damage" );
			rect = new Rect( rect.x, rect.yMax + EditorGUIUtility.standardVerticalSpacing, rect.width, EditorGUIUtility.singleLineHeight );
			elementProperty.FindPropertyRelative( "RequiredDamageTotal" ).floatValue = EditorGUI.FloatField( rect, reqTotalDamageContent, elementProperty.FindPropertyRelative( "RequiredDamageTotal" ).floatValue );
		}

		public static void DrawStartDialogueParameters(Rect rect, int index, bool isActive, bool isFocused, ReorderableList rList, SerializedProperty elementProperty) {
			// Node
			GUIContent nodeContent = new GUIContent( "Node" );
			rect = new Rect( rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight );
			EditorGUI.PropertyField( rect, elementProperty.FindPropertyRelative( "Node" ), nodeContent );
		}

		public static void DrawEndDialogueParameters( Rect rect, int index, bool isActive, bool isFocused, ReorderableList rList, SerializedProperty elementProperty ) {
			// Node
			GUIContent nodeContent = new GUIContent( "Node" );
			rect = new Rect( rect.x, rect.y, rect.width, EditorGUIUtility.singleLineHeight );
			EditorGUI.PropertyField( rect, elementProperty.FindPropertyRelative( "Node" ), nodeContent );

			// Index
			GUIContent indexContent = new GUIContent( "Option Index" );
			rect = new Rect( rect.x, rect.yMax + EditorGUIUtility.standardVerticalSpacing, rect.width, EditorGUIUtility.singleLineHeight );
			EditorGUI.PropertyField( rect, elementProperty.FindPropertyRelative( "OptionIndex" ), indexContent );
		}
		#endregion

	}

}