﻿using UnityEngine;
using Weapons;

public class __TestShoot : MonoBehaviourExtended {

	public WeaponManager WeaponManager;

	private void Update() {
		if (WeaponManager.CanShoot) {
			WeaponManager.Shoot();
		} else if (WeaponManager.CanReload && WeaponManager.BulletsInMagazine == 0) {
			WeaponManager.Reload();
		}
	}

}