﻿namespace Dialogue {

	[System.Serializable]
	public class DialogueOption {

		public string Text;

		public DialogueNode NextNode;

		public bool HasEvent;

	}

}