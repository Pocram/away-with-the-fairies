﻿using UnityEditor;

namespace Events {

	[CustomEditor(typeof(EventManager))]
	public class EventManagerInspector : Editor {

		public override void OnInspectorGUI() {
			EventManager manager = (EventManager) target;

			if (manager.SimpleEvents == null) {
				EditorGUILayout.HelpBox( "The SimpleEvents dictionary has not been initialised, yet.", MessageType.Info );
				return;
			}

		}

	}

}