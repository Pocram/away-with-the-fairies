﻿using ActorPhysics;
using UnityEngine;

public class PlayerActorHealthColliderManager : MonoBehaviourExtended {

	#region Exposed fields

	[SerializeField]
	private GroundActorPhysics _groundActorPhysics;

	[SerializeField]
	private CapsuleCollider _capsuleCollider;

	#endregion

	#region MonoBehaviour

	private void Start() {
		if (_groundActorPhysics == null)
			return;

		_groundActorPhysics.OnChangeHeight -= OnActorHeightChange;
		_groundActorPhysics.OnChangeHeight += OnActorHeightChange;
	}

	private void OnDisable() {
		if ( _groundActorPhysics == null )
			return;

		_groundActorPhysics.OnChangeHeight -= OnActorHeightChange;
	}

	#endregion

	#region Private methods

	private void OnActorHeightChange( GroundActorPhysics actor, GroundActorHeightChangeEventData data ) {
		if (_capsuleCollider == null)
			return;

		_capsuleCollider.center = new Vector3( 0f, data.NewHeight / 2f, 0f );
		_capsuleCollider.height = data.NewHeight;
	}

	#endregion

}