﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace Events {
	
	public class EventManager : MonoBehaviourExtended {

		#region Singleton

		private static EventManager _instance;

		private static EventManager Instance {
			get {
				if ( _instance == null ) {
					_instance = FindObjectOfType<EventManager>();

					// If no instance is found, create one
					if ( _instance == null ) {
						GameObject newGo = new GameObject();
						newGo.name = "EventManager";
						newGo.AddComponent<EventManager>();
					}
				}

				return _instance;
			}
		}

		#endregion

		#region Exposed variables

		public Dictionary<string, UnityEvent> SimpleEvents { get; private set; }

		#endregion

		#region MonoBehaviour

		private void Awake() {
			EnsureSingletonPattern();
		}

		#endregion

		#region Private methods

		/// <summary>
		/// Ensures only one instance can be active at a time.
		/// </summary>
		private void EnsureSingletonPattern() {
			if ( _instance != null && _instance != this ) {
				Destroy( gameObject );
			} else {
				_instance = this;
				DontDestroyOnLoad( gameObject );

				Init();
			}
		}

		private void Init() {
			if( SimpleEvents == null)
				SimpleEvents = new UnityEventDictionary();
		}

		#endregion

		#region Public methods

		public static void AddSimpleListener(string eventName, UnityAction listener) {
			UnityEvent currentEvent;

			if (Instance.SimpleEvents.TryGetValue( eventName, out currentEvent )) {
				// Event already exists,s just listen
				currentEvent.AddListener( listener );

			} else {
				// Event does not exist
				// Create it and add it to the dictionary
				currentEvent = new UnityEvent();
				currentEvent.AddListener( listener );

				Instance.SimpleEvents.Add( eventName, currentEvent );

			}
		}

		public static void RemoveSimpleListener(string eventName, UnityAction listener) {
			UnityEvent currentEvent;
			if (Instance.SimpleEvents.TryGetValue( eventName, out currentEvent )) {
				currentEvent.RemoveListener( listener );
			}

		}

		public static void TriggerSimpleEvent(string eventName) {
			UnityEvent currentEvent;
			if (Instance.SimpleEvents.TryGetValue( eventName, out currentEvent )) {
				currentEvent.Invoke();
			}
		}

		#endregion

	}

}