﻿using UnityEngine;
using UnityEditor;

namespace Weapons.Guns {

	[CustomEditor(typeof (Bullet))]
	[CanEditMultipleObjects]
	public class BulletInspector : Editor {

		public void OnDrawGizmosSelected() {
			Bullet bullet = (Bullet) target;

			Gizmos.color = Color.yellow;
			Gizmos.DrawSphere( bullet.transform.position, bullet.BulletRadius );
		}

	}

}