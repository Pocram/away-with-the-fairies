﻿using System;
using System.Linq;
using Actors.Health;
using ObjectInteraction;
using UnityEngine;

namespace Quests {

	[Serializable]
	public class QuestObjectiveSolution {

		#region Exposed variables

		public string Description;

		public bool IsMandatoryForObjective = false;

		public SolutionType Type = SolutionType.ReachLocation;

		public bool IsFinished { get; private set; }

		// Parameters
		// ==========================================================
		public ReachLocationParameter ReachLocationParam;

		public InteractWithObjectParameter InteractWithObjectParam;

		public ShootAtActorParameter ShootAtActorParam;

		public KillActorParameter KillActorParam;

		public StartDialogueParameter StartDialogueParam;

		public EndDialogueParameter EndDialogueParam;

		#endregion

		#region Events

		public delegate void FinishObjectiveSolutionEventDelegate(QuestObjectiveSolution solution, FinishQuestObjectiveSolutionEventData data);

		public event FinishObjectiveSolutionEventDelegate OnFinishSolution;

		#endregion

		#region Private variables

		/// <summary>
		/// The amount of times the respective event was received successfully.
		/// </summary>
		private int _currentFitCount = 0;

		/// <summary>
		/// The total amount of damage registered by the respective event.
		/// </summary>
		private float _currentTotalDamage = 0f;

		#endregion

		#region Public methods

		public void Init() {
			// Reset current parameters, because unity saves those in the object
			IsFinished = false;
			_currentFitCount = 0;
			_currentTotalDamage = 0f;

			switch ( Type ) {
				case SolutionType.ReachLocation:
					InitReachLocation();
					break;
				case SolutionType.InteractWithObject:
					InitInteractWithObject();
					break;
				case SolutionType.ShootAtActor:
					InitShootAtActor();
					break;
				case SolutionType.KillActor:
					InitKillActor();
					break;
				case SolutionType.StartDialogue:
					InitStartDialogue();
					break;
				case SolutionType.EndDialogue:
					InitEndDialogue();
					break;
			}
		}

		public void Destruct() {
			switch ( Type ) {
				case SolutionType.ReachLocation:
					DestructReachLocation();
					break;
				case SolutionType.InteractWithObject:
					DestructInteractWithObject();
					break;
				case SolutionType.ShootAtActor:
					DestructShootAtActor();
					break;
				case SolutionType.KillActor:
					DestructKillActor();
					break;
				case SolutionType.StartDialogue:
					DestructStartDialogue();
					break;
				case SolutionType.EndDialogue:
					DestructEndDialogue();
					break;
			}
		}

		public int GetAmountOfUiElements() {
			int elements = 0;

			switch ( Type ) {
				case SolutionType.ReachLocation:
					elements = 2;
					break;
				case SolutionType.InteractWithObject:
					elements += InteractWithObjectParam.ValidObjectNames.Length;
					elements += InteractWithObjectParam.ValidDeltaTypes.Length;
					elements += 2;
					elements += 2; // 2 arrays
					break;
				case SolutionType.KillActor:
					elements += KillActorParam.ValidActorNames.Length;
					elements += KillActorParam.ValidSourceTags.Length;
					elements += 1;
					elements += 2; // 2 arrays
					break;
				case SolutionType.ShootAtActor:
					elements += ShootAtActorParam.ValidActorNames.Length;
					elements += ShootAtActorParam.ValidSourceTags.Length;
					elements += 3;
					elements += 2; // 2 arrays
					break;
				case SolutionType.StartDialogue:
					elements = 1;
					break;
				case SolutionType.EndDialogue:
					elements = 2;
					break;
			}

			return elements;
		}

		#endregion

		#region Private methods

		// Initialisers
		// ==========================================================
		private void InitReachLocation() {
			Locations.LocationZone.OnAnyZoneEnter -= OnPlayerLocationEnter;
			Locations.LocationZone.OnAnyZoneEnter += OnPlayerLocationEnter;
		}

		private void InitInteractWithObject() {
			PlayerInteractionManager.OnPlayerInteractWithObject -= OnPlayerInteractWithObject;
			PlayerInteractionManager.OnPlayerInteractWithObject += OnPlayerInteractWithObject;
		}

		private void InitShootAtActor() {
			GenericActorHealth.OnAnyTakeDamage -= OnActorTakeDamage;
			GenericActorHealth.OnAnyTakeDamage += OnActorTakeDamage;
		}

		private void InitKillActor() {
			GenericActorHealth.OnAnyTakeDamage -= OnActorTakeDamage;
			GenericActorHealth.OnAnyTakeDamage += OnActorTakeDamage;
		}

		private void InitStartDialogue() {
			Dialogue.DialogueManager.OnNodeStart -= OnDialogueNodeStart;
			Dialogue.DialogueManager.OnNodeStart += OnDialogueNodeStart;
		}

		private void InitEndDialogue() {
			Dialogue.DialogueManager.OnDialogueEnd -= OnDialogueNodeEnd;
			Dialogue.DialogueManager.OnDialogueEnd += OnDialogueNodeEnd;
		}


		// Destructors
		// ==========================================================
		private void DestructReachLocation() {
			Locations.LocationZone.OnAnyZoneEnter -= OnPlayerLocationEnter;
		}

		private void DestructInteractWithObject() {
			PlayerInteractionManager.OnPlayerInteractWithObject -= OnPlayerInteractWithObject;
		}

		private void DestructShootAtActor() {
			GenericActorHealth.OnAnyTakeDamage -= OnActorTakeDamage;
		}

		private void DestructKillActor() {
			GenericActorHealth.OnAnyTakeDamage -= OnActorTakeDamage;
		}

		private void DestructStartDialogue() {
			Dialogue.DialogueManager.OnNodeStart -= OnDialogueNodeStart;
		}

		private void DestructEndDialogue() {
			Dialogue.DialogueManager.OnDialogueEnd -= OnDialogueNodeEnd;
		}


		// Event handlers
		// ==========================================================
		private void OnPlayerLocationEnter(Locations.LocationZone zone, Locations.LocationZone.LocationZoneEventData data) {
			if (!ReachLocationParam.LayerMask.ContainsLayer( data.Collider.gameObject.layer ))
				return;

			if (zone.LocationName == ReachLocationParam.LocationName) {
				FinishCurrentSolution();
			}
				
		}

		private void OnPlayerInteractWithObject( PlayerInteractionManager.PlayerInteractionEventData data) {
			// Check name
			bool nameFits = false;
			switch (InteractWithObjectParam.CompareMode) {
				case CompareNameMode.CompareGameObjectName:
					nameFits = InteractWithObjectParam.ValidObjectNames.Contains( data.InteractableObject.GetGameObject().name );
					break;
				case CompareNameMode.CompareInteractableName:
					nameFits = InteractWithObjectParam.ValidObjectNames.Contains( data.InteractableObject.ObjectName );
					break;
			}

			// Check interaction type
			bool typeFits = InteractWithObjectParam.ValidDeltaTypes.Contains( data.Type );

			// Everything fits!
			if (nameFits && typeFits) {
				_currentFitCount++;

				if ( _currentFitCount >= InteractWithObjectParam.RequiredAmount ) {
					FinishCurrentSolution();
				}
			}
		}

		private void OnActorTakeDamage(GenericActorHealth o, GenericActorHealth.GenericActorHealthDamageEventData e) {
			if (Type == SolutionType.ShootAtActor) {
				bool nameFits = ShootAtActorParam.ValidActorNames.Contains( o.name );
				bool damageThisShotFits = Mathf.Abs( e.HealthChange.HealthDelta ) >= ShootAtActorParam.RequiredDamagePerShot;
				bool tagFits = ShootAtActorParam.ValidSourceTags.Contains( e.SourceObject.tag );

				if (nameFits && damageThisShotFits && tagFits) {
					_currentTotalDamage += Mathf.Abs( e.HealthChange.HealthDelta );
					bool totalDamageFits = _currentTotalDamage >= ShootAtActorParam.RequiredDamageTotal;

					if (totalDamageFits) {
						_currentFitCount++;

						if ( _currentFitCount >= ShootAtActorParam.RequiredAmount ) {
							FinishCurrentSolution();
						}
					}
				}

			} else if (Type == SolutionType.KillActor) {
				bool nameFits = KillActorParam.ValidActorNames.Contains( o.name );
				bool tagFits = KillActorParam.ValidSourceTags.Contains( e.SourceObject.tag );

				if ( nameFits && tagFits ) {
					_currentFitCount++;

					if ( _currentFitCount >= KillActorParam.RequiredAmount ) {
						FinishCurrentSolution();
					}
				}
			}
		}

		private void OnDialogueNodeStart(Dialogue.DialogueNode node) {
			if (node != StartDialogueParam.Node)
				return;

			FinishCurrentSolution();
		}

		private void OnDialogueNodeEnd(Dialogue.DialogueNode node, Dialogue.DialogueManager.DialogueEndEventData data) {
			if ( node != EndDialogueParam.Node )
				return;

			if (data.DialogueHadOptions) {
				if (data.Index == EndDialogueParam.OptionIndex) {
					FinishCurrentSolution();
					return;
				}
				return;
			}

			FinishCurrentSolution();
        }


		// Misc
		// ==========================================================
		private void FinishCurrentSolution() {
			IsFinished = true;

			if ( OnFinishSolution != null )
				OnFinishSolution( this, new FinishQuestObjectiveSolutionEventData() );

			Destruct();
		}

		#endregion

		#region Internal types

		public enum SolutionType {

			ShootAtActor,
            KillActor,
			InteractWithObject,
			ReachLocation,
			StartDialogue,
			EndDialogue

		}

		[Serializable]
		public class ReachLocationParameter {

			public string LocationName;

			public LayerMask LayerMask;

		}

		[Serializable]
		public class InteractWithObjectParameter {

			public string[] ValidObjectNames = new string[0];

			public PlayerInteractionManager.InteractionDeltaType[] ValidDeltaTypes = new PlayerInteractionManager.InteractionDeltaType[0];

			public int RequiredAmount;

			public CompareNameMode CompareMode = CompareNameMode.CompareGameObjectName;
		}

		[Serializable]
		public class ShootAtActorParameter {

			public string[] ValidActorNames = new string[0];

			public int RequiredAmount;

			public float RequiredDamageTotal;

			public float RequiredDamagePerShot;

			public string[] ValidSourceTags = new string[0];

		}

		[Serializable]
		public class KillActorParameter {

			public string[] ValidActorNames = new string[0];

			public int RequiredAmount;

			public string[] ValidSourceTags = new string[0];

		}

		[Serializable]
		public class StartDialogueParameter {

			public Dialogue.DialogueNode Node;

		}

		[Serializable]
		public class EndDialogueParameter {

			public Dialogue.DialogueNode Node;

			public int OptionIndex;

		}

		public class FinishQuestObjectiveSolutionEventData {

		}

		#endregion

	}

	public enum CompareNameMode {

		CompareGameObjectName,
		CompareInteractableName

	}

}