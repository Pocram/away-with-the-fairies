﻿using UnityEngine;
using UnityEngine.UI;

namespace Dialogue {

	public sealed class DialogueOptionsPanel : MonoBehaviourExtended {

		#region Exposed variables

		[Header("Dialogue Options")]
		[SerializeField]
		private DialogueOptionUiObject[] _dialogueOptionUiObjects = new DialogueOptionUiObject[3];

		public DialogueOptionUiObject[] DialogueOptionUiObjects {
			get { return _dialogueOptionUiObjects; }
		}


		[Header("Selector")]
		[SerializeField]
		private Image _selectorImage;

		[SerializeField]
		private AnimationCurve _selectorMovementAnimationCurve = new AnimationCurve(
			new Keyframe( 0f, 0f ),
			new Keyframe( 1f, 1f )
			);

		[SerializeField]
		private float _selectorMovementDuration = 0.25f;

		#endregion

		#region Private variables
		
		// This
		private RectTransform _rectTransform;

		private float _fullHeight;

		private bool _wasInitiated = false;


		// Options
		private RectTransform[] _dialogueOptioUiObjectRectTransforms;

		private int _optionAmount = 3;


		// Selector
		private RectTransform _selectorRectTransform;

		private int _currentSelectorTargetId = 0;

		private float _selectorMovementOrigin = 0.5f;

		private float _selectorTargetPosition = 0.5f;

		private float _remainingSelectorAnimationDuration = 0.2f;

		#endregion

		#region MonoBehaviour

		private void Start() {
			if ( _wasInitiated )
				return;

			// Get all options' RectTransform component
			_dialogueOptioUiObjectRectTransforms = new RectTransform[_dialogueOptionUiObjects.Length];
			for (int i = 0; i < _dialogueOptionUiObjects.Length; i++) {
				_dialogueOptioUiObjectRectTransforms[i] = _dialogueOptionUiObjects[i].GetComponent<RectTransform>();
			}

			_rectTransform = GetComponent<RectTransform>();
			_fullHeight = _rectTransform.sizeDelta.y;

			_selectorRectTransform = _selectorImage.GetComponent<RectTransform>();
			if (_selectorRectTransform != null) {
				SetSelectorPosition( GetSelectorPositionForElementId( _currentSelectorTargetId ) );

				_selectorMovementOrigin = _selectorRectTransform.anchorMin.y;
				_selectorTargetPosition = _selectorRectTransform.anchorMin.y;
			}

			_remainingSelectorAnimationDuration = _selectorMovementDuration;

			_wasInitiated = true;
		}

		private void Update() {
			MoveSelector();
		}

		#endregion

		#region Public methods

		public void Show() {
			Start();
			gameObject.SetActive( true );
		}

		public void Hide() {
			gameObject.SetActive( false );
		}

		/// <summary>
		/// Sets the amount of available dialogue options to the given value and rearranges the UI accordingly.
		/// </summary>
		public void SetAmountOfOptions(int amount) {
			Start();

			amount = MathfExtension.ClampInt( amount, 0, 3 );
			_optionAmount = amount;

			// Activate and deactive the right amount of objects
			for (int i = 0; i < _dialogueOptionUiObjects.Length; i++) {
				if (i > _dialogueOptionUiObjects.Length)
					break;

				_dialogueOptionUiObjects[i].gameObject.SetActive( i < amount );
			}

			// Rescale the options
			RedistributeRectTransformsToAmount( amount );

			// Rescale the panel
			if(Application.isPlaying)
				_rectTransform.sizeDelta = new Vector2( _rectTransform.sizeDelta.x, _fullHeight * ( amount / 3f ) );
		}

		public void SelectOption(int index, bool skipAnimation = false) {
			index = MathfExtension.ClampInt( index, 0, _optionAmount - 1 );
			
			_currentSelectorTargetId = index;

			_selectorMovementOrigin = _selectorRectTransform.anchorMin.y;
			_selectorTargetPosition = GetSelectorPositionForElementId( index );

			if (!skipAnimation)
				return;

			_selectorMovementOrigin = _selectorTargetPosition;
			float position = GetSelectorPositionForElementId( index );
			SetSelectorPosition( position );
		}

		#endregion

		#region Private methods

		private void RedistributeRectTransformsToAmount(int amount) {
			float fAmount = Mathf.Clamp( amount, 1f, 3f );

			for (int i = 0; i < _dialogueOptioUiObjectRectTransforms.Length; i++) {
				float newMinY = ( fAmount - ( i + 1f ) ) / fAmount;
				float newMaxY = 1f - ( i / fAmount );

				_dialogueOptioUiObjectRectTransforms[i].anchorMin = new Vector2( _dialogueOptioUiObjectRectTransforms[i].anchorMin.x, newMinY );
				_dialogueOptioUiObjectRectTransforms[i].anchorMax = new Vector2( _dialogueOptioUiObjectRectTransforms[i].anchorMax.x, newMaxY );
			}
		}

		private void MoveSelector() {
			if (_selectorImage == null)
				return;

			// Update values if target changes
			if (DialogueManager.Instance.CurrentSelectionIndex != _currentSelectorTargetId) {
				_currentSelectorTargetId = DialogueManager.Instance.CurrentSelectionIndex;

				_selectorMovementOrigin = _selectorRectTransform.anchorMin.y;

				// Get new target
				_selectorTargetPosition = GetSelectorPositionForElementId( _currentSelectorTargetId );

				// Reset time
				_remainingSelectorAnimationDuration = _selectorMovementDuration;
			}

			// Count down time and clamp it
			_remainingSelectorAnimationDuration -= Time.smoothDeltaTime;
			_remainingSelectorAnimationDuration = Mathf.Clamp( _selectorMovementDuration, 0f, _remainingSelectorAnimationDuration );

			// Fetch respective value from animation curve
			float timePercentage = Mathf.Clamp01( 1f - _remainingSelectorAnimationDuration / _selectorMovementDuration );
			float animationPercentage = _selectorMovementAnimationCurve.Evaluate( timePercentage );

			// Apply
			float newPosition = Mathf.LerpUnclamped( _selectorMovementOrigin, _selectorTargetPosition, animationPercentage );
			SetSelectorPosition( newPosition );
		}

		private float GetSelectorPositionForElementId(int id) {
			return 1f - ( 2 * id + 1 ) / ( _optionAmount * 2f );
		}

		private void SetSelectorPosition(float verticalPosition) {
			if (_selectorRectTransform == null)
				return;

			_selectorRectTransform.anchorMin = new Vector2( _selectorRectTransform.anchorMin.x, verticalPosition );
			_selectorRectTransform.anchorMax = new Vector2( _selectorRectTransform.anchorMax.x, verticalPosition );
		}

		#endregion

	}


}