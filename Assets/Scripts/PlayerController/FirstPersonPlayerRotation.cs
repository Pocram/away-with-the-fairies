﻿using UnityEngine;

namespace FirstPersonPlayerController {

	[System.Serializable]
	public class FirstPersonPlayerRotation {

		public Vector2 RotationSpeed = new Vector2( 2f, 2f );

		//public bool SmoothMovement = true;

		//public float SmoothingSpeed = 50f;


		public Vector2 GetInsensifiedMouseInput( Vector2 input ) {
			return new Vector2( input.x * RotationSpeed.x, input.y * RotationSpeed.y );
		}

	}

}