﻿using UnityEngine;

public static class ArrayExtensions {

	#region KeyCode Arrays

	public static bool HasKeyDown( this KeyCode[] arr ) {
		for (int i = 0; i < arr.Length; i++) {
			if (Input.GetKeyDown( arr[i] )) {
				return true;
			}
		}

		return false;
	}

	public static bool HasKey( this KeyCode[] arr ) {
		for ( int i = 0; i < arr.Length; i++ ) {
			if ( Input.GetKey( arr[i] ) ) {
				return true;
			}
		}

		return false;
	}

	public static bool HasKeyUp( this KeyCode[] arr ) {
		for ( int i = 0; i < arr.Length; i++ ) {
			if ( Input.GetKeyUp( arr[i] ) ) {
				return true;
			}
		}

		return false;
	}

	#endregion

}