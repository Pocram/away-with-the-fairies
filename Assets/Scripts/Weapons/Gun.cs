﻿using UnityEditor;
using UnityEngine;

namespace Weapons.Guns {

	public class Gun : ScriptableObject {

		public static float BulletGravity = -9.81f;

		#region Exposed variables

		[Header( "General info" )]
		[SerializeField]
		private string _name = "My awesome gun";

		public string Name {
			get { return _name; }
		}


		[Header( "Gun info" )]
		[SerializeField]
		private int _magazineCapacity = 30;

		public int MagazineCapacity {
			get { return _magazineCapacity; }
		}

		[SerializeField]
		private float _fireDelay = 0.1f;

		public float FireDelay {
			get { return _fireDelay; }
		}

		//[SerializeField]
		//private GunFireType _gunFireType = GunFireType.Semi;

		[SerializeField]
		private float _bulletSpeed = 780f;

		public float BulletSpeed {
			get { return _bulletSpeed; }
		}

		[SerializeField]
		private float _maxBulletDistance = 2000f;

		public float MaxBulletDistance {
			get { return _maxBulletDistance; }
		}

		[SerializeField]
		private float _damagePerBullet = 10f;

		public float DamagePerBullet {
			get { return _damagePerBullet; }
		}

		[SerializeField]
		private float _bulletSprayAngleRange = 3f;

		public float BulletSprayAngleRange {
			get { return _bulletSprayAngleRange; }
		}


		[Header( "Overheating" )]
		[Range( 0f, 1f )]
		[SerializeField]
		private float _coolingPercentagePerSecond = 0.1f;

		public float CoolingPercentagePerSecond {
			get { return _coolingPercentagePerSecond; }
		}

		[Range( 0f, 1f )]
		[SerializeField]
		private float _heatingPercentagePerShot = 0.05f;

		public float HeatingPercentagePerShot {
			get { return _heatingPercentagePerShot; }
		}

		[SerializeField]
		private float _overheatedBlockingTime = 3.5f;

		public float OverheatedBlockingTime {
			get { return _overheatedBlockingTime; }
		}


		[Header( "Mesh" )]
		[SerializeField]
		private Mesh _mesh;

		[SerializeField]
		private Material _material;

		[SerializeField]
		private GameObject _gunObjectPrefab;

		[SerializeField]
		private GameObject _bulletObjectPrefab;

		public GameObject BulletObjectPrefab {
			get { return _bulletObjectPrefab; }
		}


		public Mesh Mesh {
			get { return _mesh; }
		}

		public Material Material {
			get { return _material; }
		}

		#endregion

		#region Public methods

		public int GetMaximumAmountOfSimultaneousBullets() {
			float maxLifeTimeSeconds = _maxBulletDistance / _bulletSpeed;
			return Mathf.CeilToInt( maxLifeTimeSeconds / _fireDelay );
		}

		#endregion

		#region Internal types

		private enum GunFireType {

			Semi,
			Automatic

		}

		#endregion

		#region Object creation

		// Menu item to create new node
		[MenuItem( "AWTF/Assets/Create/New Gun" )]
		public static void CreateNewAsset() {
			string folder = "Assets/ScriptableObjects/Weapons";
			string fileName = "NewGun";

			Gun newAsset = ScriptableObject.CreateInstance<Gun>();

			int totalObjectsByThatName = AssetDatabase.FindAssets( fileName, new[] {folder} ).Length;
			// No other objects by that name found
			if (totalObjectsByThatName == 0) {
				AssetDatabase.CreateAsset( newAsset, folder + "/" + fileName + ".asset" );
				AssetDatabase.SaveAssets();

				EditorUtility.FocusProjectWindow();

				Selection.activeObject = newAsset;
			} else {
				// Other objects by that name exist!
				// Count up; just read the code it's easy
				int id = 1;
				while (AssetDatabase.FindAssets( fileName + " (" + id + ")", new[] {folder} ).Length != 0) {
					id++;
				}

				AssetDatabase.CreateAsset( newAsset, folder + "/" + fileName + " (" + id + ").asset" );
				AssetDatabase.SaveAssets();

				EditorUtility.FocusProjectWindow();

				Selection.activeObject = newAsset;
			}
		}

		#endregion

	}

}