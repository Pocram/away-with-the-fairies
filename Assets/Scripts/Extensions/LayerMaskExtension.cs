﻿using UnityEngine;

public static class LayerMaskExtension {

	public static bool ContainsLayer(this LayerMask lm, int layer) {
		return lm == ( lm | ( 1 << layer ) );
	}
}