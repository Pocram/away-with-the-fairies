﻿using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Dialogue {

	public sealed class DialogueNode : ScriptableObject {

		#region Exposed variables

		[Header("Message")]
		public string SpeakerName;

		[Multiline( 5 )]
		public string Message;

		public DialogueCharacterAnimationInfo[] CharacterAnimationInfo;

		[Header("Dialogue Options")]
		public List<DialogueOption> DialogueOptions = new List<DialogueOption>();
			
		[Header("Events")]
		public DialogueNode NextNode;

		#endregion

		#region Object creation

		// Menu item to create new node
		[MenuItem( "AWTF/Assets/Create/New Dialogue Node" )]
		public static DialogueNode CreateNewNode() {
			return CreateNewNode( "NewDialogueNode" );
		}

		public static DialogueNode CreateNewNode(string name) {
			string folder = "Assets/ScriptableObjects/Dialogues/Nodes";
			string fileName = name;

			DialogueNode newAsset = ScriptableObject.CreateInstance<DialogueNode>();

			int totalObjectsByThatName = AssetDatabase.FindAssets( fileName, new[] { folder } ).Length;
			// No other objects by that name found
			if ( totalObjectsByThatName == 0 ) {
				AssetDatabase.CreateAsset( newAsset, folder + "/" + fileName + ".asset" );
				AssetDatabase.SaveAssets();

				EditorUtility.FocusProjectWindow();

				Selection.activeObject = newAsset;
			} else {
				// Other objects by that name exist!
				// Count up; just read the code it's easy
				int id = 1;
				while ( AssetDatabase.FindAssets( fileName + "(" + id + ")", new[] { folder } ).Length != 0 )
					id++;

				AssetDatabase.CreateAsset( newAsset, folder + "/" + fileName + "(" + id + ").asset" );
				AssetDatabase.SaveAssets();

				EditorUtility.FocusProjectWindow();

				Selection.activeObject = newAsset;
			}
			return newAsset;
		}

		#endregion

		#region Internal types

		[System.Serializable]
		public class DialogueCharacterAnimationInfo {

			public float DelayOfLine = 0f;

			public float CharacterDelay = 0.125f;

			public float CharacterFadeTime = 0.125f;

			public AnimationCurve CharacterFadeAnimationCurve = new AnimationCurve(
				new Keyframe(0f, 0f),
				new Keyframe(1f, 1f)
				);

			public Color LineColor = Color.white;
		}

		#endregion

	}

}