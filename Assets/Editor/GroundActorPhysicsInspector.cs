﻿using UnityEngine;
using UnityEditor;
using UnityEditor.AnimatedValues;

namespace ActorPhysics {

	[CustomEditor( typeof (GroundActorPhysics) )]
	public sealed class GroundActorPhysicsInspector : Editor {

		private GroundActorPhysics _actor = null;


		// The animated bool of IsSlowerWhenAscending
		private AnimBool _slowerAscFade;
		// The animated bool of IsFasterWhenDescending
		private AnimBool _fasterDescFade;


		// animbool of CanJumpInAir
		private AnimBool _canJumpInAirFade;
		// animbool of CanJumpWhileSliding
		private AnimBool _jumpSlideFade;


		private void OnEnable() {
			_actor = (GroundActorPhysics) target;

			_slowerAscFade = new AnimBool( _actor.IsSlowerWhenAscending );
			_slowerAscFade.speed = 4f;
			_slowerAscFade.valueChanged.AddListener( Repaint );

			_fasterDescFade = new AnimBool( _actor.IsFasterWhenDescending );
			_fasterDescFade.speed = 4f;
			_fasterDescFade.valueChanged.AddListener( Repaint );

			_jumpSlideFade = new AnimBool( _actor.CanJumpWhileSliding );
			_jumpSlideFade.speed = 4f;
			_jumpSlideFade.valueChanged.AddListener( Repaint );

			_canJumpInAirFade = new AnimBool( _actor.CanJumpInAir );
			_canJumpInAirFade.speed = 4f;
			_canJumpInAirFade.valueChanged.AddListener( Repaint );
		}


		public override void OnInspectorGUI() {
			//DrawDefaultInspector();
			//return;

			_actor = (GroundActorPhysics) target;

			Gravity();
			EditorGUILayout.Space();
			MovementGeneral();
			EditorGUILayout.Space();
			Jumping();
			EditorGUILayout.Space();
			SlopesAndSlidingUi();
		}


		private void Gravity() {
			EditorGUILayout.LabelField( "Gravity", EditorStyles.boldLabel );

			
			// Gravity
			GUIContent gravityContent = new GUIContent("Gravity", "The actor's gravitational acceleration.\nUnit: m/s²");
			_actor.Gravity = EditorGUILayout.FloatField( gravityContent, _actor.Gravity );
			// Max value: 0
			if (_actor.Gravity > 0f)
				_actor.Gravity = 0f;
			// Warn if value is 0
			if (_actor.Gravity == 0f) {
				EditorGUILayout.HelpBox( "Gravity is set to 0!", MessageType.Warning );
				// Warn again if value is 0 and actor can't move in air
				if ( !_actor.CanMoveInAir) {
					EditorGUILayout.HelpBox( "Gravity is set to 0 and actor is unable to move while airborn!\nThe actor may be immovable!", MessageType.Warning );
				}
			}


			// Grounding Gravity
			GUIContent groundingGravityContent = new GUIContent( "Grounding Gravity", "Grounding gravity keeps the actor on the ground. If the grounding gravity is too low, the actor may lose contact with the ground when descending, i.e. walking down a slope.\nUnit: m/frame" );
			_actor.GroundingVelocity = EditorGUILayout.FloatField( groundingGravityContent, _actor.GroundingVelocity );
			// Max value: 0
			if (_actor.GroundingVelocity > 0f)
				_actor.GroundingVelocity = 0f;
			// Warn if value is 0
			if ( _actor.GroundingVelocity == 0f ) {
				EditorGUILayout.HelpBox( "Grounding gravity is set to 0!\nThe actor may leave the ground unwantedly.", MessageType.Warning );
			}
		}

		private void MovementGeneral() {
			EditorGUILayout.LabelField( "Movement", EditorStyles.boldLabel );

			// Can Move in Air
			var content = new GUIContent( "Can Move in Air", "Whether the actor may be controlled while airborn." );
			_actor.CanMoveInAir = EditorGUILayout.Toggle( content, _actor.CanMoveInAir );


			// Movement speed impediment -> ascending
			// Bool
			GUIContent movementSpeedImpedimentBoolContent = new GUIContent( "Is Slower When Ascending" );
			_actor.IsSlowerWhenAscending = EditorGUILayout.ToggleLeft( movementSpeedImpedimentBoolContent, _actor.IsSlowerWhenAscending );

			// Curve
			_slowerAscFade.target = _actor.IsSlowerWhenAscending;
			if ( EditorGUILayout.BeginFadeGroup( _slowerAscFade.faded ) ) {
				GUIContent movementSpeedImpedimentCurveContent = new GUIContent( "Strength Per Angle", "The effectiveness of the movement speed per surface angle.\nA value of 1 means full movement, a value of 0 means no movement.\nt = 0 ≈ 0°\nt = 1 ≈ 90°" );
				_actor.MovementSpeedImpedimentByAngleAscending = EditorGUILayout.CurveField( movementSpeedImpedimentCurveContent, _actor.MovementSpeedImpedimentByAngleAscending, new Color( 1f, 0f, 0f ), new Rect( 0f, 0f, 1f, 1f ) );
			}
			EditorGUILayout.EndFadeGroup();

			// Movement speed bonus -> descending
			// Bool
			GUIContent movementSpeedBonusBoolContent = new GUIContent( "Is Faster When Descending" );
			_actor.IsFasterWhenDescending = EditorGUILayout.ToggleLeft( movementSpeedBonusBoolContent, _actor.IsFasterWhenDescending );

			// Curve
			_fasterDescFade.target = _actor.IsFasterWhenDescending;
			if ( EditorGUILayout.BeginFadeGroup( _fasterDescFade.faded ) ) {
				GUIContent movementSpeedBonusContent = new GUIContent( "Strength Per Angle", "The effectiveness of the actor's movement input per surface angle.\nA value of 1 means doubled speed, a value of 0 means default speed.\nt = 0 ≈ 0°\nt = 1 ≈ 90°" );
				_actor.MovementSpeedBonusByAngleDescending = EditorGUILayout.CurveField( movementSpeedBonusContent, _actor.MovementSpeedBonusByAngleDescending, new Color( 0f, 1f, 0f ), new Rect( 0f, 0f, 1f, 1f ) );
			}
			EditorGUILayout.EndFadeGroup();


			// Turn speed
			GUIContent turnSpeedContent = new GUIContent("Max Turn Speed", "The actor may not turn faster than this.\nUnit: °/s");
			_actor.MaxTurnSpeed = EditorGUILayout.FloatField( turnSpeedContent, _actor.MaxTurnSpeed );
		}

		private void Jumping() {
			EditorGUILayout.LabelField( "Jumping", EditorStyles.boldLabel );


			// Can jump in air
			GUIContent canJumpInAirContent = new GUIContent("Can Jump In Air");
			_actor.CanJumpInAir = EditorGUILayout.Toggle( canJumpInAirContent, _actor.CanJumpInAir );

			// Amount of jumps
			_canJumpInAirFade.target = _actor.CanJumpInAir;
			if (EditorGUILayout.BeginFadeGroup( _canJumpInAirFade.faded )) {
				GUIContent jumpCountContent = new GUIContent("Max Jumps In Air", "How many times the actor may jump while in the air.");
				_actor.MaxJumpsInAir = EditorGUILayout.IntField( jumpCountContent, _actor.MaxJumpsInAir );
			}
			EditorGUILayout.EndFadeGroup();


			// Jumpslopelimit
			GUIContent maxSlopeJumpLimitContent = new GUIContent("Max Slope Angle To Jump", "The actor will not be able to jump if the ground's slope is higher than this value.\nUnit: Degrees");
			EditorGUILayout.LabelField( maxSlopeJumpLimitContent );
			_actor.MaximumSlopeAngleToJump = EditorGUILayout.FloatField( _actor.MaximumSlopeAngleToJump );


			// Can jump while sliding
			// Bool
			GUIContent canJumpWhileSlidingContent = new GUIContent("Can Jump While Sliding");
			_actor.CanJumpWhileSliding = EditorGUILayout.ToggleLeft( canJumpWhileSlidingContent, _actor.CanJumpWhileSliding );

			// Curve
			_jumpSlideFade.target = _actor.CanJumpWhileSliding;
			if (EditorGUILayout.BeginFadeGroup( _jumpSlideFade.faded )) {
				GUIContent jumpingStrengthBySlidingspeedContent = new GUIContent("Jumping Strength Per Sliding Speed", "The effectiveness of the jump per sliding speed.\nA value of 1 means full force, a value of 0 means no jump at all.\nt = 0 ≈ 0% sliding speed\nt = 1 ≈ 100% sliding speed" );
				EditorGUILayout.LabelField( jumpingStrengthBySlidingspeedContent );
				_actor.JumpingStrengthBySlidingSpeed = EditorGUILayout.CurveField( _actor.JumpingStrengthBySlidingSpeed, new Color( 0f, 1f, 0f ), new Rect( 0f, 0f, 1f, 1f ) );
			}
			EditorGUILayout.EndFadeGroup();

		}

		private void SlopesAndSlidingUi() {
			EditorGUILayout.LabelField( "Slopes And Sliding", EditorStyles.boldLabel );


			// Acceleration
			var accelerationContent = new GUIContent("Acceleration Magnitude", "This value is used when accelerating the sliding vector. The calculation works like this:\nVelocity += Direction * Magnitude\nUnit: m/s²");
			_actor.SlidingSpeedAccelerationMagnitude = EditorGUILayout.FloatField( accelerationContent, _actor.SlidingSpeedAccelerationMagnitude );
			if (_actor.SlidingSpeedAccelerationMagnitude < 0)
				_actor.SlidingSpeedAccelerationMagnitude = 0f;


			// Slope limit
			var slopeLimitContent = new GUIContent( "Slope Limit", "Any surface angle above the limit causes the actor to slide down.\nUnit: Degrees" );
			_actor.SlopeLimit = EditorGUILayout.Slider( slopeLimitContent, _actor.SlopeLimit, 0f, 180f );
			// Warning if value is 0
			if (_actor.SlopeLimit == 0f) {
				EditorGUILayout.HelpBox( "Slope limit is set to 0!\nThe actor will always slide now!", MessageType.Warning );
			}


			// Max sliding velocity
			var maximumSlidingVelocityContent = new GUIContent( "Max Sliding Velocity", "The maximum velocity the actor may slide at.\nUnit: m/s" );
			_actor.MaximumSlidingVelocity = EditorGUILayout.FloatField( maximumSlidingVelocityContent, _actor.MaximumSlidingVelocity );
			// Min: 0
			if ( _actor.MaximumSlidingVelocity < 0f )
				_actor.MaximumSlidingVelocity = 0f;
			// Warning if value is 0
			if (_actor.MaximumSlidingVelocity == 0f) {
				EditorGUILayout.HelpBox( "The actor can not slide if maximum is set to 0!", MessageType.Warning );
			}


			// Sliding deceleration
			var slidingDecelerationContent = new GUIContent( "Sliding Deceleration", "The rate at which the sliding decelerates.\nUnit: m/s²" );
			_actor.SlidingDecelerationPerSecond = EditorGUILayout.FloatField( slidingDecelerationContent, _actor.SlidingDecelerationPerSecond );
			// Min: 0
			if ( _actor.SlidingDecelerationPerSecond > 0f )
				_actor.SlidingDecelerationPerSecond = 0f;
			// Warning if value is 0
			if (_actor.SlidingDecelerationPerSecond == 0f) {
				EditorGUILayout.HelpBox( "The actor will not stop sliding if the deceleration rate is set to 0!", MessageType.Warning );
			}


			// Sliding deceleration strength per angle curve
			GUIContent slidingDecStenPerAngleContent = new GUIContent( "Sliding Deceleration Effectiveness Per Surface Angle", "The strength of the deceleration rate per surface angle.\nA value of 1 means full deceleration, a value of 0 means no deceleration.\nt = 0 ≈ 0°\nt = 1 ≈ 90°" );
			EditorGUILayout.LabelField( slidingDecStenPerAngleContent );
			//_actor.SlidingDecelerationStrengthPerAngleCurve = EditorGUILayout.CurveField( _actor.SlidingDecelerationStrengthPerAngleCurve, new Color( 1f, 0.337f, 0.337f ), new Rect( 0f, 0f, 1f, 1f ) );
			_actor.SlidingDecelerationStrengthPerAngleCurve = EditorGUILayout.CurveField( _actor.SlidingDecelerationStrengthPerAngleCurve, new Color( 0.322f, 0.639f, 1f ), new Rect( 0f, 0f, 1f, 1f ) );


			// Movement speed impediment by sliding speed
			GUIContent movementSpeedImpedimentBySlidingContent = new GUIContent( "Movement Speed Effectiveness By Sliding Speed", "The effectiveness of the actor's movement input in relation to the current sliding speed.\nA value of 1 means normal movement, a value of 0 means no movement.\nt = 0 ≈ not sliding\nt = 1 ≈ sliding at full speed" );
			EditorGUILayout.LabelField( movementSpeedImpedimentBySlidingContent );
			_actor.MovementSpeedImpedimentByslidingSpeed = EditorGUILayout.CurveField( _actor.MovementSpeedImpedimentByslidingSpeed, new Color( 1f, 0f, 0f ), new Rect( 0f, 0f, 1f, 1f ) );
		}

	}

}