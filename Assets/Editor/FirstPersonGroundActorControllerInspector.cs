﻿using UnityEngine;
using UnityEditor;

namespace FirstPersonPlayerController {

	[CustomEditor( typeof( FirstPersonGroundActorController ) )]
	public class FirstPersonGroundActorControllerInspector : Editor {

		private void OnSceneGUI() {
			FirstPersonGroundActorController controller = (FirstPersonGroundActorController) target;

			// Draw sprinting arc
			if ( controller.MinRunLookDotToRun == 1f ) {
				Handles.color = new Color( 1f, 1f, 0f, 0.4f );
				Handles.DrawLine( controller.transform.position, controller.transform.position + controller.transform.forward * 1.5f );

			} else {
				float anglePercentage = ( 1f - ( controller.MinRunLookDotToRun + 1f ) / 2f );
				float arcAngle = anglePercentage * 360f;
				Vector3 startPoint = Quaternion.AngleAxis( -( ( arcAngle + 180f ) / 2f - 90f ), controller.transform.up ) * controller.transform.forward;

				Handles.color = new Color( 1f, 1f, 0f, 0.2f );
				Handles.DrawSolidArc( controller.transform.position, controller.transform.up, startPoint, arcAngle, 1.5f );
			}
		}

	}

}