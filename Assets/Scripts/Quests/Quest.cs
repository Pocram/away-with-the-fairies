﻿using System.Collections.Generic;
using System.IO;
using UnityEditor;
using UnityEngine;

namespace Quests {

	[System.Serializable]
	public sealed class Quest : ScriptableObject {

		#region Exposed variables

		// Info
		// =============================================
		[Header("Info")]
		public string Name;

		[TextArea(3, 3)]
		public string Description;

		public string DescriptionShort;

		public bool IsFinished { get; private set; }



		// Objectives
		// =============================================
		[Header("Objectives")]
		public QuestObjective MainObjective;

		public List<QuestObjective> SubObjectives = new List<QuestObjective>();

		#endregion

		#region Private properties


		#endregion

		#region Events

		public delegate void FinishQuestEventDelegate(Quest quest, FinishQuestEventData data);

		public static FinishQuestEventDelegate OnFinishQuest;

		public delegate void QuestUpdateEventDelegate(Quest quest, QuestUpdateEventData data);

		public static QuestUpdateEventDelegate OnQuestWasUpdated;

		#endregion

		#region Public Methods

		public void Init() {
			IsFinished = false;
			InitObjectives();
			SubscribeToAllObjectives();
		}

		public void Destruct() {
			IsFinished = false;
			UnsubscribeFromAllObjectives();
			DestructObjectives();
		}

		#endregion

		#region Private methods

		private void InitObjectives() {
			if (MainObjective != null) {
				MainObjective.Init();
			}

			for ( int i = 0; i < SubObjectives.Count; i++ ) {
				if (SubObjectives[i] != null) {
					SubObjectives[i].Init();
				}
			}
		}

		private void DestructObjectives() {
			if ( MainObjective != null ) {
				MainObjective.Destruct();
			}

			for ( int i = 0; i < SubObjectives.Count; i++ ) {
				if (SubObjectives[i] == null)
					continue;

				SubObjectives[i].Destruct();
			}
		}

		private void SubscribeToAllObjectives() {
			if (MainObjective != null) {
				MainObjective.OnFinishObjective -= OnFinishObjective;
				MainObjective.OnFailObjective -= OnFailObjective;
				MainObjective.OnFinishObjective += OnFinishObjective;
				MainObjective.OnFailObjective += OnFailObjective;

				MainObjective.OnObjectiveUpdated -= OnObjectiveUpdate;
				MainObjective.OnObjectiveUpdated += OnObjectiveUpdate;
			}

			// Do the deed
			for (int i = 0; i < SubObjectives.Count; i++) {
				if (SubObjectives[i] == null)
					continue;

				SubObjectives[i].OnFinishObjective -= OnFinishObjective;
				SubObjectives[i].OnFailObjective -= OnFailObjective;
				SubObjectives[i].OnFinishObjective += OnFinishObjective;
				SubObjectives[i].OnFailObjective += OnFailObjective;

				SubObjectives[i].OnObjectiveUpdated -= OnObjectiveUpdate;
				SubObjectives[i].OnObjectiveUpdated += OnObjectiveUpdate;
			}
		}

		private void UnsubscribeFromAllObjectives() {
			if ( MainObjective != null ) {
				MainObjective.OnFinishObjective -= OnFinishObjective;
				MainObjective.OnFailObjective -= OnFailObjective;

				MainObjective.OnObjectiveUpdated -= OnObjectiveUpdate;
			}

			// DO IT
			for ( int i = 0; i < SubObjectives.Count; i++ ) {
				if ( SubObjectives[i] == null )
					continue;

				SubObjectives[i].OnFinishObjective -= OnFinishObjective;
				SubObjectives[i].OnFailObjective -= OnFailObjective;

				SubObjectives[i].OnObjectiveUpdated -= OnObjectiveUpdate;
			}
		}


		/// <summary>
		/// Called when a sub-objective has finished.
		/// </summary>
		private void OnFinishObjective(QuestObjective objective, QuestObjective.FinishObjectiveEventData data) {
			Debug.Log( "Objective " + objective.name + " finished!" );
			CheckAllObjectives();
		}

		/// <summary>
		/// Called when a sub-objective has failed.
		/// </summary>
		private void OnFailObjective( QuestObjective objective, QuestObjective.FailObjectiveEventData data ) {

		}

		private void CheckAllObjectives() {
			// If main objective is not finished, ignore rest
			if ( !MainObjective.IsFinished )
				return;

			// If any sub objective is not finished, ignore rest
			for (int i = 0; i < SubObjectives.Count; i++) {
				if (!SubObjectives[i].IsFinished) {
					Debug.Log( SubObjectives[i].name + " is not finished!" );
					return;
				}
			}

			Debug.Log( "Finished quest " + Name + "!" );
			IsFinished = true;
			if (OnFinishQuest != null)
				OnFinishQuest( this, new FinishQuestEventData() );
		}

		private void OnObjectiveUpdate(QuestObjective objective, QuestObjective.QuestObjectiveUpdateEventData data) {
			QuestUpdateEventData eventData = new QuestUpdateEventData();

			if (objective == MainObjective) {
				eventData.MainObjectiveWasUpdated = true;
			}

			eventData.UpdatedObjectives.Add( objective );
			eventData.UpdatedObjectiveSolutions.AddRange( data.UpdatedSolutions );

			Debug.Log( Name + " got an update! Objective: " + objective.name + ", Solution: " + data.UpdatedSolutions[0].Type );

			if (OnQuestWasUpdated != null)
				OnQuestWasUpdated( this, eventData );
		}

		#endregion

		#region Object creation

		// Menu item to create new node
		[MenuItem( "AWTF/Assets/Quests/New Quest" )]
		public static Quest CreateNewNode() {
			return CreateNewNode( "NewQuest" );
		}

		public static Quest CreateNewNode( string name ) {
			string folder = "ScriptableObjects/Quests";
			string fileName = name;

			// Create directory if needed
			if ( !Directory.Exists( Application.dataPath + "/" + folder ) ) {
				Directory.CreateDirectory( Application.dataPath + "/" + folder );
			}

			folder = "Assets/" + folder;

			Quest newAsset = ScriptableObject.CreateInstance<Quest>();

			int totalObjectsByThatName = AssetDatabase.FindAssets( fileName, new[] { folder } ).Length;
			// No other objects by that name found
			if ( totalObjectsByThatName == 0 ) {
				AssetDatabase.CreateAsset( newAsset, folder + "/" + fileName + ".asset" );
				AssetDatabase.SaveAssets();

				EditorUtility.FocusProjectWindow();

				Selection.activeObject = newAsset;
			} else {
				// Other objects by that name exist!
				// Count up; just read the code it's easy
				int id = 1;
				while ( AssetDatabase.FindAssets( fileName + " (" + id + ")", new[] { folder } ).Length != 0 )
					id++;

				AssetDatabase.CreateAsset( newAsset, folder + "/" + fileName + " (" + id + ").asset" );
				AssetDatabase.SaveAssets();

				EditorUtility.FocusProjectWindow();

				Selection.activeObject = newAsset;
			}
			return newAsset;
		}

		#endregion

		#region Public Types

		public class FinishQuestEventData {


		}

		public class QuestUpdateEventData {

			public bool MainObjectiveWasUpdated = false;

			public List<QuestObjective> UpdatedObjectives = new List<QuestObjective>();

			public List<QuestObjectiveSolution> UpdatedObjectiveSolutions = new List<QuestObjectiveSolution>();

		}

		#endregion

	}

}