﻿using UnityEngine;

namespace Weapons {

	public class PlayerShootingManager : MonoBehaviourExtended {

		#region Input Blocking

		public static int InputBlockers = 0;

		private float _lastBlockTime = 0f;

		private float _unblockDelayTime = 0.1f;

		#endregion

		[Header("Input")]
		[SerializeField]
		private KeyCode _fireKey = KeyCode.Mouse0;

		[SerializeField]
		private KeyCode _reloadKey = KeyCode.Mouse1;


		[SerializeField]
		private WeaponManager _weaponManager;


		private void Update() {
			if (GamePausing.PauseManager.IsPaused)
				return;

			// Prevent shooting if blocked
			if (_weaponManager == null || InputBlockers > 0) {
				_lastBlockTime = Time.unscaledTime;
				return;
			}

			// Prevent shooting for a short time after unblocking
			if (Time.unscaledTime - _lastBlockTime <= _unblockDelayTime) {
				return;
			}


			if (Input.GetKey( _fireKey )) {
				_weaponManager.Shoot();
			} else if (Input.GetKeyDown( _reloadKey )) {
				_weaponManager.Reload();
			}
		}

	}

}