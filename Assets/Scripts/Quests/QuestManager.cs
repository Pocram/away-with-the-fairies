﻿using UnityEngine;

namespace Quests {

	[DisallowMultipleComponent]
	public class QuestManager : MonoBehaviourExtended {

		#region Singleton

		private static QuestManager _instance;

		private static QuestManager Instance {
			get {
				if ( _instance == null ) {
					_instance = FindObjectOfType<QuestManager>();

					// If no instance is found, create one
					if ( _instance == null ) {
						GameObject newGo = new GameObject();
						newGo.name = "QuestManager";
						newGo.AddComponent<QuestManager>();
					}
				}

				return _instance;
			}
		}

		#endregion

		#region Exposed variables

		[SerializeField]
		private Quest _currentQuest;

		public static Quest CurrentQuest {
			get { return Instance._currentQuest; }
		}

		public static bool HasActiveQuest {
			get { return Instance._currentQuest != null; }
		}

		#endregion

		#region MonoBehaviour

		private void Awake() {
			EnsureSingletonPattern();
		}

		private void Start() {
			// TODO: REMOVE ME
			StartQuest( CurrentQuest );

			Quest.OnFinishQuest -= OnFinishQuest;
			Quest.OnFinishQuest += OnFinishQuest;
		}

		private void OnDisable() {
			Quest.OnFinishQuest -= OnFinishQuest;
		}

		#endregion

		#region Public static methods

		public static void StartQuest(Quest quest) {
			Instance._currentQuest.Destruct();
			Instance._currentQuest = quest;
			Instance._currentQuest.Init();
		}

		public static void EndCurrentQuest() {
			if ( Instance._currentQuest == null )
				return;

			Instance._currentQuest.Destruct();
			Instance._currentQuest = null;
		}

		/// <summary>
		/// Adds an objective to the current active quest at the given index. If index is less than zero it is added at the end.
		/// </summary>
		public static void AddObjectiveToCurrentQuest(QuestObjective objective, int index = -1) {
			if ( !HasActiveQuest )
				return;

			if (index < 0) {
				index = CurrentQuest.SubObjectives.Count;
			}

			Instance._currentQuest.SubObjectives.Insert( index, objective );
		}

		#endregion

		#region Private methods

		/// <summary>
		/// Ensures only one instance can be active at a time.
		/// </summary>
		private void EnsureSingletonPattern() {
			if ( _instance != null && _instance != this ) {
				Destroy( gameObject );
			} else {
				_instance = this;
				DontDestroyOnLoad( gameObject );
			}
		}

		private void OnFinishQuest( Quest quest, Quest.FinishQuestEventData data ) {
			if (quest != Instance._currentQuest) {
				Debug.LogWarning( "A quest that is not the current quest has finished!" );
			}

			EndCurrentQuest();
		}

		#endregion

	}

}