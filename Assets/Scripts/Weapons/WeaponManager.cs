﻿using UnityEngine;

using Weapons.Guns;

namespace Weapons {

	public class WeaponManager : MonoBehaviourExtended {

		public Transform BulletOrigin;

		#region Gun info

		[SerializeField]
		private Gun _currentGun;

		public Gun CurrentGun {
			get { return _currentGun; }
		}

		[SerializeField]
		private int _bulletsInMagazine = 0;

		public int BulletsInMagazine {
			get { return _bulletsInMagazine; }
		}

		[SerializeField]
		private int _bulletInventory = 300;

		public int BulletsInInventory {
			get {
				return _bulletInventory;
			}
		}

		private float _remainingFiringDelay = 0f;

		[SerializeField]
		[Range(0f, 1f)]
		private float _currentWeaponTemperature = 0f;

		public float CurrentWeaponTemperature {
			get { return _currentWeaponTemperature; }
		}

		[SerializeField]
		[Range(0f, 1f)]
		private float _overheatingBlockingTemperatureThreshold = 0.95f;

		private float _remainingOverheatedBlockTime = 0f;

		public float RemainingOverheatedBlockTime {
			get {
				return _remainingOverheatedBlockTime;
			}
		}

		#endregion

		#region Exposed variables

		public bool HasWeapon {
			get { return _currentGun != null; }
		}

		/// <summary>
		/// The weapon can shoot if the weapon has bullets, is not overheated and is off cooldown.
		/// </summary>
		public bool CanShoot {
			get {
				if (_currentGun == null)
					return false;

				if (BulletsInMagazine == 0)
					return false;

				if (RemainingOverheatedBlockTime > 0f)
					return false;

				if (_remainingFiringDelay > 0f)
					return false;

				return true;
			}
		}

		public bool CanReload {
			get {
				return BulletsInMagazine < CurrentGun.MagazineCapacity
				       && BulletsInInventory > 0;
			}
		}

		#endregion

		#region Bullet object pool

		private string _bulletObjectPoolFolderName = "BULLET_OBJECT_POOL";

		private ObjectPool<Bullet> _bulletObjectPool;

		#endregion

		#region Events

		public delegate void GunShootEvent(ShootingEventData e);

		public delegate void GunReloadingEvent(ReloadingEventData e);

		public event GunShootEvent OnGunShoot;

		public event GunReloadingEvent OnGunReload;

		#endregion

		#region MonoBehavviour

		private void Start() {
			ValidateGun();
			UpdateBulletPool();
		}

		private void Update() {
			if (GamePausing.PauseManager.IsPaused)
				return;

			if (_currentGun == null)
				return;

			HandleShootingCooldown();
			HandleGunCooldown();

			ValidateGun();
		}

		#endregion

		#region Private methods

		/// <summary>
		/// Validates gun properties and updates values if needed.
		/// </summary>
		private void ValidateGun() {
			if (_currentGun == null)
				return;

			_bulletsInMagazine = MathfExtension.ClampInt( _bulletsInMagazine, 0, _currentGun.MagazineCapacity );

			_remainingFiringDelay = Mathf.Clamp( _remainingFiringDelay, 0f, _currentGun.FireDelay );
			_currentWeaponTemperature = Mathf.Clamp( _currentWeaponTemperature, 0f, 1f );
		}

		private void HandleShootingCooldown() {
			if (_remainingFiringDelay > 0f) {
				_remainingFiringDelay -= Time.deltaTime;
				_remainingFiringDelay = Mathf.Clamp( _remainingFiringDelay, 0f, _currentGun.FireDelay );
			}
		}

		private void HandleGunCooldown() {
			if (_currentWeaponTemperature > 0f) {
				_currentWeaponTemperature -= _currentGun.CoolingPercentagePerSecond * Time.deltaTime;
				_currentWeaponTemperature = Mathf.Clamp( _currentWeaponTemperature, 0f, 1f );
			}

			if (_remainingOverheatedBlockTime > 0f) {
				_remainingOverheatedBlockTime -= Time.deltaTime;
			}
		}

		#endregion

		#region Public methods

		public void Shoot() {
			if (_currentGun == null || BulletOrigin == null)
				return;

			ShootingEventData e;

			if (_bulletsInMagazine == 0 || _remainingFiringDelay > 0f || _remainingOverheatedBlockTime > 0f) {
				e = new ShootingEventData( false, 0, 0 );
				if (OnGunShoot != null)
					OnGunShoot( e );
				return;
			}

			// Remove bullet
			_bulletsInMagazine--;
			if (_bulletsInMagazine < 0)
				_bulletsInMagazine = 0;

			// Reset firing delay timer
			_remainingFiringDelay = _currentGun.FireDelay;

			// Bring the heat
			_currentWeaponTemperature += _currentGun.HeatingPercentagePerShot;
			if (_currentWeaponTemperature >= _overheatingBlockingTemperatureThreshold) {
				_remainingOverheatedBlockTime = _currentGun.OverheatedBlockingTime;
			}

			e = new ShootingEventData( true, _bulletsInMagazine - 1, _bulletsInMagazine );

			// Get next bullet from pool
			Bullet bullet = _bulletObjectPool.GetNext();

			// Activate and prepare it
			bullet.gameObject.SetActive( true );
			bullet.transform.position = BulletOrigin.position;
			
			// Get spray
			Vector3 finalDirection = BulletOrigin.forward;
			if (_currentGun.BulletSprayAngleRange > 0f) {
				// Rotate x
				float randXAngle = Random.Range( -_currentGun.BulletSprayAngleRange, _currentGun.BulletSprayAngleRange );
				float randYAngle = Random.Range( -_currentGun.BulletSprayAngleRange, _currentGun.BulletSprayAngleRange );

				finalDirection = Quaternion.AngleAxis( randXAngle, transform.up ) * finalDirection;
				finalDirection = Quaternion.AngleAxis( randYAngle, transform.right ) * finalDirection;
			}

			// Shoot it
			bullet.Shoot( finalDirection, _currentGun, gameObject );


			ValidateGun();
		}

		public void Reload() {
			ReloadingEventData e;

			if (_bulletInventory <= 0) {
				e = new ReloadingEventData( ReloadSuccess.NoAmmoLeft, _bulletsInMagazine, _bulletsInMagazine, _bulletInventory,
					_bulletInventory );
				if (OnGunReload != null)
					OnGunReload( e );
				return;
			}

			if (_bulletsInMagazine >= _currentGun.MagazineCapacity) {
				e = new ReloadingEventData( ReloadSuccess.GunFull, _bulletsInMagazine, _bulletsInMagazine, _bulletInventory,
					_bulletInventory );
				if (OnGunReload != null)
					OnGunReload( e );
				return;
			}

			int previousAmmoInGun = _bulletsInMagazine;
			int previousAmmoInventory = _bulletInventory;

			int emptySlotsInGun = _currentGun.MagazineCapacity - _bulletsInMagazine;
			_bulletsInMagazine += _bulletInventory;
			_bulletInventory -= emptySlotsInGun;

			if (_bulletInventory < 0)
				_bulletInventory = 0;

			e = new ReloadingEventData( ReloadSuccess.Success, previousAmmoInGun, _bulletsInMagazine, previousAmmoInventory,
				_bulletInventory );
			if (OnGunReload != null)
				OnGunReload( e );

			ValidateGun();
		}


		[ContextMenu("Update bullet pool")]
		public void UpdateBulletPool() {
			if (_currentGun == null || _currentGun.BulletObjectPrefab == null)
				return;

			if ( _bulletObjectPool == null ) {
				// Find folder object
				Transform poolFolderObject = transform.FindChild( _bulletObjectPoolFolderName );
				if ( poolFolderObject == null ) {
					GameObject newGo = new GameObject( _bulletObjectPoolFolderName );;

					poolFolderObject = newGo.transform;
				}

				_bulletObjectPool = new ObjectPool<Bullet>( poolFolderObject, _currentGun.BulletObjectPrefab, 0 );
				
			}

			// Get amount of bullets in pool
			int maxBulletCount = _currentGun.GetMaximumAmountOfSimultaneousBullets();

			if (maxBulletCount <= _bulletObjectPool.Count)
				return;

			_bulletObjectPool.Resize( maxBulletCount );
			for ( int i = 0; i < _bulletObjectPool.Pool.Count; i++ ) {
				_bulletObjectPool.Pool[i].tag = gameObject.tag;
			}
		}

		#endregion

		#region Public types

		public enum ReloadSuccess {
			Success,
			NoAmmoLeft,
			GunFull
		}

		public class ShootingEventData {
			public ShootingEventData(bool couldshoot, int previousAmmo, int newAmmo) {
				CouldShoot = couldshoot;

				PreviousAmmoAmount = previousAmmo;

				NewAmmoAmount = newAmmo;
			}

			public bool CouldShoot;

			public int PreviousAmmoAmount;

			public int NewAmmoAmount;
		}

		public class ReloadingEventData {
			public ReloadingEventData(WeaponManager.ReloadSuccess reloadSuccess, int previousAmmoInGun, int newAmmoInGun,
				int previousAmmoInInventory, int newAmmoInInventory) {
				ReloadSuccess = reloadSuccess;

				PreviousAmmoInGun = previousAmmoInGun;
				NewAmmoInGun = newAmmoInGun;

				PreviousAmmoInInventory = previousAmmoInInventory;
				NewAmmoInInventory = newAmmoInInventory;

				BulletsReloaded = PreviousAmmoInGun - NewAmmoInGun;
			}

			public WeaponManager.ReloadSuccess ReloadSuccess;


			public int PreviousAmmoInGun;
			public int NewAmmoInGun;

			public int PreviousAmmoInInventory;
			public int NewAmmoInInventory;

			public int BulletsReloaded;
		}

		#endregion

	}

}