﻿using UnityEngine;

public class MonoBehaviourExtended : MonoBehaviour {

	protected static void BlockPlayerInput() {
		FirstPersonPlayerController.PlayerInput.InputBlockers++;
		ObjectInteraction.PlayerInteractionManager.InputBlockers++;
		Weapons.PlayerShootingManager.InputBlockers++;
	}

	protected static void FreePlayerInput() {
		FirstPersonPlayerController.PlayerInput.InputBlockers--;
		ObjectInteraction.PlayerInteractionManager.InputBlockers--;
		Weapons.PlayerShootingManager.InputBlockers--;
	}

}