﻿using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer( typeof (CurveStyleAttribute) )]
public sealed class CurveRangeAttributeDrawer : PropertyDrawer {

	public override void OnGUI( Rect position, SerializedProperty property, GUIContent label ) {
		CurveStyleAttribute style = (CurveStyleAttribute) attribute;

		// Catch exception and scold user
		if (property.propertyType != SerializedPropertyType.AnimationCurve) {
			EditorGUI.LabelField( position, label.text, "Can only apply [CurveRange] to AnimationCurves." );
		}

		if (style.HasRect)
			property.animationCurveValue = EditorGUI.CurveField( position, label, property.animationCurveValue, style.Color, style.CurveRect );
		else
			property.animationCurveValue = EditorGUI.CurveField( position, label, property.animationCurveValue, style.Color, new Rect() );
	}

}