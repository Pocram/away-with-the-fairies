﻿using UnityEngine;

namespace GamePausing {

	public class PauseManager : MonoBehaviourExtended {

		#region Singleton

		private static PauseManager _instance;

		private static PauseManager Instance {
			get {
				if ( _instance == null ) {
					_instance = FindObjectOfType<PauseManager>();

					// If no instance is found, create one
					if ( _instance == null ) {
						GameObject newGo = new GameObject();
						newGo.name = "PauseManager";
						newGo.AddComponent<PauseManager>();
					}
				}

				return _instance;
			}
		}

		#endregion

		#region Public static properties

		public static bool IsPaused = false;

		#endregion

		#region Exposed variables

		[SerializeField]
		private KeyCode _pauseKey = KeyCode.Escape;

		#endregion

		#region Private variables

		private static int _pauseBlockers = 0;

		private float _previousTimeScale = 1f;

		#endregion

		#region MonoBehaviour

		private void Awake() {
			_previousTimeScale = Time.timeScale;

			EnsureSingletonPattern();
		}

		private void Update() {
			// You can not pause during a dialogue
			if (Dialogue.DialogueManager.Instance.DialogueIsActive)
				return;

			if (Input.GetKeyDown( _pauseKey )) {
				if (IsPaused) {
					ResumeGame();
				} else {
					PauseGame();
				}
			}
		}

		#endregion

		#region Private methods

		/// <summary>
		/// Ensures only one instance can be active at a time.
		/// </summary>
		private void EnsureSingletonPattern() {
			if ( _instance != null && _instance != this ) {
				Destroy( gameObject );
			} else {
				_instance = this;
				DontDestroyOnLoad( gameObject );
			}
		}

		#endregion

		#region Public static methods

		public static void PauseGame() {
			if (IsPaused || _pauseBlockers > 0)
				return;

			IsPaused = true;

			// Pause time
			Instance._previousTimeScale = Time.timeScale;
			Time.timeScale = 0f;

			// Unlock cursor
			CursorController.Instance.FreeCursor();
		}

		public static void ResumeGame() {
			if (!IsPaused)
				return;
			IsPaused = false;

			// Resume time
			Time.timeScale = Instance._previousTimeScale;

			// Lock cursor
			CursorController.Instance.LockCursor();
		}

		public static void BlockPausing() {
			_pauseBlockers++;
		}

		public static void EnablePausing() {
			_pauseBlockers--;
		}

		#endregion

	}

}
