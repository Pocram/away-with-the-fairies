﻿//#define GROUND_CHARACTER_CONTROLLER_DEBUG

using UnityEngine;
using ActorPhysics;

namespace FirstPersonPlayerController {

	[ExecuteInEditMode]
	public class FirstPersonGroundActorController : MonoBehaviour {

		#region Targets

		[Header( "Targets" )]
		[Tooltip( "The GroundActorPhysics component of the target actor." )]
		[SerializeField]
		private GroundActorPhysics _actor;

		[Tooltip( "The player camera." )]
		[SerializeField]
		private FirstPersonPlayerCameraController _playerCameraController;

		#endregion

		#region Exposed variables

		#region Movement variables

		[Header( "Movement" )]
		public float BaseMovementSpeed = 2.5f;

		public float RunMovementSpeedMultiplier = 2.25f;

		public float CrouchingMovementSpeed = 1.75f;

		public float ProneMovementSpeed = 1f;

		[Range( -1f, 1f )]
		public float MinRunLookDotToRun = 0.5f;

		public float JumpHeight = 1f;

		public FirstPersonPlayerRotation RotationData;


		[Header( "Limitations" )]
		[SerializeField]
		private LayerMask _solidObjectsLayerMask;

		public bool CanJumpWhileCrouching = true;

		public bool CanJumpWhileProne = false;

		public bool SprintingCancelsCrouching = true;

		public bool CanSprintWhileCrouching = false;

		public bool CanSprintWhileProne = false;

		#endregion

		#region Height

		[Header( "Player height" )]
		[SerializeField]
		private float _defaultPlayerHeight = 1.30f;

		[SerializeField]
		private float _coweringPlayerHeight = 0.9f;

		[SerializeField]
		private float _pronePlayerHeight = 0.5f;

		public float ActorHeight {
			get { return _currentActorHeight; }
		}

		#endregion

		#endregion

		#region Private fields

		private MovementState _movementState = MovementState.Idle;

		private PostureState _postureState = PostureState.Upright;

		private float _currentActorHeight = 0f;

		#endregion

		#region MonoBehaviour

		private void Update() {
			if ( _actor == null )
				return;

			// TODO: Move to StandUp()
			switch (_postureState) {
				case PostureState.Upright:
					SetDefaultHeight();
					break;
				case PostureState.Crouching:
					SetCrouchingHeight();
					break;
				case PostureState.Prone:
					SetProneHeight();
					break;
			}
		}

		#endregion

		#region Private methods

		private void GetMovementBehaviourState(Vector3 direction, ref bool run) {
			// Handle airborn states
			if (_actor.IsInAir) {
				if ( _actor.Velocity.y < 0f ) {
					_movementState = MovementState.Falling;
					return;
				}

				if ( _actor.IsJumping ) {
					_movementState = MovementState.Jumping;
					return;
				}
			}

			// Check if the actor may run
			if ( direction.magnitude > 0f ) {
				if (_postureState == PostureState.Upright
				    || ( _postureState == PostureState.Crouching && CanSprintWhileCrouching )
				    || ( _postureState == PostureState.Prone && CanSprintWhileProne )
					|| (_postureState == PostureState.Crouching && SprintingCancelsCrouching)) {

					if ( run && _actor.IsOnGround ) {
						// If sprinting cancels crouching and the actor has enough room, do so
						if (_postureState == PostureState.Crouching && SprintingCancelsCrouching) {
							if (HasSpaceForActorHeight( _defaultPlayerHeight )) {
								_postureState = PostureState.Upright;
							}
						}

						// Check if the actor may sprint in this direction
						float dot = Vector3.Dot( direction, transform.forward );

						if ( dot >= MinRunLookDotToRun ) {
							_movementState = MovementState.Running;
							return;
						} else {
							run = false;
							_movementState = MovementState.Walking;
							return;
						}

					}

				}

				run = false;
				_movementState = MovementState.Walking;
				return;
			}

			if ( _movementState == MovementState.Walking || _movementState == MovementState.Running || _movementState == MovementState.Falling || _movementState == MovementState.Jumping ) {
				run = false;
				_movementState = MovementState.Idle;
			}
		}

		private void SetDefaultHeight() {
			SetHeight( _defaultPlayerHeight );
		}

		private void SetCrouchingHeight() {
			SetHeight( _coweringPlayerHeight );
		}

		private void SetProneHeight() {
			SetHeight( _pronePlayerHeight );
		}


		private bool StandUp() {
			// Check for sufficient space
			if ( !HasSpaceForActorHeight( _defaultPlayerHeight ) ) {
				return false;
			}

			_postureState = PostureState.Upright;

			return true;
		}

		private bool HasSpaceForActorHeight( float height ) {
			return !_actor.CheckCapsule( height, _solidObjectsLayerMask );
		}

		#endregion

		#region Public state methods

		public void MoveInDirection( Vector3 direction, LocationSpace locationSpace, bool run = false ) {
			if ( locationSpace == LocationSpace.Local ) {
				// Apply the forward angle to the input angle.
				// This converts the input (world) to local.
				float angleOfForwardVectorInWorldSpace = Vector3.Angle( transform.forward, Vector3.forward )
														 * Mathf.Sign( Vector3.Dot( transform.forward, Vector3.right ) );

				direction = Quaternion.AngleAxis( angleOfForwardVectorInWorldSpace, Vector3.up ) * direction;
			}

			GetMovementBehaviourState( direction, ref run );

			float movementSpeed = BaseMovementSpeed;

			// Get base speed depending on posture
			switch (_postureState) {
				case PostureState.Crouching:
					movementSpeed = CrouchingMovementSpeed;
					break;
				case PostureState.Prone:
					movementSpeed = ProneMovementSpeed;
					break;
			}

			// Send movement data to physics
			if (_movementState == MovementState.Walking || (_actor.IsInAir && !run)) {
				_actor.MoveInDirection( direction * movementSpeed );
			} else if (_movementState == MovementState.Running || (_actor.IsInAir && run)) {
				_actor.MoveInDirection( direction * movementSpeed * RunMovementSpeedMultiplier );
			}
		}

		/// <summary>
		/// Rotates the player by the given angles.<para/>
		/// The X component rotates the player object around its Y axis.<para/>
		/// The Y component rotates the camera up and down.
		/// </summary>
		public void RotateAndLook( Vector2 angles ) {
			angles = RotationData.GetInsensifiedMouseInput( angles );

			_actor.RotateAroundY( angles.x );
			_playerCameraController.RotateUpDownByAngle( - angles.y );
		}

		public bool Jump() {
			// Check for some posture related conditions first
			if (_postureState == PostureState.Crouching || _postureState == PostureState.Prone) {
				// Can the actor jump while crouching?
				if (_postureState == PostureState.Crouching && !CanJumpWhileCrouching) {
					return false;
				}

				// Can the actor jump while prone?
				if ( _postureState == PostureState.Prone && !CanJumpWhileProne ) {
					return false;
				}

				// Does the actor have enough space to stand up?
				if (!StandUp()) {
					return false;
				}
			}

			_postureState = PostureState.Upright;

			float jumpForce = _actor.GetJumpForceRequiredToReachJumpHeight( JumpHeight );
			return _actor.Jump( jumpForce );
		}


		public bool Crouch() {
			if ( _actor.IsSliding )
				return false;

			// If the actor is crouching already, stand up
			if ( _postureState == PostureState.Crouching ) {
				return StandUp();
			}

			// Check for sufficient space to crouch
			if (!HasSpaceForActorHeight( _coweringPlayerHeight )) {
				return false;
			}

			// Else: Crouch
			_postureState = PostureState.Crouching;
			return true;
		}

		public bool GoProne() {
			if ( _actor.IsSliding )
				return false;

			// If the actor is prone already, stand up
			if ( _postureState == PostureState.Prone ) {
				return StandUp();
			}

			// Else: Go prone
			_postureState = PostureState.Prone;
			return true;
		}

		#endregion

		#region Public methods

		public void SetHeight( float newHeight ) {
			_currentActorHeight = newHeight;
			_actor.SetActorHeight( _currentActorHeight );
		}

		#endregion

		#region Internal types

		private enum MovementState {

			Idle,
			Walking,
			Running,
			//Crouching,
			//Prone,
			Jumping,
			Falling

		}

		private enum PostureState {

			Upright,
			Crouching,
			Prone

		}

		#endregion

		#region __DEBUG

#if GROUND_CHARACTER_CONTROLLER_DEBUG
		private void OnGUI() {
			Vector2 boxDimensions = new Vector2( 300f, 100f );
			Vector2 boxPosition = new Vector2( 10f, 10f );
			float boxPadding = 5f;

			string displayText = "<b>MovementState</b>: " + _movementState;
			displayText += "\n<b>PostureState</b>: " + _postureState;

			GUI.Box( new Rect( boxPosition.x, boxPosition.y, boxDimensions.x, boxDimensions.y ), "<b>GroundCharacterController Debug Console</b>" );
			GUI.Label( new Rect( boxPosition.x + boxPadding, boxPosition.y + boxPadding + 20f, boxDimensions.x - boxPadding, boxDimensions.y - boxPadding - 20f ), displayText );
		}
#endif

		#endregion

	}

}