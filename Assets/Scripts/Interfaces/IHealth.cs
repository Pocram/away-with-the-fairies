﻿using UnityEngine;

public interface IHealth {

	float CurrentHealth { get; }

	/// <summary>
	/// Adds n points of helth to the current amount of health.
	/// </summary>
	/// <param name="healthPoints">The amount of health points to add.</param>
	/// <param name="canDamage">Whether the value can decrease the overall amount of health.</param>
	void Heal(float healthPoints, GameObject sourceObject, bool canDamage = false);

	/// <summary>
	/// Reduces the current health by n points.
	/// </summary>
	/// <param name="damagePoints">The amount of health to be subtracted. The value should be greater than zero.</param>
	/// <param name="canHeal">Whether the value can increase the overall amount of health.</param>
	void Damage( float damagePoints, GameObject sourceObject, bool canHeal = false );

}