﻿using System.Collections;
using GamePausing;
using UnityEngine;

namespace Dialogue {

	[DisallowMultipleComponent]
	public class DialogueManager : MonoBehaviourExtended {

		#region Singleton

		private static DialogueManager _instance;

		public static DialogueManager Instance {
			get {
				if ( _instance == null ) {
					_instance = FindObjectOfType<DialogueManager>();

					// If no instance is found, create one
					if ( _instance == null ) {
						GameObject newGo = new GameObject();
						newGo.name = "DialogueManager";
						newGo.AddComponent<DialogueManager>();
					}
				}

				return _instance;
			}
		}

		#endregion

		#region Exposed variables

		public bool DialogueIsActive { get; private set; }

		public DialogueNode CurrentDialogueNode { get; private set; }

		[SerializeField]
		private int _currentSelectionIndex;

		public int CurrentSelectionIndex {
			get { return _currentSelectionIndex; }
			set {
				if ( !DialogueIsActive )
					return;

				value = MathfExtension.ClampInt( value, 0, 2 );
				value = MathfExtension.ClampInt( value, 0, CurrentDialogueNode.DialogueOptions.Count - 1 );

				_currentSelectionIndex = value;
			}
		}

		// Input
		// ===========================================================
		[Header( "Input" )]
		[SerializeField]
		private KeyCode[] _scrollUpKeys = new KeyCode[] { KeyCode.UpArrow, KeyCode.W, KeyCode.LeftArrow, KeyCode.A };

		[SerializeField]
		private KeyCode[] _scrollDownKeys = new KeyCode[] { KeyCode.DownArrow, KeyCode.S, KeyCode.RightArrow, KeyCode.D };

		[SerializeField]
		private KeyCode[] _chooseOptionKeys = new KeyCode[] { KeyCode.Return };

		[SerializeField]
		private KeyCode[] _skipAnimationKeys = new KeyCode[] { KeyCode.Escape, KeyCode.E, KeyCode.Return, KeyCode.Mouse1 };

		#endregion

		#region Events

		public delegate void DialogueStartEventDelegate( DialogueNode node );

		public delegate void DialogueEndEventDelegate( DialogueNode node, DialogueEndEventData data );

		public static event DialogueStartEventDelegate OnNodeStart;

		public static event DialogueEndEventDelegate OnDialogueEnd;

		#endregion

		#region Private variables

		private Coroutine _getOptionsInputCoroutine;

		#endregion

		#region MonoBehaviour

		private void Awake() {
			EnsureSingletonPattern();
		}

		private void Start() {
			DialogueSystemUiManager.OnMessageFullyShown += OnMessageIsDisplayed;
		}

		private void OnDisable() {
			DialogueSystemUiManager.OnMessageFullyShown -= OnMessageIsDisplayed;
		}

		private void Update() {
			if ( DialogueIsActive ) {
				if ( _skipAnimationKeys.HasKeyDown() ) {
					DialogueSystemUiManager.Instance.SkipAnimation();
				}
			}
		}

		#endregion

		#region Private methods

		/// <summary>
		/// Ensures only one instance can be active at a time.
		/// </summary>
		private void EnsureSingletonPattern() {
			if ( _instance != null && _instance != this ) {
				Destroy( gameObject );
			} else {
				_instance = this;
				DontDestroyOnLoad( gameObject );
			}
		}

		private void OnMessageIsDisplayed( DialogueNode node ) {
			if ( _getOptionsInputCoroutine != null )
				StopCoroutine( _getOptionsInputCoroutine );
			_getOptionsInputCoroutine = StartCoroutine( DetectOptionsInputRoutine( node ) );
		}

		private IEnumerator DetectOptionsInputRoutine( DialogueNode node ) {
			float lastScrollDirection = 0f;

			while ( true ) {
				float scrollDir = Input.GetAxisRaw( "Mouse ScrollWheel" );

				if ( _scrollDownKeys.HasKeyDown() || ( scrollDir < 0f && lastScrollDirection == 0f ) ) {
					CurrentSelectionIndex++;
				} else if ( _scrollUpKeys.HasKeyDown() || ( scrollDir > 0f && lastScrollDirection == 0f ) ) {
					CurrentSelectionIndex--;
				} else if ( _chooseOptionKeys.HasKeyDown() ) {
					break;
				}

				lastScrollDirection = Input.GetAxisRaw( "Mouse ScrollWheel" );
				yield return new WaitIfPaused();
			}

			ChooseCurrentOption();
		}

		#endregion

		#region Public static methods

		public static void ShowDialogueNode( DialogueNode node ) {
			Instance.CurrentDialogueNode = node;
			Instance.CurrentSelectionIndex = 0;
			Instance.DialogueIsActive = true;

			if ( OnNodeStart != null )
				OnNodeStart( node );

			CursorController.Instance.FreeCursor();
			BlockPlayerInput();
		}

		public static void ChooseCurrentOption() {
			if ( !Instance.DialogueIsActive )
				return;

			DialogueNode node = Instance.CurrentDialogueNode;
			int index = Instance.CurrentSelectionIndex;

			if ( Instance.CurrentDialogueNode.DialogueOptions.Count == 0 ) {
				// No options available
				if ( node.NextNode == null ) {
					Close();
					return;
				}

				// Start next node
				ShowDialogueNode( node.NextNode );

			} else {
				// Options ...
				if ( node.DialogueOptions[index].HasEvent ) {
					// TODO: Handle option events
				}

				if ( node.DialogueOptions[index].NextNode == null ) {
					Close();
					return;
				}

				// Start next node
				ShowDialogueNode( node.DialogueOptions[index].NextNode );
			}
		}

		public static void Close() {
			if ( !Instance.DialogueIsActive )
				return;

			if ( OnDialogueEnd != null )
				OnDialogueEnd( Instance.CurrentDialogueNode, new DialogueEndEventData( Instance._currentSelectionIndex, Instance.CurrentDialogueNode.DialogueOptions.Count > 0 ) );

			Instance.DialogueIsActive = false;
			Instance.CurrentDialogueNode = null;

			CursorController.Instance.LockCursor();
			FreePlayerInput();
		}

		#endregion

		public class DialogueEndEventData {

			public DialogueEndEventData(int index, bool hadOptions) {
				Index = index;

				DialogueHadOptions = hadOptions;
			}

			public int Index;

			public bool DialogueHadOptions;
		}

	}
}